/*
 * @file mcscalc.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date July 11, 2024
 * @brief mcscalc is a simple calculator for 802.11 MCS rates.
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2023, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <err.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define __UNUSED __attribute__((unused))

const unsigned MCSINDEX_MAX = 13U; /* max mcs index as of 802.11be */
const unsigned OFDM_SYMBOL_DURATION_NSEC = 12800U; /* in nanoseconds */

typedef unsigned mcs_index_t; /* MCS Index data type */
typedef unsigned nss_t; /* Number of Spetial Streams */

typedef enum : unsigned {
	MODULATION_BPSQ = 0, /* BPSQ */
	MODULATION_QPSK, /* QPSK */
	MODULATION_16QAM, /* 16-QAM */
	MODULATION_64QAM, /* 64-QAM */
	MODULATION_256QAM, /* 256-QAM */
	MODULATION_1024QAM, /* 1024-QAM */
	MODULATION_4096QAM, /* 4096-QAM */
	MODULATION_MAX,
} modulation_e;

typedef enum : unsigned {
	CODING_1_2 = 0, /* 1/2 */
	CODING_2_3, /* 2/3 */
	CODING_3_4, /* 3/4 */
	CODING_5_6, /* 5/6 */
	CODING_MAX, /* invalid coding value */
} coding_e;

typedef enum : unsigned {
	GI_SHORT = 0,
	GI_MEDIUM,
	GI_LONG,
} guard_interval_e;

typedef enum : unsigned {
	DSPR_INVALID = 0, /* Invalid Data Subcarrier per Resource Unit */
	DSPR_26TONE = 24, /* 26 Tone */
	DSPR_52TONE = 48, /* 52 Tone */
	DSPR_52_26TONE = 72, /* 52+26 Tone */
	DSPR_106TONE = 102, /* 106 Tone */
	DSPR_106_26TONE = 126, /* 106+26 Tone */
	DSPR_242TONE = 234, /* 242 Tone, 20 MHz */
	DSPR_484TONE = 468, /* 484 Tone, 40 MHz */
	DSPR_484_242TONE = 702, /* 484+242 Tone */
	DSPR_996TONE = 980, /* 996 Tone, 80 MHz */
	DSPR_996_484TONE = 1448, /* 996+484 Tone */
	DSPR_996_484_242TONE = 1682, /* 996+484+242 Tone */
	DSPR_2_996TONE = 1960, /* 2x996 Tone, 160 MHz */
	DSPR_2_996_484TONE = 2428, /* 2x996+484 Tone */
	DSPR_3_996TONE = 2940, /* 3x996 Tone */
	DSPR_3_996_484TONE = 3408, /* 3x996+484 Tone */
	DSPR_4_996TONE = 3920, /* 4x996 Tone, 320MHz */
} data_subcarrier_e;

static inline unsigned
num_coded_bits_per_subcarrier(const modulation_e modulation)
{
	unsigned nbpsc = 0;

	switch (modulation) {
	case MODULATION_BPSQ:
		nbpsc = 1;
		break;
	case MODULATION_QPSK:
	case MODULATION_16QAM:
	case MODULATION_64QAM:
	case MODULATION_256QAM:
	case MODULATION_1024QAM:
	case MODULATION_4096QAM:
		nbpsc = ((unsigned)modulation) * 2;
		break;
	case MODULATION_MAX:
		nbpsc = 0;
		break;
	}

	return nbpsc;
}

static inline double coding_to_double(const coding_e coding)
{
	double cd = 0.0;

	switch (coding) {
	case CODING_1_2:
		cd = 0.5;
		break;
	case CODING_2_3:
		cd = 2.0 / 3.0;
		break;
	case CODING_3_4:
		cd = 0.75;
		break;
	case CODING_5_6:
		cd = 5.0 / 6.0;
		break;
	case CODING_MAX:
		cd = 0.0;
		break;
	}
	return cd;
}

static inline unsigned guard_interval_to_nanosecond(const guard_interval_e gi)
{
	unsigned gi_nsec = 0;
	switch (gi) {
	case GI_SHORT:
		gi_nsec = 800;
		break;
	case GI_MEDIUM:
		gi_nsec = 1200;
		break;
	case GI_LONG:
		gi_nsec = 3200;
		break;
	}
	return gi_nsec;
}

static inline modulation_e mcsindex_to_modulation(const mcs_index_t index)
{
	modulation_e mod = MODULATION_MAX;
	switch (index) {
	case 0:
		mod = MODULATION_BPSQ;
		break;
	case 1:
	case 2:
		mod = MODULATION_QPSK;
		break;
	case 3:
	case 4:
		mod = MODULATION_16QAM;
		break;
	case 5:
	case 6:
	case 7:
		mod = MODULATION_64QAM;
		break;
	case 8:
	case 9:
		mod = MODULATION_256QAM;
		break;
	case 10:
	case 11:
		mod = MODULATION_1024QAM;
		break;
	case 12:
	case 13:
		mod = MODULATION_4096QAM;
		break;
	default:
		mod = MODULATION_MAX;
		break;
	}
	return mod;
}

static inline coding_e mcsindex_to_conding(const mcs_index_t index)
{
	if (index > MCSINDEX_MAX) /* invalid index */
		return CODING_MAX;

	if (index == 0 || index == 1 || index == 3)
		return CODING_1_2;

	if (index == 5)
		return CODING_2_3;

	if ((index % 2) == 0)
		return CODING_3_4;

	return CODING_5_6;
}

double calculate_mcs_rate(const mcs_index_t mcs_index, const nss_t nss,
			  const data_subcarrier_e nsd,
			  const guard_interval_e gi)
{
	unsigned gi_nsec = guard_interval_to_nanosecond(gi);
	coding_e coding = mcsindex_to_conding(mcs_index);
	unsigned nbpscs = num_coded_bits_per_subcarrier(
		mcsindex_to_modulation(mcs_index));

	return (((unsigned)nsd) * nbpscs * coding_to_double(coding) * nss) /
	       ((OFDM_SYMBOL_DURATION_NSEC + gi_nsec) / 1000.0);
}

int main(__UNUSED int argc, __UNUSED char *argv[])
{
	data_subcarrier_e nsd = DSPR_4_996TONE;
	guard_interval_e gi = GI_SHORT;
	mcs_index_t mcs_index = 5;
	nss_t nss = 2;

	printf("nsd = %u\nGI = %u nanoseconds\nmcs_index = %u\nNss = %u\n", nsd,
	       guard_interval_to_nanosecond(gi), mcs_index, nss);
	printf("data rate = %.1lf\n",
	       calculate_mcs_rate(mcs_index, nss, nsd, gi));

	return EXIT_SUCCESS;
}
