/**
 * @file ping.cpp
 * @brief Implementation file of ICMP echo maker
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <cstring>
#include <fstream>

#include <arpa/inet.h>
#include <errno.h>
#include <linux/icmp.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.hpp"
#include "pingv4.hpp"

const std::string netutils::pingv4::DEFAULT_INTERFACE("DEFAULT_INTERFACE");
const uint16_t netutils::pingv4::DEFAULT_MAX_TRIES(UINT16_MAX);
const std::chrono::milliseconds netutils::pingv4::DEFAULT_TIMEOUT(SIZE_MAX);

netutils::pingv4::pingv4(const std::string &ifname, const std::string &uri,
			 const uint16_t max_tries,
			 const std::chrono::milliseconds &timeout,
			 const bool print_responses)
	: _ifname(ifname)
	, _uri(uri)
	, _max_tries(max_tries)
	, _timeout(timeout)
	, _host_ip()
	, _print_resps(print_responses)
	, _results()
{
	/* left blank */
}

void netutils::pingv4::reset(void)
{
	this->_results.clear();
	this->_results.shrink_to_fit();
}

bool netutils::pingv4::set_ifname(const std::string &ifname)
{
	this->_ifname = ifname;
	return true;
}

bool netutils::pingv4::set_uri(const std::string &uri)
{
	this->_uri = uri;
	return true;
}

bool netutils::pingv4::set_max_tries(const uint16_t &max_tries)
{
	this->_max_tries = max_tries;
	return true;
}

bool netutils::pingv4::set_print_responses(const bool enable)
{
	this->_print_resps = enable;
	return true;
}

bool netutils::pingv4::set_timeout(const std::chrono::milliseconds &timeout)
{
	this->_timeout = timeout;
	return true;
}

const std::chrono::milliseconds &netutils::pingv4::get_timeout(void) const
{
	return this->_timeout;
}

const std::string &netutils::pingv4::get_host_ip(void) const
{
	return this->_host_ip;
}

const std::vector<netutils::ping_result> &
netutils::pingv4::get_results(void) const
{
	return this->_results;
}

void netutils::pingv4::run(void)
{
	std::chrono::steady_clock::time_point begin;
	char ipstr[INET_ADDRSTRLEN];
	sockaddr_in send_addr;
	sockaddr_in recv_addr;
	icmphdr icmp_packet;
	int sock = -1;

	std::memset(ipstr, 0, sizeof(ipstr));
	std::memset(&send_addr, 0, sizeof(send_addr));
	std::memset(&recv_addr, 0, sizeof(recv_addr));
	std::memset(&icmp_packet, 0, sizeof(icmp_packet));

	this->_get_host_ip(&send_addr);
	inet_ntop(AF_INET, &send_addr.sin_addr, ipstr, sizeof(ipstr));
	this->_host_ip = ipstr;

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
	if (sock < 0) {
		return;
	}

	this->_set_socket_timeout(
		sock, netutils::pingv4::sock_timeout_type::TIMEOUT_RECV);
	this->_set_socket_timeout(
		sock, netutils::pingv4::sock_timeout_type::TIMEOUT_SEND);

	if (this->_ifname != netutils::pingv4::DEFAULT_INTERFACE) {
		if (this->_print_resps)
			std::cout << "using interface " << this->_ifname
				  << std::endl;
		this->_bind_to_interface(sock);
	}

	if (this->_print_resps)
		std::cout << "ping target is " << ipstr << std::endl;

	this->_set_icmp_filter(sock);

	icmp_packet.type = ICMP_ECHO;
	icmp_packet.code = 0;

	if (this->_print_resps)
		std::cout << "sending ICMP echo requests to " << ipstr
			  << std::endl;

	for (size_t i = 0; i < this->_max_tries; ++i) {
		uint16_t echo_id = this->_get_echo_id();
		std::chrono::microseconds latency(0);
		icmphdr *icmp_reply = nullptr;
		ssize_t sendbytes = 0;
		ssize_t recvbytes = 0;
		socklen_t addrlen = 0;
		iphdr *iph = nullptr;
		char recvbuf[256];
		int hdrlen = 0;

		std::memset(recvbuf, 0, sizeof(recvbuf));

		icmp_packet.un.echo.id = htons(echo_id);
		icmp_packet.un.echo.sequence = htons(i);

		icmp_packet.checksum = this->_icmp_checksum(
			reinterpret_cast<uint16_t *>(&icmp_packet),
			sizeof(icmp_packet));

		begin = std::chrono::steady_clock::now();

		sendbytes = sendto(sock, &icmp_packet, sizeof(icmp_packet), 0,
				   reinterpret_cast<sockaddr *>(&send_addr),
				   sizeof(send_addr));

		if (sendbytes < 0 || sendbytes != sizeof(icmp_packet)) {
			/* failed to send  packet */
			continue;
		}

		addrlen = sizeof(recv_addr);
		recvbytes = recvfrom(sock, recvbuf, 0, sizeof(recvbuf),
				     reinterpret_cast<sockaddr *>(&recv_addr),
				     &addrlen);

		/* calculate latecy first, then do error hangling / response parsin */
		latency = std::chrono::duration_cast<std::chrono::microseconds>(
			std::chrono::steady_clock::now() - begin);

		if (recvbytes < 0) {
			if (errno != ETIMEDOUT && errno != EAGAIN)
				break; /* we cannot recover from this */

			this->_results.emplace_back(
				latency, this->_host_ip,
				netutils::ping_result::state::timedout);

			if (this->_print_resps)
				std::cout << "PING timedout after "
					  << latency.count() << "µs"
					  << std::endl;
			continue;
		}

		iph = reinterpret_cast<iphdr *>(recvbuf);
		icmp_reply = reinterpret_cast<icmphdr *>(recvbuf + hdrlen);

		hdrlen = iph->ihl << 2;
		if (icmp_reply->type == ICMP_ECHOREPLY) {
			if (this->_print_resps)
				std::cout << "PING reply from " << ipstr
					  << " latency: " << latency.count()
					  << "µs" << std::endl;

			this->_results.emplace_back(latency, this->_host_ip);
			continue;
		}

		if (icmp_reply->type == ICMP_DEST_UNREACH) {
			char gwip[INET_ADDRSTRLEN];

			inet_ntop(AF_INET, &recv_addr.sin_addr, gwip,
				  sizeof(gwip));

			if (this->_print_resps)
				std::cout << gwip << ": Destination unreachable"
					  << std::endl;

			this->_results.emplace_back(
				latency, this->_host_ip,
				netutils::ping_result::state::unreachable);

			continue;
		}
	}
}

netutils::ping_result netutils::pingv4::get_last_result(void) const
{
	netutils::ping_result res;
	if (!this->_results.empty())
		res = this->_results.back();
	return res;
}

int netutils::pingv4::_bind_to_interface(int sock) const
{
	ifreq ifr;

	std::memset(&ifr, 0, sizeof(ifr));
	memccpy(ifr.ifr_name, this->_ifname.c_str(), '\0',
		sizeof(ifr.ifr_name));

	return setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE,
			  reinterpret_cast<void *>(&ifr), sizeof(ifr));
}

int netutils::pingv4::_set_icmp_filter(int sock) const
{
	icmp_filter filter;

	std::memset(&filter, 0, sizeof(filter));

	filter.data = ~((1 << ICMP_SOURCE_QUENCH) | (1 << ICMP_DEST_UNREACH) |
			(1 << ICMP_TIME_EXCEEDED) | (1 << ICMP_REDIRECT) |
			(1 << ICMP_ECHOREPLY));

	return setsockopt(sock, SOL_RAW, ICMP_FILTER,
			  reinterpret_cast<void *>(&filter), sizeof(filter));
}

void netutils::pingv4::_get_host_ip(sockaddr_in *addr) const
{
	addrinfo *results = nullptr;
	addrinfo *rp = nullptr; /* used to traverse results */
	sockaddr_in zeroaddr;
	addrinfo hints;
	int ret = 0;

	std::memset(&zeroaddr, 0, sizeof(zeroaddr));
	std::memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	ret = getaddrinfo(this->_uri.c_str(), nullptr, &hints, &results);
	if (ret != 0) {
		goto out;
	}

	for (rp = results; rp != nullptr; rp = rp->ai_next) {
		if (std::memcmp(&zeroaddr, &rp->ai_addr, sizeof(zeroaddr)) !=
		    0) {
			std::memcpy(addr, rp->ai_addr, sizeof(*addr));
			break; /* we find the proper IP */
		}
	}

out:
	if (results != nullptr)
		freeaddrinfo(results);
}

size_t netutils::pingv4::_calculate_latency(
	const std::chrono::steady_clock::time_point &begin,
	const std::chrono::steady_clock::time_point &end) const
{
	return std::chrono::duration_cast<std::chrono::microseconds>(end -
								     begin)
		.count();
}

uint16_t netutils::pingv4::_get_echo_id(void)
{
	std::ifstream ifs("/dev/random",
			  std::ifstream::in | std::ifstream::binary);
	uint16_t id = 0;

	ifs.read(reinterpret_cast<char *>(&id), sizeof(id));

	return id;
}

int netutils::pingv4::_set_socket_timeout(
	int sock, netutils::pingv4::sock_timeout_type type) const
{
	int opt = (type == netutils::pingv4::sock_timeout_type::TIMEOUT_RECV) ?
				SO_RCVTIMEO :
				SO_SNDTIMEO;
	timeval tv;

	netutils::msecs_to_timeval(this->_timeout, &tv);

	return setsockopt(sock, SOL_SOCKET, opt, &tv, sizeof(tv));
}

uint32_t netutils::pingv4::_icmp_checksum(const uint16_t *buf, uint32_t len)
{
	uint16_t checksum = 0;
	uint32_t sum = 0;

	/* sum up 2 byte values */
	while (len > 1) {
		sum += *(buf++);
		len -= sizeof(*buf);
	}

	/* add left over byte */
	if (len > 0)
		sum += *(reinterpret_cast<const uint8_t *>(buf));

	/*
	 * fold 32 bits sum into 16 bits
	 * sum = (lower 16 bits) + (upper 16 bits shifted to right 16 bits)
	 */
	while (sum >> 16)
		sum = (sum & 0xFFFF) + (sum >> 16);

	checksum = ~sum;

	return checksum;
}
