/**
 * @file main.c
 * @brief Example usage of a simple mmap backed malloc implementation
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Mar 17, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mmmalloc.h>

#define UNUSED(var) ((void)(var))

int main(int argc, char *argv[])
{
	char *data = NULL;
	char *tmp = NULL;

	UNUSED(argc);
	UNUSED(argv);

	data = mmm_calloc(10, sizeof(char));
	if (!data) {
		perror("mmm_calloc failed");
		return EXIT_FAILURE;
	}

	snprintf(data, 10*sizeof(char), "123456789");
	printf("%s\n", data);

	tmp = mmm_realloc(data, 20 * sizeof(char));
	if (!tmp) {
		perror("mmm_realloc failed");
		mmm_free(data);
		return EXIT_FAILURE;
	}
	data = tmp;
	tmp = NULL;

	snprintf(data, 20*sizeof(char), "1234567890123456789");
	printf("%s\n", data);

	mmm_free(data);

	return 0;
}

