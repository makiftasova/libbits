/**
 * @file elf.c
 * @brief Implementation file for ELF file format parser.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define _BSD_SOURCE
#include <endian.h>
#undef _BSD_SOURCE

#include "elf.h"

#pragma GCC poison printf /* do not use printf */

/* relatively safe version of void free(void*) */
#define SFREE(_ptr_)                    \
	({                              \
		if ((_ptr_) != NULL) {  \
			free((_ptr_));  \
			(_ptr_) = NULL; \
		}                       \
	})

/* read _size_ bytes from _fd_ into _dst_ */
#define READ(_fd_, _dst_, _size_) \
	((int64_t)(_size_) != read((_fd_), (_dst_), (_size_)))

/* seek to beginning of _fd_ */
#define SEEK_BEGIN(_fd_) (0 != lseek((_fd_), 0, SEEK_SET))

#define SEEK_OFFSET(_fd_, _off_) \
	((int64_t)(_off_) != lseek((_fd_), (_off_), SEEK_SET))

const uint8_t ELF_MAGIC[4] = { 0x7F, 0x45, 0x4C, 0x46 };

#define ELF_VALID_MAGIG(_elf_ident_)               \
	((_elf_ident_).magic[0] == ELF_MAGIC[0] || \
	 (_elf_ident_).magic[1] == ELF_MAGIC[1] || \
	 (_elf_ident_).magic[2] == ELF_MAGIC[2] || \
	 (_elf_ident_).magic[3] == ELF_MAGIC[3])

#define ELF_INVALID_MAGIG(_elf_ident_)             \
	((_elf_ident_).magic[0] != ELF_MAGIC[0] || \
	 (_elf_ident_).magic[1] != ELF_MAGIC[1] || \
	 (_elf_ident_).magic[2] != ELF_MAGIC[2] || \
	 (_elf_ident_).magic[3] != ELF_MAGIC[3])

#define ELF_PROG_HEADER_ENDIAN_TO_HOST(_list_, _count_, _fun_) \
	({                                                     \
		for (uint16_t i = 0; i < (_count_); ++i) {     \
			(_fun_)(&(_list_)[i]);                 \
		}                                              \
	})

#define ELF_PROG_HEADER_DUMP(_list_, _count_, _fun_, _stream_)            \
	({                                                                \
		for (uint16_t i = 0; i < (_count_); ++i) {                \
			fprintf((_stream_), "========== %u ==========\n", \
				i + 1);                                   \
			(_fun_)(&(_list_)[i], (_stream_));                \
		}                                                         \
	})

static inline void elf32_header_le_to_host(struct elf32_header *hdr)
{
	hdr->type = le16toh(hdr->type);
	hdr->machine = le16toh(hdr->machine);
	hdr->version = le32toh(hdr->version);
	hdr->entry = le32toh(hdr->entry);
	hdr->phoff = le32toh(hdr->phoff);
	hdr->shoff = le32toh(hdr->shoff);
	hdr->flags = le32toh(hdr->flags);
	hdr->ehsize = le16toh(hdr->ehsize);
	hdr->phentsize = le16toh(hdr->phentsize);
	hdr->phnum = le16toh(hdr->phnum);
	hdr->shentsize = le16toh(hdr->shentsize);
	hdr->shnum = le16toh(hdr->shnum);
	hdr->shstrndx = le16toh(hdr->shstrndx);
}

static inline void elf64_header_le_to_host(struct elf64_header *hdr)
{
	hdr->type = le16toh(hdr->type);
	hdr->machine = le16toh(hdr->machine);
	hdr->version = le32toh(hdr->version);
	hdr->entry = le64toh(hdr->entry);
	hdr->phoff = le64toh(hdr->phoff);
	hdr->shoff = le64toh(hdr->shoff);
	hdr->flags = le32toh(hdr->flags);
	hdr->ehsize = le16toh(hdr->ehsize);
	hdr->phentsize = le16toh(hdr->phentsize);
	hdr->phnum = le16toh(hdr->phnum);
	hdr->shentsize = le16toh(hdr->shentsize);
	hdr->shnum = le16toh(hdr->shnum);
	hdr->shstrndx = le16toh(hdr->shstrndx);
}

static inline void elf32_header_be_to_host(struct elf32_header *hdr)
{
	hdr->type = be16toh(hdr->type);
	hdr->machine = be16toh(hdr->machine);
	hdr->version = be32toh(hdr->version);
	hdr->entry = be32toh(hdr->entry);
	hdr->phoff = be32toh(hdr->phoff);
	hdr->shoff = be32toh(hdr->shoff);
	hdr->flags = be32toh(hdr->flags);
	hdr->ehsize = be16toh(hdr->ehsize);
	hdr->phentsize = be16toh(hdr->phentsize);
	hdr->phnum = be16toh(hdr->phnum);
	hdr->shentsize = be16toh(hdr->shentsize);
	hdr->shnum = be16toh(hdr->shnum);
	hdr->shstrndx = be16toh(hdr->shstrndx);
}

static inline void elf64_header_be_to_host(struct elf64_header *hdr)
{
	hdr->type = be16toh(hdr->type);
	hdr->machine = be16toh(hdr->machine);
	hdr->version = be32toh(hdr->version);
	hdr->entry = be64toh(hdr->entry);
	hdr->phoff = be64toh(hdr->phoff);
	hdr->shoff = be64toh(hdr->shoff);
	hdr->flags = be32toh(hdr->flags);
	hdr->ehsize = be16toh(hdr->ehsize);
	hdr->phentsize = be16toh(hdr->phentsize);
	hdr->phnum = be16toh(hdr->phnum);
	hdr->shentsize = be16toh(hdr->shentsize);
	hdr->shnum = be16toh(hdr->shnum);
	hdr->shstrndx = be16toh(hdr->shstrndx);
}

static inline void elf_header_endian_to_host(struct elf_file *f)
{
	/* TODO: mixed/middle/PDP endian? */
	if (ELF_IDENT_ENDIAN_BIG(f->elf_ident)) {
		if (ELF_IDENT_CLASS_32_BIT(f->elf_ident)) {
			elf32_header_be_to_host(f->elf32_header);
		} else if (ELF_IDENT_CLASS_64_BIT(f->elf_ident)) {
			elf64_header_be_to_host(f->elf64_header);
		}
	} else {
		if (ELF_IDENT_CLASS_32_BIT(f->elf_ident)) {
			elf32_header_le_to_host(f->elf32_header);
		} else if (ELF_IDENT_CLASS_64_BIT(f->elf_ident)) {
			elf64_header_le_to_host(f->elf64_header);
		}
	}
}

static inline void elf32_prog_header_le_to_host(struct elf32_prog_header *hdr)
{
	hdr->type = le32toh(hdr->type);
	hdr->offset = le32toh(hdr->offset);
	hdr->vaddr = le32toh(hdr->vaddr);
	hdr->paddr = le32toh(hdr->paddr);
	hdr->filesz = le32toh(hdr->filesz);
	hdr->memsz = le32toh(hdr->memsz);
	hdr->flags = le32toh(hdr->flags);
	hdr->align = le32toh(hdr->align);
}

static inline void elf32_prog_header_be_to_host(struct elf32_prog_header *hdr)
{
	hdr->type = be32toh(hdr->type);
	hdr->offset = be32toh(hdr->offset);
	hdr->vaddr = be32toh(hdr->vaddr);
	hdr->paddr = be32toh(hdr->paddr);
	hdr->filesz = be32toh(hdr->filesz);
	hdr->memsz = be32toh(hdr->memsz);
	hdr->flags = be32toh(hdr->flags);
	hdr->align = be32toh(hdr->align);
}

static inline void elf64_prog_header_le_to_host(struct elf64_prog_header *hdr)
{
	hdr->type = le32toh(hdr->type);
	hdr->flags = le32toh(hdr->flags);
	hdr->offset = le64toh(hdr->offset);
	hdr->vaddr = le64toh(hdr->vaddr);
	hdr->paddr = le64toh(hdr->paddr);
	hdr->filesz = le64toh(hdr->filesz);
	hdr->memsz = le64toh(hdr->memsz);
	hdr->align = le64toh(hdr->align);
}

static inline void elf64_prog_header_be_to_host(struct elf64_prog_header *hdr)
{
	hdr->type = be32toh(hdr->type);
	hdr->flags = be32toh(hdr->flags);
	hdr->offset = be64toh(hdr->offset);
	hdr->vaddr = be64toh(hdr->vaddr);
	hdr->paddr = be64toh(hdr->paddr);
	hdr->filesz = be64toh(hdr->filesz);
	hdr->memsz = be64toh(hdr->memsz);
	hdr->align = be64toh(hdr->align);
}

static void elf_prog_header_list_le_to_host(struct elf_prog_header_list *hdrs)
{
	switch (hdrs->bits) {
	case ELF_BITS_32:
		ELF_PROG_HEADER_ENDIAN_TO_HOST(hdrs->elf32_prog_headers,
					       hdrs->count,
					       elf32_prog_header_le_to_host);
		break;
	case ELF_BITS_64:
		ELF_PROG_HEADER_ENDIAN_TO_HOST(hdrs->elf64_prog_headers,
					       hdrs->count,
					       elf64_prog_header_le_to_host);
		break;
	case ELF_BITS_INVAL:
		break;
	}
}

static void elf_prog_header_list_be_to_host(struct elf_prog_header_list *hdrs)
{
	switch (hdrs->bits) {
	case ELF_BITS_32:
		ELF_PROG_HEADER_ENDIAN_TO_HOST(hdrs->elf32_prog_headers,
					       hdrs->count,
					       elf32_prog_header_be_to_host);
		break;
	case ELF_BITS_64:
		ELF_PROG_HEADER_ENDIAN_TO_HOST(hdrs->elf64_prog_headers,
					       hdrs->count,
					       elf64_prog_header_be_to_host);
		break;
	case ELF_BITS_INVAL:
		break;
	}
}

static inline void elf_prog_header_list_endian_to_host(struct elf_file *file)
{
	/* TODO: mixed/middle/PDP endian ? */
	if (ELF_IDENT_ENDIAN_BIG(file->elf_ident)) {
		elf_prog_header_list_be_to_host(&file->elf_prog_headers);
	} else {
		elf_prog_header_list_le_to_host(&file->elf_prog_headers);
	}
}

static int elf_read_header(struct elf_file *f)
{
	int ret = 0;
	int error = 0;
	bool fail = false;
	struct elf_ident ident;
	struct elf32_header *hdr32 = NULL;
	struct elf64_header *hdr64 = NULL;

	if (f == NULL) {
		ret = -1;
		error = EINVAL;
		goto out;
	}

	if (READ(f->fd, &ident, ELF_IDENT_SIZE)) {
		ret = -1;
		error = errno;
		fail = true;
		goto out;
	}

	if (ELF_INVALID_MAGIG(ident)) {
		error = EINVAL;
		ret = -1;
		fail = true;
		goto out;
	}

	if (SEEK_BEGIN(f->fd)) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	if (ELF_IDENT_CLASS_64_BIT(&ident)) {
		hdr64 = calloc(1, ELF64_HEADER_SIZE);
		if (hdr64 == NULL) {
			error = errno;
			ret = -1;
			fail = true;
			goto out;
		}

		if (READ(f->fd, hdr64, ELF64_HEADER_SIZE)) {
			error = errno;
			ret = -1;
			fail = true;
			goto out;
		}

		f->elf64_header = hdr64;
		f->elf_ident = &hdr64->ident;

	} else if (ELF_IDENT_CLASS_32_BIT(&ident)) {
		hdr32 = calloc(1, ELF32_HEADER_SIZE);
		if (hdr32 == NULL) {
			error = errno;
			ret = -1;
			fail = true;
			goto out;
		}

		if (READ(f->fd, hdr32, ELF32_HEADER_SIZE)) {
			error = errno;
			ret = -1;
			fail = true;
			goto out;
		}

		f->elf32_header = hdr32;
		f->elf_ident = &hdr32->ident;
	}

out:
	if (fail) {
		SFREE(hdr32);
		SFREE(hdr64);
	}
	errno = error;
	return ret;
}

static int elf32_read_prog_header(struct elf_file *f)
{
	const uint16_t num_entry = f->elf32_header->phnum;
	struct elf32_prog_header *phdrs = NULL;
	bool fail = false;
	int error = 0;
	int ret = 0;

	if (f->elf32_header->phentsize != sizeof(struct elf32_prog_header)) {
		error = EINVAL;
		ret = -1;
		fail = true;
		goto out;
	}

	if (SEEK_OFFSET(f->fd, f->elf32_header->phoff)) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	phdrs = calloc(num_entry, sizeof(*phdrs));
	if (phdrs == NULL) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	if (READ(f->fd, phdrs, num_entry * sizeof(*phdrs))) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	f->elf_prog_headers.bits = ELF_BITS_32;
	f->elf_prog_headers.count = num_entry;
	f->elf_prog_headers.elf32_prog_headers = phdrs;

out:
	if (fail) {
		SFREE(phdrs);
	}
	errno = error;
	return ret;
}

static int elf64_read_prog_header(struct elf_file *f)
{
	const uint16_t num_entry = f->elf64_header->phnum;
	struct elf64_prog_header *phdrs = NULL;
	bool fail = false;
	int error = 0;
	int ret = 0;

	if (f->elf64_header->phentsize != sizeof(*phdrs)) {
		error = EINVAL;
		ret = -1;
		fail = true;
		goto out;
	}

	if (SEEK_OFFSET(f->fd, f->elf64_header->phoff)) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	phdrs = calloc(num_entry, sizeof(*phdrs));
	if (phdrs == NULL) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	if (READ(f->fd, phdrs, num_entry * sizeof(*phdrs))) {
		error = errno;
		ret = -1;
		fail = true;
		goto out;
	}

	f->elf_prog_headers.bits = ELF_BITS_64;
	f->elf_prog_headers.count = num_entry;
	f->elf_prog_headers.elf64_prog_headers = phdrs;

out:
	if (fail) {
		SFREE(phdrs);
	}
	errno = error;
	return ret;
}

const char *elf_osabi_to_str(ELF_OSABI osabi)
{
	switch (osabi) {
	case EOA_SYSV:
		return "System V";
	case EOA_HP_UX:
		return "HP-UX";
	case EOA_NETBSD:
		return "NetBSD";
	case EOA_LINUX:
		return "Linux";
	case EOA_GNUHURD:
		return "GNU Hurd";
	case EOA_SOLARIS:
		return "Solaris";
	case EOA_AIX:
		return "AIX";
	case EOA_IRIX:
		return "IRIX";
	case EOA_FREEBSD:
		return "FreeBSD";
	case EOA_TRU64:
		return "Tru64";
	case EOA_MODESTO:
		return "Novell Modesto";
	case EOA_OPENBSD:
		return "OpenBSD";
	case EOA_OPENVMS:
		return "openVMS";
	case EOA_NONSTOP:
		return "NonStop Kernel";
	case EOA_AROS:
		return "AROS";
	case EOA_FENIX_OS:
		return "Fenix OS";
	case EOA_CLOUDABI:
		return "CloudABI";
	default:
		return "Invalid OSABI ID";
	}
}

const char *elf_type_to_str(ELF_TYPE type)
{
	switch (type) {
	case ET_NONE:
		return "None";
	case ET_REL:
		return "REL (Relocatable File)";
	case ET_EXEC:
		return "EXEC (Executable File)";
	case ET_DYN:
		return "DYN (Shared Object File)";
	case ET_CORE:
		return "CORE (Coredump File)";
	case ET_LOOS:
		return "LOOS";
	case ET_HIOS:
		return "HIOS";
	case ET_LOPROC:
		return "LOPROC";
	case ET_HIPROC:
		return "HIPROC";
	default:
		return "Unknown Type";
	}
}

const char *elf_machine_to_str(ELF_MACHINE machine)
{
	switch (machine) {
	case EMAC_GENERIC:
		return "Generic ISA";
	case EMAC_SPARC:
		return "SPARC";
	case EMAC_X86:
		return "x86";
	case EMAC_MIPS:
		return "MIPS";
	case EMAC_POWERPC:
		return "PowerPC";
	case EMAC_S390:
		return "S390";
	case EMAC_ARM:
		return "ARM";
	case EMAC_SUPERH:
		return "SuperH";
	case EMAC_IA_64:
		return "IA-64";
	case EMAC_X86_64:
		return "x86-64";
	case EMAC_AARCH64:
		return "AArch64";
	case EMAC_RISC_V:
		return "RISC-V";
	default:
		return "Unknown Machine";
	}
}

void elf_ident_dump(const struct elf_ident *ident, FILE *stream)
{
	uint8_t *e_ident = (uint8_t *)ident;
	fprintf(stream, "MAGIC: ");
	for (uint8_t i = 0; i < EI_LAST; ++i) {
		fprintf(stream, "%.2X ", e_ident[i]);
	}
	fprintf(stream, "\n");
	fprintf(stream, "Class: %s\n",
		ELF_IDENT_CLASS_64_BIT(ident) ? "ELF64" : "ELF32");
	fprintf(stream, "Endian: %s Endian\n",
		ELF_IDENT_ENDIAN_BIG(ident) ? "Big" : "Little");
	fprintf(stream, "OS ABI: %s\n", elf_osabi_to_str(ident->osabi));
	fprintf(stream, "OS ABI Version: %u\n", ident->abi_version);
	fprintf(stream, "ELF Version: 0x%X\n", ident->version);
}

void elf32_header_dump(const struct elf32_header *header, FILE *stream)
{
	fprintf(stream, "ELF Type: %s\n", elf_type_to_str(header->type));
	fprintf(stream, "ELF Machine: %s\n",
		elf_machine_to_str(header->machine));
	fprintf(stream, "Version: %u\n", header->version);
	fprintf(stream, "Entry Point: 0x%X\n", header->entry);
	fprintf(stream, "Start of program headers: %u (bytes into file)\n",
		header->phoff);
	fprintf(stream, "Start of section headers: %u (bytes into file)\n",
		header->shoff);
	fprintf(stream, "Flags: 0x%X\n", header->flags);
	fprintf(stream, "Size of this header: %u (bytes)\n", header->ehsize);
	fprintf(stream, "Size of program headers: %u (bytes)\n",
		header->phentsize);
	fprintf(stream, "Number of program headers: %u\n", header->phnum);
	fprintf(stream, "Size of section headers: %u (bytes)\n",
		header->shentsize);
	fprintf(stream, "Number of section headers: %u\n", header->shnum);
	fprintf(stream, "Section header string table index: %u\n",
		header->shstrndx);
}

void elf64_header_dump(const struct elf64_header *header, FILE *stream)
{
	fprintf(stream, "ELF Type: %s\n", elf_type_to_str(header->type));
	fprintf(stream, "ELF Machine: %s\n",
		elf_machine_to_str(header->machine));
	fprintf(stream, "Version: 0x%X\n", header->version);
	fprintf(stream, "Entry Point: 0x%lX\n", header->entry);
	fprintf(stream, "Start of program headers: %lu (bytes into file)\n",
		header->phoff);
	fprintf(stream, "Start of section headers: %lu (bytes into file)\n",
		header->shoff);
	fprintf(stream, "Flags: 0x%X\n", header->flags);
	fprintf(stream, "Size of this header: %u (bytes)\n", header->ehsize);
	fprintf(stream, "Size of program headers: %u (bytes)\n",
		header->phentsize);
	fprintf(stream, "Number of program headers: %u\n", header->phnum);
	fprintf(stream, "Size of section headers: %u (bytes)\n",
		header->shentsize);
	fprintf(stream, "Number of section headers: %u\n", header->shnum);
	fprintf(stream, "Section header string table index: %u\n",
		header->shstrndx);
}

const char *elf_prog_type_to_str(ELF_PH_TYPE type)
{
	switch (type) {
	case EPT_NULL:
		return "NULL";
	case EPT_LOAD:
		return "LOAD";
	case EPT_DYNAMIC:
		return "DYNAMIC";
	case EPT_INERTP:
		return "INTERP";
	case EPT_NOTE:
		return "NOTE";
	case EPT_SHLIB:
		return "SHARED LIB";
	case EPT_PHDR:
		return "PHDR";
	case EPT_LOOS:
		return "LOOS";
	case GNU_EH_FRAME:
		return "GNU_EH_FRAME";
	case GNU_STACK:
		return "GNU_STACK";
	case GNU_RELRO:
		return "GNU_RELRO";
	case EPT_HIOS:
		return "HIOS";
	case EPT_LOPROC:
		return "LOPROC";
	case EPT_HIPROC:
		return "HIPROC";
	default:
		return "UNKNOWN";
	}
}

void elf32_prog_header_dump(const struct elf32_prog_header *phdr, FILE *stream)
{
	fprintf(stream, "Type: %s (0x%X)\n", elf_prog_type_to_str(phdr->type),
		phdr->type);
	fprintf(stream, "Offset: 0x%X\n", phdr->offset);
	fprintf(stream, "Vaddr: 0x%X\n", phdr->vaddr);
	fprintf(stream, "Paddr: 0x%X\n", phdr->paddr);
	fprintf(stream, "File Size: %u\n", phdr->filesz);
	fprintf(stream, "Mem Size: %u\n", phdr->memsz);
	fprintf(stream, "Flags: 0x%X\n", phdr->flags);
	fprintf(stream, "Align: 0x%u\n", phdr->align);
}

void elf64_prog_header_dump(const struct elf64_prog_header *phdr, FILE *stream)
{
	fprintf(stream, "Type: %s (0x%X)\n", elf_prog_type_to_str(phdr->type),
		phdr->type);
	fprintf(stream, "Flags: 0x%X\n", phdr->flags);
	fprintf(stream, "Offset: 0x%lX\n", phdr->offset);
	fprintf(stream, "Vaddr: 0x%lX\n", phdr->vaddr);
	fprintf(stream, "Paddr: 0x%lX\n", phdr->paddr);
	fprintf(stream, "File Size: %lu\n", phdr->filesz);
	fprintf(stream, "Mem Size: %lu\n", phdr->memsz);
	fprintf(stream, "Align: 0x%lX\n", phdr->align);
}

void elf_prog_header_list_dump(const struct elf_prog_header_list *hdrs,
			       FILE *stream)
{
	fprintf(stream, "Number of Program Headers: %u\n", hdrs->count);
	switch (hdrs->bits) {
	case ELF_BITS_32:
		ELF_PROG_HEADER_DUMP(hdrs->elf32_prog_headers, hdrs->count,
				     elf32_prog_header_dump, stream);
		break;
	case ELF_BITS_64:
		ELF_PROG_HEADER_DUMP(hdrs->elf64_prog_headers, hdrs->count,
				     elf64_prog_header_dump, stream);
		break;
	default:
		errno = EINVAL;
	}
}

struct elf_file *elf_read_file(const char *const file_path)
{
	int error = 0;
	struct elf_file *f = NULL;
	bool fail = false;

	f = calloc(1, sizeof(struct elf_file));
	if (f == NULL) {
		error = errno;
		fail = true;
		goto out;
	}

	f->fd = open(file_path, O_RDONLY);
	if (f->fd < 0) {
		error = errno;
		fail = true;
		goto out;
	}

	if (elf_read_header(f)) {
		error = errno;
		fail = true;
		goto out;
	}

	elf_header_endian_to_host(f);

	if (ELF_IDENT_CLASS_32_BIT(f->elf_ident)) {
		elf32_read_prog_header(f);
	} else if (ELF_IDENT_CLASS_64_BIT(f->elf_ident)) {
		elf64_read_prog_header(f);
	} else {
		error = EINVAL;
		fail = true;
		goto out;
	}

	elf_prog_header_list_endian_to_host(f);

out:
	if (fail) {
		elf_release_file(f);
		f = NULL;
	}
	errno = error;
	return f;
}

int elf_release_file(struct elf_file *file)
{
	int ret = 0;
	int error = 0;

	if (file) {
		if (close(file->fd) != 0) {
			ret = -1;
			error = errno;
		}
		SFREE(file->elf_header);
		SFREE(file->elf_prog_headers.elf_prog_headers);
		SFREE(file);
	}

	errno = error;
	return ret;
}

void elf_file_dump(const struct elf_file *file, FILE *stream)
{
	if (!stream) {
		stream = stdout;
	}
	fprintf(stream, "===== ELF HEADER =====\n");
	elf_ident_dump(file->elf_ident, stream);
	if (ELF_IDENT_CLASS_32_BIT(file->elf_ident)) {
		elf32_header_dump(file->elf32_header, stream);
	} else if (ELF_IDENT_CLASS_64_BIT(file->elf_ident)) {
		elf64_header_dump(file->elf64_header, stream);
	} else {
		fprintf(stream, "Unsupported ELF class %u\n",
			file->elf_ident->class);
	}

	fprintf(stream, "===== ELF PROGRAM HEADERS =====\n");
	elf_prog_header_list_dump(&file->elf_prog_headers, stream);
}
