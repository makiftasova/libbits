#! /usr/bin/env python3

"""pyspeedserver.py: A simple data generator for speed-test needs"""

import argparse
import http.server
import ipaddress
import logging
import socketserver
import sys
import typing
import urllib.parse
import time
import random

__author__ = "Mehmet Akif Tasova"
__copyright__ = "Copyright 2025, Mehmet Akif Tasova"
__license__ = "BSD-3-Clause"
__version__ = "1.2.0"
__maintainer__ = "Mehmet Akif Tasova"
__email__ = "makiftasova@gmail.com"


def setup_logger(name, log_level=logging.INFO, file: str = None,
                 stream: typing.TextIO = sys.stderr) -> logging.Logger:
    """
    Configure logger
    """

    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')

    _logger = logging.getLogger(name)
    _logger.setLevel(log_level)

    stream_handler = logging.StreamHandler(stream=stream)
    stream_handler.setFormatter(formatter)
    _logger.addHandler(stream_handler)

    if file:
        file_handler = logging.FileHandler(file, mode='w')
        file_handler.setFormatter(formatter)
        _logger.addHandler(file_handler)

    return _logger


logger = setup_logger(sys.argv[0])


def check_port(port: int) -> int:
    """
    Check if given input is a valid port number
    """
    try:
        iport = int(port)
        if 0 < iport < 65536:
            return iport
        raise ValueError(f"{port} is not a valid port number")
    except ValueError as exc:
        raise argparse.ArgumentTypeError(
            f"{port} is not a valid port number") from exc


def check_ip(addr: str) -> str:
    """
    Check if given ipaddr is parseable by inet_aton
    """
    try:
        saddr = str(addr)
        ipaddress.ip_address(saddr)
        return saddr
    except ValueError as exc:
        raise argparse.ArgumentTypeError(
            f"{addr} is not a valid IPv4 or IPv6 address") from exc


class ThreadedHTTPServer(socketserver.ForkingMixIn, http.server.HTTPServer):
    """
    Simple threaded HTTP server
    """


class HTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    """
    Simple request handler for HTTP GET, POST, and PUT
    """
    real_path = ""
    parsed_params = {}
    client_id = ""
    nbytes = 0
    duration = 0
    content_length = 0
    MAX_CHUNK_LEN = 32768
    server_version = "pyspeedserver/" + __version__

    def log_message(self, fmt, *args):
        """
        Logger to log info messages
        """
        logger.info(fmt, *args)

    def log_error(self, fmt, *args):
        """
        Logger to log error messages
        """
        logger.error(fmt, *args)

    def log_warning(self, fmt, *args):
        """
        Logger to log warning messages
        """
        logger.warning(fmt, *args)

    def _send_http_error(self, http_code: int, message: str) -> bool:
        """TODO: Docstring for _send_response.

        :http_code: TODO
        :message: TODO
        :returns: TODO

        """
        self.send_response(http_code, message)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Content-Length', len(message))
        self.end_headers()
        self.wfile.write(message.encode('utf-8'))
        return True

    def _set_http_header_content_length(self, http_code: int, content_length: int) -> bool:
        self.send_response(http_code)
        self.send_header('Content-Type', 'application/octet-stream')
        self.send_header('Content-Length', content_length)
        self.end_headers()

    def _set_http_header_chunked_data(self, http_code: int) -> bool:
        self.send_response(http_code)
        self.send_header('Content-Type', 'application/octet-stream')
        self.send_header('Transfer-Encoding', 'chunked')
        self.end_headers()

    def _param_not_exists(self, name: str, send_error: bool = False) -> bool:
        """
        Check if parameter exists in lsit of parameters, and return HTTP 400 if it is missing
        """
        if name not in self.parsed_params:
            if send_error:
                msg = f"Parameter {name} is missing"
                self._send_http_error(400, msg)
            return True
        return False

    def _try_get_client_id(self) -> bool:
        """
        Try reading client id
        if return value is False, it means we failed to read client id
        """
        if self._param_not_exists('id', True):
            self.client_id = (
                    f'{self.client_address[0]}'
                    f'_{int(time.time())}'
                    f'_{random.randint(0, 10000)}'
                )
            self.log_warning(
                "client id was missing, now using %s", self.client_id)
            return True

        self.client_id = self.parsed_params['id'][0]
        return True

    def _try_get_duration(self) -> bool:
        """
        Try reading test duration
        if return value is False, it means we failed
        """
        if self._param_not_exists('duration'):
            self.log_error("duration missing")
            return False

        try:
            _duration = int(self.parsed_params['duration'][0])
        except ValueError as exc:
            self.log_error("Cannot read duration %s", exc)
            return False

        if _duration < 0:
            self.log_error("Duration %d cannot be negative value", _duration)
            return False

        self.duration = _duration
        return True

    def _try_get_nbytes(self) -> bool:
        """
        Try reading number of bytes parameter as int.
        if return value is False, it means we failed
        """
        if self._param_not_exists('length'):
            self.log_error("length missing")
            return False

        try:
            _nbytes = int(self.parsed_params['length'][0])
        except ValueError as exc:
            self.log_error("Cannot read length %s", exc)
            return False

        self.nbytes = _nbytes
        return True

    def _parse_get_params(self) -> bool:
        """
        Parse HTTP GET query string and return variables in a dictionary
        """
        have_length = False
        have_duration = False

        parsed = urllib.parse.urlparse(self.path)
        self.real_path = parsed.path
        self.parsed_params = urllib.parse.parse_qs(parsed.query)

        self.log_message("Parsed path: %s -- parameters %s",
                         self.real_path, self.parsed_params)

        if not self._try_get_client_id():
            return False

        if self._try_get_nbytes():
            have_length = True

        if self._try_get_duration():
            have_duration = True

        if have_duration and have_length:
            self._send_http_error(
                400, "duration and length cannot be used at the same time")
            return False

        if not have_duration and not have_length:
            self._send_http_error(
                400, "neither duration nor length is specified")
            return False

        return True

    def _parse_put_post_params(self) -> bool:
        """
        Parse HTTP PUT query string and return variables in a dictionary
        """
        parsed = urllib.parse.urlparse(self.path)
        self.real_path = parsed.path
        self.parsed_params = urllib.parse.parse_qs(parsed.query)

        self.log_message("Parsed path: %s -- parameters %s",
                         self.real_path, self.parsed_params)

        if not self._try_get_client_id():
            return False

        return True

    def _check_content_length(self) -> bool:
        try:
            self.content_length = int(self.headers['Content-Length'])
        except ValueError as exc:
            msg = "Content-Length is not a number"
            self._send_http_error(405, msg)

            self.log_message("Client %s send invalid Content-Length %s: %s",
                             self.client_id, self.headers['Content-Length'], exc)
            return False
        return True

    def _send_data_size_based(self, max_chunk_len: int = MAX_CHUNK_LEN) -> None:
        chunk_len = self.nbytes if self.nbytes < max_chunk_len else max_chunk_len
        count = self.nbytes // chunk_len
        leftover_nbytes = self.nbytes % chunk_len
        chunk = b'\xFF' * chunk_len

        self.log_message("Sending %i chunks of %i bytes and %i extra bytes to client %s",
                         count, chunk_len, leftover_nbytes, self.client_id)

        self._set_http_header_content_length(200, self.nbytes)

        for _ in range(count):
            self.wfile.write(chunk)

        if leftover_nbytes:
            self.wfile.write(b'\xFF' * leftover_nbytes)

    def _send_data_time_based(self, chunk_len: int = MAX_CHUNK_LEN) -> None:
        # In 'Transfer-Encoding: chunked' expected chunk format is as follows
        # {size_of_chunk_hexadecimal}\r\n{chunk_data}\r\n
        # Also last chunk sent should be a zero length one to indicate end of
        # transfer
        chunk = f"{chunk_len:0x}\r\n".encode(
            'utf-8') + (b'\xFF' * chunk_len) + "\r\n".encode('utf-8')
        last_chunk = '0\r\n\r\n'.encode('utf-8')

        self.log_message("Sending chunks of %i bytes to client %s for %i second(s)",
                         chunk_len, self.client_id, self.duration)

        self._set_http_header_chunked_data(200)

        time_end = time.time() + self.duration
        while time.time() < time_end:
            self.wfile.write(chunk)
        self.wfile.write(last_chunk)

    def _send_data(self, max_chunk_len: int = MAX_CHUNK_LEN) -> None:
        """
        Send data to client
        """
        if self.nbytes > 0:
            self.log_message("Try sending %i bytes to client %s",
                             self.nbytes, self.client_id)
            self._send_data_size_based(max_chunk_len)
            self.log_message("Sent %i bytes to client %s",
                             self.nbytes, self.client_id)
            return

        if self.duration > 0:
            self.log_message("Try sending data to client %s for %i seconds",
                             self.client_id, self.duration)
            self._send_data_time_based(max_chunk_len)
            self.log_message("Sent data to client %s for %i seconds",
                             self.client_id, self.duration)
            return

        msg = f"{self.client_id} request does not contain duration or length parameters!!!"
        self.log_error(msg)
        self._send_http_error(400, msg)

    def _read_data(self, max_chunk_len: int = MAX_CHUNK_LEN) -> None:
        """
        Read and dump data sent by client
        """
        self.log_message("Client %s is going to send %i byte(s) of data",
                         self.client_id, self.content_length)

        chunk_len = self.content_length if self.content_length < max_chunk_len else max_chunk_len
        count = self.content_length // chunk_len
        leftover_nbytes = self.content_length % chunk_len

        self.log_message("Reading %i chunks of %i bytes and %i extra bytes from client %s",
                         count, chunk_len, leftover_nbytes, self.client_id)

        for _ in range(count):
            self.rfile.read(chunk_len)

        if leftover_nbytes:
            self.rfile.read(leftover_nbytes)

    def do_HEAD(self):
        """
        Handle HTTP HEAD requests
        """
        if self._parse_get_params() is False:
            self.log_error('Cannot parse HEAD parameters "%s"', self.path)
            return

        if self.nbytes > 0:
            self.log_message("HEAD response to client %s for %i bytes",
                             self.client_id, self.nbytes)
            self._set_http_header_content_length(200, self.nbytes)
            self.log_message("Sent HEAD response to client %s for %i bytes",
                             self.client_id, self.nbytes)
            return

        if self.duration > 0:
            self.log_message("HEAD response to client %s for %i seconds",
                             self.client_id, self.duration)
            self._set_http_header_chunked_data(200)
            self.log_message("Sent HEAD response to client %s for %i seconds",
                             self.client_id, self.duration)
            return

        self.log_error("Execution should not reach here!!!")
        self._send_http_error(400, "Unsupported Request")

    def do_GET(self):
        """
        Handle HTTP GET requests
        """
        self.log_error("version: {self.server_version}")
        if self._parse_get_params() is False:
            self.log_error('Cannot parse GET parameters "%s"', self.path)
            return

        self._send_data()

    def do_PUT(self):
        """
        Handle HTTP PUT requests
        """
        if self._parse_put_post_params() is False:
            self.log_error('Cannot parse PUT parameters "%s"', self.path)
            return

        if self._check_content_length() is False:
            self.log_error(
                'Unexpected Content-Length data from client %s', self.client_id)
            return

        self._read_data()

        self.send_response(200)
        self.end_headers()

    def do_POST(self):
        """
        Handle HTTP POST requests
        """
        if self._parse_put_post_params() is False:
            self.log_error('Cannot parse POST parameters "%s"', self.path)
            return

        if self._check_content_length() is False:
            self.log_error(
                'Unexpected Content-Length data from client %s', self.client_id)
            return

        self._read_data()

        self.send_response(200)
        self.end_headers()


def main():
    """
    One function to run them all!!!
    """
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--port', '-p', default=8080, metavar='PORT',
                           type=check_port, help='Specify port to listen [default: 8080]')
    argparser.add_argument('--address', '-a', default='0.0.0.0', metavar='IP_ADDRESS',
                           type=check_ip, help='IP Adress of interface to bind [default: 0.0.0.0]')
    args = argparser.parse_args()

    server = ThreadedHTTPServer((args.address, args.port), HTTPRequestHandler)
    logger.info("Listening on %s port %u", args.address, args.port)
    logger.info("CTRL-C to exit")
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass


if "__main__" == __name__:
    main()
