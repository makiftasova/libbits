/**
 * @file sieve.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Dec 20, 2019
 * @brief An implementation of Sieve of Eratosthenes
 *
 * see https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#define print_error(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)

typedef unsigned long long ull_t;

enum prime_status {
	PRM_U = 0, /* prime status is unknown */
	PRM_N = 1, /* number is not a prime number */
	PRM_Y = 2, /* number is a prime number */
};

struct sieve_elem {
	ull_t num;
	enum prime_status is_prime;
};

ssize_t sieve_elem_print(const struct sieve_elem *elem)
{
	if (!elem) {
		return 0;
	}

	return printf("%llu - %s", elem->num,
		      elem->is_prime == PRM_Y ? "PRIME" : "NOT-PRIME");
}

ssize_t sieve_elem_list_print(const struct sieve_elem *arr, const ull_t nitems)
{
	ssize_t nbytes = 0;

	if (!arr || !nitems) {
		return 0;
	}

	for (ull_t i = 0; i < nitems; ++i) {
		nbytes += sieve_elem_print(&arr[i]);
	}

	return nbytes;
}

ssize_t sieve_of_eratosthenes(const ull_t end, ull_t *primes, ull_t nprimes)
{
	struct sieve_elem *elems = NULL;
	const ull_t num_elems = end - 1; /* prime numbers starts from 2 */
	ull_t num_primes = 0;
	ssize_t ret = -1;
	ull_t p_ind = 0;
	ull_t p = 2;

	if (end < 2) {
		errno = EINVAL;
		goto out;
	}

	elems = calloc(num_elems, sizeof(*elems));
	if (!elems) {
		perror("failed to allocate memory");
		goto out;
	}

	/* initialize the list */
	for (ull_t i = 0; i < num_elems; ++i) {
		elems[i].num = i + 2; /* start list from 2 */
		elems[i].is_prime = PRM_U;
	}

	/*
	 * if given range is too small, simply fill results of already
	 * known prime numbers
	 */
	if (end < 4) {
		for (ull_t i = 0; i < num_elems; ++i) {
			if (elems[i].num < 4) {
				elems[i].is_prime = PRM_Y;
			}
		}
		goto skip_sieve;
	}

	/* mark 2 as prime since we are pretty sure that 2 is a prime number */
	elems[p_ind].is_prime = PRM_Y;

	while (p < num_elems) {
		int cont = 0;
		ull_t i = 0;
		/* mark non-primes */
		for (i = p_ind + p; i < num_elems; i += p) {
			if (elems[i].num % p == 0) {
				elems[i].is_prime = PRM_N;
			}
		}

		/* update p */
		for (i = p_ind + 1; i < num_elems; ++i) {
			if (elems[i].is_prime == PRM_U) {
				p = elems[i].num;
				p_ind = i;
				elems[i].is_prime = PRM_Y;
				++num_primes;
				break;
			}
		}

		/* check if any non-marked number left on sieve */
		cont = 0;
		for (; i < num_elems; ++i) {
			if (elems[i].is_prime == PRM_U) {
				cont = 1;
				break;
			}
		}
		if (!cont) {
			break;
		}
	}

skip_sieve:
	if (primes && nprimes < num_primes) {
		errno = ENOBUFS;
		goto out;
	}

	/* fill primes to given array */
	if (primes) {
		for (ull_t i = 0, j = 0; i < num_elems && j < num_primes; ++i) {
			if (elems[i].is_prime == PRM_Y) {
				primes[j] = elems[i].num;
				++j;
			}
		}
	}

	ret = num_primes;

out:
	SFREE(elems);
	return ret;
}

ssize_t ull_array_dump(const ull_t *arr, const ull_t len)
{
	ssize_t nbytes = 0;
	for (ull_t i = 0; i < len; ++i) {
		nbytes += printf("%llu\n", arr[i]);
	}
	return nbytes;
}

void usage(const char *const exec)
{
	printf("%s - calculate prime numbers up to given number\n", exec);
	printf("usage:\n\t%s NUMBER\n", exec);
	printf("\t%s -e NUMBER\n", exec);
	printf("\t%s -h\n", exec);
	puts("parameters:");
	puts("\t-h\t\t: shows this help message");
	puts("\t-e NUMBER\t: set end of number range to given NUMBER");
}

int main(int argc, char *argv[])
{
	const char *number_str = NULL;
	ull_t *primes = NULL;
	ssize_t nprimes = 0;
	int end_set = 0;
	ull_t end = 0;
	int opt = 0;

	while ((opt = getopt(argc, argv, "he:")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
			break;
		case 'e':
			number_str = optarg;
			errno = 0;
			end = strtoull(optarg, NULL, 10);
			if (errno) {
				perror("Invalid number");
				return EXIT_FAILURE;
			}
			end_set = 1;
			break;
		default:
			print_error("Unknown option %c: %s\n", opt,
				    strerror(EINVAL));
			/* fallthrough */
		case '?':
			usage(argv[0]);
			return EXIT_FAILURE;
			break;
		}
	}

	if (!end_set) {
		if (optind >= argc) {
			print_error("Expected range end NUMBER\n");
			usage(argv[0]);
			return EXIT_FAILURE;
		}
		number_str = argv[optind];
		errno = 0;
		end = strtoull(argv[optind], NULL, 10);
		if (errno) {
			perror("Invalid number");
			return EXIT_FAILURE;
		}
		end_set = 1;
	}

	if (end_set && end < 2) {
		print_error("Invalid number \"%s\": %s\n", number_str,
			    strerror(EINVAL));
		return EXIT_FAILURE;
	}

	primes = calloc(end, sizeof(*primes));
	if (!primes) {
		perror("failed to allocate memory for output buffer");
		return EXIT_FAILURE;
	}

	nprimes = sieve_of_eratosthenes(end, primes, end * sizeof(*primes));
	if (nprimes < 0) {
		perror("sieve_of_eratosthenes failed");
		SFREE(primes);
		return EXIT_FAILURE;
	}

	printf("found %ld primes\n", nprimes);
	ull_array_dump(primes, (const ull_t)nprimes);

	SFREE(primes);

	return EXIT_SUCCESS;
}
