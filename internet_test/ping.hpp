/**
 * @file ping.hpp
 * @brief Common definitions used for ICMP echo makers
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __PING_HPP__
#define __PING_HPP__

#include <chrono>
#include <iostream>
#include <string>
#include <sstream>

namespace netutils
{
class ping_result {
    public:
	enum class state {
		ok = 0,
		timedout,
		unreachable,
		cant_send,
		invalid,
	};

	ping_result();

	ping_result(const std::chrono::microseconds &latency,
		    const std::string &ip, state status = state::ok);

	const std::chrono::microseconds &get_latency(void) const;

	unsigned int get_latency_in_milliseconds(void) const;

	inline bool is_valid(void) const
	{
		return this->_status != state::invalid;
	}

	inline bool is_timedout(void) const
	{
		return this->_status == state::timedout;
	}

	inline bool is_unreachable(void) const
	{
		return this->_status == state::unreachable;
	}

	std::string to_string(void) const;

    private:
	std::chrono::microseconds _latency;
	std::string _ip;
	state _status;
};

std::ostream &operator<<(std::ostream &os, const ping_result &result);
} /* namespace netutils */

#endif /* __PING_HPP__ */
