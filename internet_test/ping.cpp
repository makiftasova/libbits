/**
 * @file ping.cpp
 * @brief Common definitions used for ICMP echo makers
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ping.hpp"

netutils::ping_result::ping_result()
	: _latency(SIZE_MAX)
	, _ip("0.0.0.0")
	, _status(state::invalid)
{
	/* left_blank */
}

netutils::ping_result::ping_result(const std::chrono::microseconds &latency,
				   const std::string &ip, state status)
	: _latency(latency)
	, _ip(ip)
	, _status(status)
{
	/* left_blank */
}

const std::chrono::microseconds &
netutils::ping_result::get_latency(void) const
{
	return this->_latency;
}

unsigned int
netutils::ping_result::get_latency_in_milliseconds(void) const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(
		       this->get_latency())
		.count();
}

std::string netutils::ping_result::to_string(void) const
{
	if (!this->is_valid())
		return "Invalid Ping result";

	std::stringstream ss;

	if (this->_status == state::cant_send) {
		ss << "Couldn't send ICMP ECHO to " << this->_ip;
		return ss.str();
	}

	ss << "Response from " << this->_ip;

	switch (this->_status) {
	case state::ok:
		break;
	case state::timedout:
		ss << " timedout";
		break;
	case state::unreachable:
		ss << " unreachable";
		break;
	case state::invalid:
		[[fallthrough]];
	case state::cant_send: /* handled above */
		break;
	}

	ss << " after " << this->_latency.count() << "μs";

	return ss.str();
}

std::ostream &netutils::operator<<(std::ostream &os, const ping_result &result)
{
	os << result.to_string();
	return os;
}

