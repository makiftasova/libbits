/**
 * @file mqtt_bitmap.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @brief Read bitmaps from JSON and send them over MQTT, up to size of uint64_t.
 *
 * Copyright 2020 Mehmet Akif Tasova
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <endian.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <json-c/json.h>

#include <mosquitto.h>

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 1883
#define DEFAULT_TIMEOUT 60

#define UNUSED(var) ((void)(var))

#define NBITS(nbytes) ((nbytes) * (CHAR_BIT))
#define VAR_NBITS(var) NBITS(sizeof((var)))

void usage(const char *exec)
{
	printf("%s - Convert given json into bitmap and send to MQTT.\n", exec);
	puts("Usage:");
	printf("\t%s [-f FILE] [-t TOPIC] [-i IP] [-p PORT]\n", exec);
	puts("\tConvert given json file into uint64_t bitmap and send to MQTT\n");
	puts("Options:");
	puts("\t-f FILE\t: Path to json file (\"bitmap.json\" if ommitted)");
	puts("\t-t TOPIC\t: MQTT topic to publish data");
	puts("\t-i [IP]\t: IP address of broker");
	puts("\t-p [PORT]\t: Port of broker");
}

int main(int argc, char *argv[])
{
	struct json_object *json_bits = NULL;
	const char *filepath = "bitmap.json";
	struct mosquitto *mosq = NULL;
	const char *ip = DEFAULT_IP;
	const char *topic = NULL;
	uint64_t bitmap_data = 0;
	int port = DEFAULT_PORT;
	size_t bit_index = 0;
	uint64_t num_bits = 0;
	int mosq_ret = 0;
	char opt = '\0';
	int ret = 0;

	while ((opt = getopt(argc, argv, "hf:t:i:p:")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
		case 'f':
			filepath = optarg;
			break;
		case 't':
			topic = optarg;
			break;
		case 'i':
			ip = optarg;
			break;
		case 'p':
			errno = 0;
			port = strtol(optarg, NULL, 10);
			if (errno != 0) {
				printf("invalid port %s\n", optarg);
				usage(argv[0]);
				return EXIT_FAILURE;
			}
			errno = 0;
			break;
		default:
			printf("invalid parameter %c\n", opt);
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (!topic) {
		puts("MQTT topic is missing.");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	json_bits = json_object_from_file(filepath);
	if (!json_bits) {
		printf("Failed to parse JSON file: %s", strerror(errno));
		return EXIT_FAILURE;
	}

	num_bits = (uint64_t)json_object_object_length(json_bits);
	if (num_bits > VAR_NBITS(bitmap_data)) {
		printf("WARN: given array of bits if longer than %lu "
		       "truncating the array\n",
		       VAR_NBITS(bitmap_data));
	}

	/* read json and fill into uint64_t */
	json_object_object_foreach(json_bits, obj_key, obj_val)
	{
		int val = json_object_get_int(obj_val);
		uint64_t bit = val == 0 ? 0 : 1;
		UNUSED(obj_key);
		bitmap_data |= (bit << bit_index);
		++bit_index;
	}

	/* cleanup json object */
	json_object_put(json_bits);
	json_bits = NULL;

	/* init libmosquitto and connect to broker */
	mosquitto_lib_init();

	mosq = mosquitto_new(NULL, true, NULL);
	if (!mosq) {
		printf("mosquitto_new failed: %s\n", strerror(errno));
		ret = EXIT_FAILURE;
		goto out;
	}

	mosq_ret = mosquitto_connect(mosq, ip, port, DEFAULT_TIMEOUT);
	if (mosq_ret != MOSQ_ERR_SUCCESS) {
		printf("mosquitto_connect failed with code %d\n", mosq_ret);
		ret = EXIT_FAILURE;
		goto out;
	}

	/* publish bitmap to MQTT */
	mosq_ret = mosquitto_publish(mosq, NULL, topic, sizeof(bitmap_data),
				     &bitmap_data, 0, 0);
	if (mosq_ret != MOSQ_ERR_SUCCESS) {
		printf("mosquitto_publish faield with code %d\n", mosq_ret);
		ret = EXIT_FAILURE;
		goto out;
	}

out:
	/* cleanup */
	if (mosq) {
		mosquitto_disconnect(mosq);
		mosquitto_destroy(mosq);
	}
	mosquitto_lib_cleanup();
	return ret;
}
