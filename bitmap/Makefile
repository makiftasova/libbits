#
# @file Makefile
# @author Mehmet Akif Tasova <makiftasova@gmail.com>
# @date Nov 21, 2019
# @brief Makefile for bitmap library written in C89
#
# Licensed under 3-clause BSD license
#
# Copyright (c) 2019, Mehmet Akif Tasova.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the Copyright Holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

CC ?= gcc
RM ?= rm -f
PREFIX ?= /usr

CFLAGS-y  := -std=c89 -Wall -Wextra -Wpedantic -Wshadow -Werror
CFLAGS-$(DEBUG)   += -ggdb -g3 -O0
CFLAGS-$(RELEASE) += -O3 -DNDEBUG

LDFLAGS-y :=
LDFLAGS-$(DEBUG)   +=
LDFLAGS-$(RELEASE) +=

TARGET  := bitmap
SOURCES := main.c bitmap.c
OBJECTS := $(SOURCES:.c=.o)
CFLAGS  := $(CFLAGS-y)
LDFLAGS := $(LDFLAGS-y)

SHARED_TARGET := lib$(TARGET).so

LIB_PREFIX := $(PREFIX)/lib
BIN_PREFIX := $(PREFIX)/bin

.PHONY: all clean

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

build: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) $(LDFLAGS) -o $(TARGET)

shared: $(OBJECTS)
	$(CC) -shared $(CFLAGS) $(TARGET).o -o $(SHARED_TARGET)

all: build

doc:
	doxygen Doxyfile

doc_clean:
	$(RM) -r doc

install:
	mkdir -p $(BIN_PREFIX)
	install --mode=755 $(TARGET) $(BIN_PREFIX)/$(TARGET)

uninstall:
	$(RM) $(BIN_PREFIX)/$(TARGET)

clean: doc_clean
	$(RM) $(TARGET) $(SHARED_TARGET) $(OBJECTS)

