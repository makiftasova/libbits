/**
 * @file bitmap.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @brief A simple bitmap viewer which uses SDL2
 *
 * Copyright 2019 Mehmet Akif Tasova
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <SDL2/SDL.h>

#define __PACKED __attribute__((__packed__))

#define SCLOSE(fd)                   \
	do {                         \
		if ((fd) > -1) {     \
			close((fd)); \
			(fd) = -1;   \
		}                    \
	} while (0)

#define BITMAP_MARKER_BM "BM" //{0x42, 0x4D}
#define BITMAP_MARKER_BA "BA" //{0x42, 0x41}
#define BITMAP_MARKER_CI "CI" //{0x43, 0x49}
#define BITMAP_MARKER_CP "CP" //{0x43, 0x50}
#define BITMAP_MARKER_IC "IC" //{0x49, 0x43}
#define BITMAP_MARKER_PT "PT" //{0x50, 0x54}

#define BITMAP_COMP_RGB 0 /* RGB pixels with no alpha */
#define BITMAP_COMP_RLE8 1 /* Run-Length Encoding (RLE) 8-bits */
#define BITMAP_COMP_RLE4 2 /* Run-Length Encoding (RLE) 4-bits */
#define BITMAP_COMP_BITFIELDS 3
#define BITMAP_COMP_JPEG 4
#define BITMAP_COMP_PNG 5
#define BITMAP_COMP_ALPHABITFIELDS 6 /* RGBA masks */
#define BITMAP_COMP_CMYK 11
#define BITMAP_COMP_CMYLRLE8 12 /* CMYK with RLE8 */
#define BITMAP_COMP_CMYKRLE4 13 /* CMYK with RLE4 */

struct bitmap_header {
	union {
		uint8_t marker[2];
		uint16_t marker_b;
	};
	uint32_t size;
	uint16_t reserved0;
	uint16_t reserved1;
	uint32_t data_offset;
} __PACKED;

struct bitmap_info_header {
	uint32_t header_size;
	uint32_t width;
	uint32_t height;
	uint16_t planes; /* number of color planes */
	uint16_t bits_per_pixels;
	uint32_t compression;
	uint32_t img_size;
	uint32_t pixels_per_meter_x;
	uint32_t pixels_per_meter_y;
	uint32_t num_colors;
	uint32_t num_important_colors;
} __PACKED;

struct bitmap_pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
} __PACKED;

struct bitmap_file {
	struct bitmap_header header;
	struct bitmap_info_header infoheader;
} __PACKED;

typedef struct bitmap {
	union {
		struct bitmap_file *file;
		void *mem;
	};
	union {
		uint8_t *data_begin;
		struct bitmap_pixel *pixels;
	};
	uint64_t num_pixels;
	size_t file_size;
	uint32_t data_size;
} bitmap_t;

int bitmap_close(bitmap_t *bitmap)
{
	if (!bitmap) {
		return 0;
	}

	if (bitmap->mem) {
		(void)munmap(bitmap->mem, bitmap->file_size);
	}
	free(bitmap);
	return 0;
}

bitmap_t *bitmap_open(const char *const path)
{
	int error = 0;
	int fd = -1;
	struct stat statbuf;
	void *mem = NULL;
	bitmap_t *bitmap = NULL;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		error = errno;
		goto fail;
	}

	if (fstat(fd, &statbuf) < 0) {
		error = errno;
		goto fail;
	}

	bitmap = calloc(1, sizeof(*bitmap));
	if (!bitmap) {
		error = errno;
		goto fail;
	}

	mem = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (bitmap->mem == MAP_FAILED) {
		error = errno;
		goto fail;
	}

	bitmap->mem = mem;
	bitmap->file_size = statbuf.st_size;
	bitmap->data_size = bitmap->file->header.size;
	bitmap->data_begin = bitmap->mem + bitmap->file->header.data_offset;

	SCLOSE(fd);

	/* TODO use proper check to ensure file is proper bitmap file */
	if (bitmap->data_size != bitmap->file_size) {
		error = EBADF;
		goto fail;
	}

	bitmap->num_pixels = (uint64_t)bitmap->file->infoheader.width *
			     (uint64_t)bitmap->file->infoheader.height;

	return bitmap;

fail:
	SCLOSE(fd);
	bitmap_close(bitmap);
	errno = error;
	return NULL;
}

void bitmap_print(const bitmap_t *bitmap)
{
	printf("marker = %c%c\n", bitmap->file->header.marker[0],
	       bitmap->file->header.marker[1]);
	printf("file size = %lu\n", bitmap->file_size);
	printf("data size = %u\n", bitmap->data_size);
	printf("mmap begin = %p\n", bitmap->mem);
	printf("data begin = %p\n", bitmap->data_begin);
	printf("data offset = 0x%X\n", bitmap->file->header.data_offset);
	printf("width = %u\n", bitmap->file->infoheader.width);
	printf("height = %u\n", bitmap->file->infoheader.height);
	printf("num pixels = %lu\n", bitmap->num_pixels);
	printf("compression = %u\n", bitmap->file->infoheader.compression);
}

#define ERR_HANDLE_SDL(retval)          \
	do {                            \
		if ((retval)) {         \
			error = EINVAL; \
			ret = -1;       \
			goto out;       \
		}                       \
	} while (0)

int bitmap_show(const bitmap_t *bitmap)
{
	int ret = 0;
	int error = 0;
	uint32_t w = 0;
	uint32_t h = 0;
	uint32_t img_w = 0;
	uint32_t img_h = 0;
	SDL_Event event;
	SDL_Renderer *renderer = NULL;
	SDL_Window *window = NULL;

	if (!bitmap) {
		errno = EINVAL;
		return -1;
	}

	if (bitmap->file->infoheader.compression != BITMAP_COMP_RGB) {
		errno = ENOTSUP;
		return -1;
	}

	img_w = bitmap->file->infoheader.width;
	img_h = bitmap->file->infoheader.height;

	memset(&event, 0, sizeof(event));

	ERR_HANDLE_SDL(SDL_Init(SDL_INIT_VIDEO));

	ERR_HANDLE_SDL(SDL_CreateWindowAndRenderer(img_w, img_h, 0, &window,
						   &renderer));

	ERR_HANDLE_SDL(SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0));

	ERR_HANDLE_SDL(SDL_RenderClear(renderer));

	for (h = 0; h < img_h; ++h) {
		for (w = 0; w < img_w; ++w) {
			struct bitmap_pixel *pix = NULL;

			pix = &bitmap->pixels[img_w * h + w];

			ERR_HANDLE_SDL(SDL_SetRenderDrawColor(
				renderer, pix->r, pix->g, pix->b, 255));
			/*
			 * bitmap saves pixel data from bottom to top,
			 * but SDL2 draw from top to bottom
			 */
			ERR_HANDLE_SDL(
				SDL_RenderDrawPoint(renderer, w, img_h - h));
		}
	}

	SDL_RenderPresent(renderer);

	while (1) {
		if (SDL_PollEvent(&event) && event.type == SDL_QUIT) {
			break;
		}
	}

out:
	if (renderer) {
		SDL_DestroyRenderer(renderer);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	errno = error;
	return ret;
}
#undef ERR_HANDLE_SDL

void usage(const char *const exec)
{
	printf("%s - show bitmap image\n", exec);
	printf("Usage:\n\t%s FILE\n", exec);
}

int main(int argc, char *argv[])
{
	bitmap_t *bitmap = NULL;

	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	bitmap = bitmap_open(argv[1]);
	if (!bitmap) {
		perror("failed to open bitmap file");
		return 2;
	}

	bitmap_print(bitmap);

	for (int i = 0; i < 10; ++i) {
		printf("0x%02X 0x%02X 0x%02X\n", bitmap->pixels[i].b,
		       bitmap->pixels[i].g, bitmap->pixels[i].r);
	}

	if (bitmap_show(bitmap)) {
		perror("can not display bitmap image");
	}

	bitmap_close(bitmap);

	return 0;
}
