/**
 * @file main.c
 * @brief Example usage of a simple bitmap library written in C89
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Nov 21, 2019
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bitmap.h"

/**
 * @brief Macro for reducing number of lines to write when calling functions.
 */
#define TEST_FN(fn, bitmap, bit)                                 \
	do {                                                     \
		if (fn((bitmap), (bit)) < 0) {                   \
			fprintf(stderr,                          \
				"%d: " #fn "(" #bitmap ", " #bit \
				") failed: %s\n",                \
				__LINE__, strerror(errno));      \
		}                                                \
	} while (0)

/**
 * @brief Function to check if underlying system is little endian or not.
 *
 * This function does not take any arguments.
 *
 * @return If underlying system is little endian, then this function will
 * return 1. If underlying system is not little endian, then this function will
 * return 0.
 */
int is_little_endian(void)
{
	volatile uint32_t i = 0x89ABCDEF;
	return (*((uint8_t *)(&i))) == 0xEF;
}

/**
 * @brief Macroto print endianness of underlying system to stdout
 */
#define PRINT_SYSTEM_ENDIAN()    \
	printf("System is %s\n", \
	       is_little_endian() ? "Little-Endian" : "Big-Endian")

/**
 * @brief Printf given array of bytes into stdout, up to 10 bytes per line, in
 * hexadecimal digits.
 *
 * @param[in] bytes array of bytes to print.
 * @param[in] nbytes of bytes in array.
 *
 * @return no return value.
 */
void print_bytes(const uint8_t *bytes, const size_t nbytes)
{
	size_t i = 0;

	if (bytes == NULL || nbytes == 0) {
		return;
	}

	for (i = 0; i < nbytes; ++i) {
		printf("0x%02X ", bytes[i]);
		if ((i + 1) % 10 == 0) {
			printf("\n");
		}
	}
	printf("\n");
}

/**
 * @brief enrty point for test program.
 *
 * argc and argv arguments are simply ignored.
 *
 * @param[in] argc number of command line arguments
 * @param[in] argv list command line arguments.
 *
 * @return on successful exit, exits with exit code EXIT_SUCCESS. on error
 * prints error message to terminal and exits with exit code EXIT_FAILURE.
 */
int main(int argc, char *argv[])
{
	bitmap_t bmap = BITMAP_FAILED;
	bitmap_t bmap2 = BITMAP_FAILED;
	uint8_t *new_raw_data = NULL;
	size_t new_raw_data_size = 0;
	uint8_t raw_data[4] = { 0x00, 0x00, 0xF0, 0x01 };
	/* bits : 0000 0000 0000 0000 1111 0000 0000 0001 */
	/* index: 0123 4567 8901 2345 6789 0123 4567 8901 */

	(void)argc;
	(void)argv;

	PRINT_SYSTEM_ENDIAN();

	bmap = bitmap_create(&raw_data, sizeof(raw_data));
	if (bmap == BITMAP_FAILED) {
		perror("bitmap_create(raw_data) failed");
		goto fail;
	}

	puts("bitmap_create(raw_data): OK");

	bmap2 = bitmap_duplicate(bmap);
	if (bmap == BITMAP_FAILED) {
		perror("bitmap_duplicate(bmap) failed");
		goto fail;
	}

	puts("bitmap_duplicate(bmap): OK");

	puts("bmap pre-op:");
	if (bitmap_dump_bits(bmap) < 0) {
		perror("bitmap_dump_bits(bmap) failed");
		goto fail;
	}

	TEST_FN(bitmap_get_bit, bmap, 123);

	TEST_FN(bitmap_set_bit, bmap, 0);

	TEST_FN(bitmap_set_bit, bmap, 11);

	TEST_FN(bitmap_set_bit, bmap, 31);

	TEST_FN(bitmap_set_bit, bmap, 32);

	TEST_FN(bitmap_clear_bit, bmap, 24);

	puts("bmap post-op:");
	if (bitmap_dump_bits(bmap) < 0) {
		perror("bitmap_dump_bits(bmap) failed");
		goto fail;
	}

	puts("bmap2:");
	if (bitmap_dump_bits(bmap2) < 0) {
		perror("bitmap_dump_bits(bmap2) failed");
		goto fail;
	}

	new_raw_data = bitmap_get_bytes(bmap, &new_raw_data_size);
	if (new_raw_data == NULL) {
		perror("bitmap_get_bytes(bmap) failed");
		goto fail;
	}
	puts("get bytes from bmap: OK");

	bitmap_destroy(bmap);
	puts("bitmap_destroy(bmap): OK");

	bitmap_destroy(bmap2);
	puts("bitmap_destroy(bmap2): OK");

	puts("bytes from deleted bmap:");
	print_bytes(new_raw_data, new_raw_data_size);

	free(new_raw_data);

	puts("TEST OK");

	return EXIT_SUCCESS;

fail:
	bitmap_destroy(bmap);
	bitmap_destroy(bmap2);
	if (new_raw_data != NULL) {
		free(new_raw_data);
	}
	puts("TEST FAIL");
	return EXIT_FAILURE;
}
