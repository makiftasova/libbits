# Dependencies
* json-c (libjson-c) 0.14.0 or newer.
* mosquitto (libmosquitto) 1.5.7 or newer.

# Example JSON
File `bitmap.json` is an example input file. When given example `bitmap.json`
file is used as input file for `mqtt_bitmap`, `mosquitto_sub`'s output should be
`ABDEFG`.
