/**
 * @file iface.hpp
 * @brief A class to check a network interface's status
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __IFACE_HPP__
#define __IFACE_HPP__

#include <chrono>
#include <string>
#include <vector>

#include <net/if.h>

#include "common.hpp"

namespace netutils
{
class iface {
    public:
	// clang-format off
	enum flag {
		unknown = 		0x0000,			/* No flags set */
		up =			IFF_UP,			/* Interface is up.  */
		broadcast =		IFF_BROADCAST,		/* Broadcast address valid.  */
		debug =			IFF_DEBUG,		/* Turn on debugging.  */
		loopback =		IFF_LOOPBACK,		/* Is a loopback net.  */
		point_to_point =	IFF_POINTOPOINT,	/* Interface is point-to-point link.  */
		no_trailers =		IFF_NOTRAILERS,		/* Avoid use of trailers.  */
		running =		IFF_RUNNING,		/* Resources allocated.  */
		no_arp =		IFF_NOARP,		/* No address resolution protocol.  */
		promiscus =		IFF_PROMISC,		/* Receive all packets.  */
		all_multicast =		IFF_ALLMULTI,		/* Receive all multicast packets.  */
		master =		IFF_MASTER,		/* Master of a load balancer.  */
		slave =			IFF_SLAVE,		/* Slave of a load balancer.  */
		multicast =		IFF_MULTICAST,		/* Supports multicast.  */
		portsel =		IFF_PORTSEL,		/* Can set media type.  */
		auto_media =		IFF_AUTOMEDIA,		/* Auto media select active.  */
		dynamic =		IFF_DYNAMIC,		/* Dialup device with changing addresses.  */
	};
	//clang-format on

	static const std::chrono::milliseconds DEFAULT_VALIDITY_PERIOD; /* 5 seconds */

	iface() = delete;
	iface(const std::string& ifname, const std::chrono::milliseconds &validity_period = DEFAULT_VALIDITY_PERIOD);

	void update_flags(void);

	const std::vector<flag> &get_flags(void);

	bool is_flag_set(const flag to_check) const;

	inline bool is_up(void) const
	{
		return this->is_flag_set(flag::up);
	}

	inline bool is_running(void) const
	{
		return this->is_flag_set(flag::running);
	}

	/* check if interface is usable for internet connection */
	inline bool is_active(void) const
	{
		return this->is_up() && this->is_running();
	}

	std::string to_string(void) const;

    private :
	std::string _ifname;
	std::chrono::milliseconds _validity_period;
	mutable std::vector<flag> _flags;
	mutable std::chrono::system_clock::time_point _last_update;

	bool _flags_needs_update(void) const;

	void _update_flags(void) const;
};

std::ostream &operator<<(std::ostream &os, const iface &iface);

} /* namespace netutils */

#endif /* __IFACE_HPP__ */
