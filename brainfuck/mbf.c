/**
 * @file mbf.c
 * @brief Yet Another brainfuck impelementataion.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Jan 23, 2017
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (C) 2017, Mehmet Akif Tasova
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#define ERR_NO_FILE 1
#define ERR_OPEN_FILE 2
#define ERR_BUFF_ALLOC_FAIL 3
#define ERR_PRG_MEM_ALLOC_FAIL 4
#define ERR_BUFF_OVERFLOW 5
#define ERR_BUFF_UNDERFLOW 6

#define BF_BUFFER_LENGTH 32768 /* 2^15 */

void usage(const char *prg_name);

bool file_exists(const char *path);

size_t file_length(FILE *file);

int main(int argc, char **argv)
{
	char cmd = '\0';
	int8_t ret = 0;
	size_t prg_len = 0, loop = 0;
	char *prg_ptr = NULL, *prg_mem = NULL, *prg_mem_end = NULL;
	int8_t *bf_buffer = NULL, *bf_buffer_end = NULL, *bf_ptr = NULL;
	FILE *src = NULL;

	if (argc != 2) {
		usage(argv[0]);
		return 0;
	}

	if (!file_exists(argv[1])) {
		fprintf(stderr, "Could not open: %s: File not found!\n",
			argv[1]);
		return -ERR_NO_FILE;
	}

	src = fopen(argv[1], "r");
	if (NULL == src) {
		fprintf(stderr, "Failed to open %s: %s\n", argv[1],
			strerror(errno));
		return -ERR_OPEN_FILE;
	}

	prg_len = file_length(src);
#ifdef DEBUG
	fprintf(stderr, "[DBG] prg_len: %lu\n", prg_len);
#endif
	prg_mem = (char *)calloc(sizeof(char) + 1, prg_len);
	if (NULL == prg_mem) {
		fprintf(stderr, "Failed to alloca memory for program: %s\n",
			strerror(errno));
		return -ERR_PRG_MEM_ALLOC_FAIL;
	}

	prg_mem_end = prg_mem + prg_len;
	prg_ptr = prg_mem;
	while ((cmd = fgetc(src)) != EOF) {
		*prg_ptr = cmd;
		++prg_ptr;
	}
	fclose(src);
	src = NULL;
#ifdef DEBUG
	fprintf(stderr, "[DBG] prg: \"%s\"\n", prg_mem);
#endif

	prg_ptr = prg_mem;

	bf_buffer = (int8_t *)calloc(sizeof(int8_t), BF_BUFFER_LENGTH);

	if (NULL == bf_buffer) {
		fprintf(stderr, "Failed to allocate buffer: %s\n",
			strerror(errno));
		ret = -ERR_BUFF_ALLOC_FAIL;
		goto free_prgmem;
	}

	bf_buffer_end = bf_buffer + BF_BUFFER_LENGTH;
	bf_ptr = bf_buffer;

	while (prg_ptr != prg_mem_end) {
		if ('\0' == *prg_ptr)
			break;
#ifdef DEBUG
		fprintf(stderr, "[DBG] current input: %c at %p\n", *prg_ptr,
			prg_ptr);
#endif
		switch (*prg_ptr) {
		case '>':
			++bf_ptr;
			if (bf_ptr == bf_buffer_end) {
				fprintf(stderr, "Buffer overflow!\n");
				ret = -ERR_BUFF_OVERFLOW;
				goto free_buffer;
			}
			break;
		case '<':
			if (bf_ptr == bf_buffer) {
				fprintf(stderr, "Buffer underflow!\n");
				ret = -ERR_BUFF_UNDERFLOW;
				goto free_buffer;
			}
			--bf_ptr;
			break;
		case '+':
			++*bf_ptr;
			break;
		case '-':
			--*bf_ptr;
			break;
		case '.':
			if (*bf_ptr)
				putchar((char)(*bf_ptr));
			break;
		case ',':
			*bf_ptr = (int8_t)getchar();
			break;
		case '[':
			if (*bf_ptr == 0) {
				loop = 1;
				while (loop > 0) {
					++prg_ptr;
					switch (*prg_ptr) {
					case '[':
						++loop;
						break;
					case ']':
						--loop;
						break;
					}
				}
			}
			break;
		case ']':
			loop = 1;
			while (loop > 0) {
				--prg_ptr;
				switch (*prg_ptr) {
				case '[':
					--loop;
					break;
				case ']':
					++loop;
					break;
				}
			}
			loop = 0;
			--prg_ptr;
			break;
		default:
			break;
		}
		++prg_ptr;
		if (prg_ptr == prg_mem_end)
			break;
	}

free_buffer:
	free(bf_buffer);
	bf_buffer = NULL;
free_prgmem:
	free(prg_mem);
	prg_mem = NULL;
	return ret;
}

void usage(const char *prg_name)
{
	printf("Usage:\n\t%s [SOURCE]\n", prg_name);
	printf("\t\tSOURCE : A valid Brainfuck source code\n");
}

bool file_exists(const char *path)
{
	return access(path, F_OK) != 1;
}

size_t file_length(FILE *file)
{
	size_t size = 0;
	fseek(file, 0L, SEEK_END);
	size = ftell(file);
	rewind(file);
	return size;
}
