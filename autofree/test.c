/**
 * @file test.c
 * @author Mehmet Akif TASOVA <makiftasova@gmail.com>
 * @date Jul 05, 2021
 * @brief Some tests for autofree.h
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif TASOVA.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>

#include "autofree.h"

#define UNUSED(__var) ((void)(__var))

struct mystruct {
	int x;
};

typedef struct {
	int x;
} mytypedef_t;

AUTOFREE_USING_INT
AUTOFREE_USING_ULONGLONG
AUTOFREE_USING_CUSTOM_LONG(struct_mystruct, struct mystruct)
AUTOFREE_USING_CUSTOM(mytypedef_t)

static void free_buffer(void *buf)
{
	if (buf) {
		free(buf);
	}
}

void test_int(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
	AUTOFREE_INT(buf1) = NULL;
#pragma GCC diagnostic pop
	int *buf2 = NULL;

	buf1 = calloc(100, sizeof(*buf1));
	buf2 = calloc(100, sizeof(*buf2));

	for (int i = 0; i < 100; ++i) {
		buf1[i] = buf2[i] = i;
	}

	printf("%d - %d\n", buf1[0], buf2[0]);

	free_buffer(buf2);
}

void test_ulonglong(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
	AUTOFREE_ULONGLONG(buf1) = NULL;
#pragma GCC diagnostic pop
	unsigned long long *buf2 = NULL;

	buf1 = calloc(100, sizeof(*buf1));
	buf2 = calloc(100, sizeof(*buf2));

	for (int i = 0; i < 100; ++i) {
		buf1[i] = buf2[i] = i;
	}

	printf("%llu - %llu\n", buf1[0], buf2[0]);

	free_buffer(buf2);
}

void test_mystruct(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
	AUTOFREE_CUSTOM_LONG(struct_mystruct, struct mystruct, buf1) = NULL;
#pragma GCC diagnostic pop
	mytypedef_t *buf2 = NULL;

	buf1 = calloc(100, sizeof(*buf1));
	buf2 = calloc(100, sizeof(*buf2));

	for (int i = 0; i < 100; ++i) {
		buf1[i].x = buf2[i].x = i;
	}

	printf("%d - %d\n", buf1[0].x, buf2[0].x);

	free_buffer(buf2);
}

void test_mytypedef(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
	AUTOFREE_CUSTOM(mytypedef_t, buf1) = NULL;
#pragma GCC diagnostic pop
	mytypedef_t *buf2 = NULL;

	buf1 = calloc(100, sizeof(*buf1));
	buf2 = calloc(100, sizeof(*buf2));

	for (int i = 0; i < 100; ++i) {
		buf1[i].x = buf2[i].x = i;
	}

	printf("%d - %d\n", buf1[0].x, buf2[0].x);

	free_buffer(buf2);
}

void test_custom(void)
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"
	AUTOFREE_INT(buf1) = NULL;
#pragma GCC diagnostic pop
	int *buf2 = NULL;

	buf1 = calloc(100, sizeof(*buf1));
	buf2 = calloc(100, sizeof(*buf2));

	for (int i = 0; i < 100; ++i) {
		buf1[i] = buf2[i] = i;
	}

	printf("%d - %d\n", buf1[0], buf2[0]);

	free_buffer(buf2);
}

int main(int argc, char *argv[])
{
	int test = 0;

	if (argc != 2) {
		return EXIT_FAILURE;
	}

	test = atoi(argv[1]);

	switch (test) {
	case 1:
		test_int();
		break;
	case 2:
		test_ulonglong();
		break;
	case 3:
		test_mystruct();
		break;
	case 4:
		test_mytypedef();
		break;
	case 0:
		printf("could not convert %s to integer\n", argv[1]);
		/* fall-through */
	default:
		printf("not supported: %d\n", test);
		return EXIT_FAILURE;
		break;
	}

	return EXIT_SUCCESS;
}
