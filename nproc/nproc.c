/**
 * @file nproc.c
 * @brief Print number of CPUs available on system
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Sep 15, 2020
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2020, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define EXIT_UNKNOWN_OPT -10
#define EXIT_SYSCONF_FAIL -20

/*
 * TODO: find a way to get number of CPUs available to current process via
 * affinity masks etc.
 */
enum nproc_type {
	ONLINE, /* number of online CPUs */
	CONFIG, /* number of configured CPUs */
};

void usage(const char *const name)
{
	printf("%s - print number of available CPUs\n"
	       "OPTIONS:\n"
	       "\t-c\tprint number of configured CPUs\n"
	       "\t-o\tprint number of online CPUs (default option)\n"
	       "\t-h\tprint this help message\n",
	       name);
}

int main(int argc, char *argv[])
{
	int opt = 0;
	enum nproc_type type = ONLINE;
	long nproc = 0;

	while ((opt = getopt(argc, argv, "cho")) != -1) {
		switch (opt) {
		case 'c':
			type = CONFIG;
			break;
		case 'o':
			type = ONLINE;
			break;
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
		default:
			usage(argv[0]);
			return EXIT_UNKNOWN_OPT;
		}
	}

	errno = 0;

	switch (type) {
	case ONLINE:
		nproc = sysconf(_SC_NPROCESSORS_ONLN);
		break;
	case CONFIG:
		nproc = sysconf(_SC_NPROCESSORS_CONF);
		break;
	}

	if (nproc == -1 && errno != 0) {
		perror("sysconf failed");
		return EXIT_SYSCONF_FAIL;
	}

	printf("%ld\n", nproc);

	return EXIT_SUCCESS;
}
