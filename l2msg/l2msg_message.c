/*
 * @file l2msg_message.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date August 05, 2024
 * @brief Message packer/unpacker for l2msg
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2024, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <netpacket/packet.h>
#include <sys/ioctl.h>

#include "l2msg_common.h"
#include "l2msg_message.h"

#include <stdio.h>

/** Broadcast address for OSI layer 2 */
const struct ether_addr ETHER_BCAST_ADDR = {
	.ether_addr_octet = { 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU }
};

const struct ether_addr ETHER_INVALID_ADDR = {
	.ether_addr_octet = { 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U }
};

typedef struct {
	uint8_t dest[ETH_ALEN]; /* destination eth addr	*/
	uint8_t source[ETH_ALEN]; /* source ether addr	*/
	uint16_t proto; /* packet type ID field	*/
	uint16_t data_len; /* length of data in frame */
} __attribute__((packed)) eth_header_t;

static inline msg_status_e errno_to_msg_status(const int number)
{
	switch (number) {
	case 0:
		return MSG_STATUS_OK;
	case EMSGSIZE:
		return MSG_STATUS_TEXT_TOO_LONG;
	case EAFNOSUPPORT:
		return MSG_STATUS_AFNOTSUPP;
	case EWOULDBLOCK:
		return MSG_STATUS_WOULDBLOCK;
	case ECONNRESET:
		return MSG_STATUS_OTHEREND_CLOSED;
	case EBADF:
	case ENOTSOCK:
		return MSG_STATUS_FD_NOT_VALID;
	case EINTR:
		return MSG_STATUS_INTERRUPTED;
	default:
		fprintf(stderr, "errno = %d\n", number);
		return MSG_STATUS_INVALID;
	}
}

const char *msg_status_to_string(const msg_status_e status)
{
	static const char *reasons[MSG_STATUS_MAX] = {
		[MSG_STATUS_OK] = "Success",
		[MSG_STATUS_INVALID] = "Invalid message_t object",
		[MSG_STATUS_INVALID_MAGIC] = "Invalid magic value",
		[MSG_STATUS_TEXT_TOO_LONG] =
			"Given text message is too long for buffer",
		[MSG_STATUS_LEN_MISMATCH] = "Message length does not match",
		[MSG_STATUS_CRC_MISMATCH] = "CRC32 cehcksum mismatch",
		[MSG_STATUS_INVALID_DEST] = "Invalid destination MAC Address",
		[MSG_STATUS_SEND_FAILED] = "sendto(2) failed",
		[MSG_STATUS_RECV_FAILED] = "recvfrom(2) failed",
		[MSG_STATUS_WRITE_FAILED] = "write(2) failed",
		[MSG_STATUS_READ_FAILED] = "read(2) failed",
		[MSG_STATUS_AFNOTSUPP] = "Address family not supported",
		[MSG_STATUS_FD_NOT_VALID] =
			"Given socket or file descriptor is not valid",
		[MSG_STATUS_INTERRUPTED] = "Operation interrupted by a signal",
		[MSG_STATUS_OTHEREND_CLOSED] =
			"Other end closed the communication line",
		[MSG_STATUS_WOULDBLOCK] =
			"Sending this message would block a non-blocking connection",
		[MSG_STATUS_PARTIAL_DATA_SENT] =
			"Could not transmit whole message",
		[MSG_STATUS_INVALID_IF_INDEX] = "Invalid interface index",
		[MSG_STATUS_CANNOT_GET_IF_MAC] =
			"Cannot get interface MAC address",
		[MSG_STATUS_MALLOC_FAIL] = "Cannot allocate enough memory",
	};

	if (status >= MSG_STATUS_MAX)
		return "Invalid status code";
	return reasons[status];
}

static int get_if_index(int sock, const char *const ifname)
{
	struct ifreq if_idx;
	int ret = -1;

	memset(&if_idx, 0, sizeof(if_idx));
	memccpy(if_idx.ifr_name, ifname, '\0', sizeof(if_idx.ifr_name) - 1);

	errno = 0;
	ret = ioctl(sock, SIOCGIFINDEX, &if_idx);
	if (ret < 0) {
		fprintf(stderr, "Cannot get if index for interface %s: %s\n",
			ifname, strerror(errno));
		return -1;
	}

	return if_idx.ifr_ifindex;
}

static int get_if_mac(int sock, const char *const ifname,
		      struct ether_addr *addr)
{
	struct ifreq if_mac;

	memset(&if_mac, 0, sizeof(if_mac));
	memccpy(if_mac.ifr_name, ifname, '\0', sizeof(if_mac.ifr_name) - 1);

	errno = 0;
	if (ioctl(sock, SIOCGIFHWADDR, &if_mac) != 0) {
		fprintf(stderr,
			"Cannot get if MAC Address for interface %s: %s\n",
			ifname, strerror(errno));
		return -1;
	}

	memcpy(addr, if_mac.ifr_hwaddr.sa_data, sizeof(*addr));
	return 0;
}

#if defined(DEBUG)
static int log_message(FILE *stream, const message_t *const message,
		       const char *text)
{
	char src_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };
	char dst_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };

	/* clang-format off */
	return fprintf(stream,
		       "%s:\n\tmagic: 0x%X\n\tsrc: %s\n\tdst: %s\n\tcrc32: 0x%08X\n\tmsg: '%s'\n\tmsg_len: %" PRIu32 "\n",
		       text ? text : "MESSAGE DUMP",
		       message->magic,
		       ether_ntoa_r(&message->src, src_mac),
		       ether_ntoa_r(&message->dst, dst_mac),
		       message->crc,
		       message->msg,
		       message->msg_len);
	/* clang-format on */
}

static int log_message_bytes(FILE *stream, const message_bytes_t *const data,
			     const char *text)
{
	char dst_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };

	/* clang-format off */
	return fprintf(stream,
		       "%s(%zu):\n\tmagic: 0x%X\n\tdst: %s\n\tcrc32: 0x%08X\n\tmsg: '%s'\n\tmsg_len: %" PRIu32 "\n",
		       text ? text : "MESSAGE DUMP",
		       data->data_len,
		       data->data.magic,
		       ether_ntoa_r(&data->data.dst, dst_mac),
		       data->data.crc,
		       data->data.msg,
		       data->data.msg_len);
	/* clang-format on */
}

static int log_message_bytes_hex(FILE *stream,
				 const message_bytes_t *const data,
				 const char *text)
{
	int nbytes = 0;

	/* clang-format off */
	nbytes = fprintf(stream, "%s (%zu):\n\t",
			 text ? text : "BYTES",
			 data->data_len);
	/* clang-format on */
	for (size_t i = 0; i < data->data_len; ++i) {
		nbytes += fprintf(stream, "%02X ", data->data.bytes[i]);
		if (i && (i % 10) == 9)
			nbytes += fprintf(stream, "\n\t");
	}
	nbytes += fprintf(stream, "\n");

	return nbytes;
}

static int log_raw_buffer(FILE *stream, const uint8_t *buf, const size_t buflen)
{
	int nbytes = 0;
	const eth_header_t *ethhdr = (const eth_header_t *)buf;
	const message_bytes_t *bytes =
		(const message_bytes_t *)(buf + sizeof(*ethhdr));
	char src_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };
	char dst_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };

	/* clang-format off */
	nbytes = fprintf(stream, "TOTAL_LEN: %zu\nHEADER: src: %s -> dst: %s, proto: 0x%04X\ndata_len: %"PRIu16"\n",
			 buflen,
			 ether_ntoa_r((const struct ether_addr *)ethhdr->source, src_mac),
			 ether_ntoa_r((const struct ether_addr *)ethhdr->dest, dst_mac),
			 ethhdr->proto,
			 ethhdr->data_len);
	/* clang-format on */

	nbytes += log_message_bytes_hex(stream, bytes, NULL);

	return nbytes;
}
#endif /* defined(DEBUG) */

msg_status_e message_from_text(message_t *const message, const char *text)
{
	const uint64_t len = strlen(text);
	if (len > MSG_MAX_LEN)
		return MSG_STATUS_TEXT_TOO_LONG;

	memset(message, 0, sizeof(*message));
	message->magic = MESSAGE_MAGIC;
	memccpy(message->msg, text, '\0', len);
	message->msg_len = len;
	message->crc =
		crc32(0, (const unsigned char *)message->msg, message->msg_len);

	return MSG_STATUS_OK;
}

msg_status_e message_set_destination(message_t *const message,
				     const struct ether_addr *dst)
{
	memcpy(&message->dst, dst, sizeof(message->dst));
	return MSG_STATUS_OK;
}

msg_status_e message_serialize(const message_t *const message,
			       message_bytes_t *data)
{
#if defined(DEBUG)
	log_message(stderr, message, "SERIALIZING");
#endif /* defined(DEBUG) */

	memset(data, 0, sizeof(*data));

	/* DO NOT TRY TO ACCESS temp->src HERE, IT IS OUT OF BOUNDS! */
	data->data.magic = htonl(message->magic);
	memcpy(&data->data.dst, &message->dst, sizeof(data->data.dst));
	data->data.pad1[0] = data->data.pad1[1] = 0;
	data->data.crc = htonl(message->crc);
	data->data.msg_len = htonl(message->msg_len);
	strncpy(data->data.msg, message->msg, message->msg_len * sizeof(char));

	data->data_len = sizeof(data->data.magic) + sizeof(data->data.dst) +
			 sizeof(data->data.pad1) + sizeof(data->data.crc) +
			 sizeof(data->data.msg_len) +
			 (message->msg_len * sizeof(data->data.msg[0]));

#if defined(DEBUG)
	log_message_bytes(stderr, data, "SERIALIZED");
#endif /* defined(DEBUG) */

	return MSG_STATUS_OK;
}

msg_status_e message_deserialize(const message_bytes_t *const data,
				 message_t *const message)
{
	uint32_t msg_len = 0;
	uint32_t magic = 0;
	uint32_t crc = 0;

	memset(message, 0, sizeof(*message));

	magic = ntohl(data->data.magic);
	if (magic != MESSAGE_MAGIC)
		return MSG_STATUS_INVALID_MAGIC;

	crc = ntohl(data->data.crc);
	msg_len = ntohl(data->data.msg_len);

	if (strlen(data->data.msg) != msg_len)
		return MSG_STATUS_LEN_MISMATCH;

	if (msg_len >= MSG_MAX_LEN)
		return MSG_STATUS_TEXT_TOO_LONG;

	if (crc32(0, (const unsigned char *)data->data.msg, msg_len) != crc)
		return MSG_STATUS_CRC_MISMATCH;

	message->magic = magic;
	memcpy(&message->dst, &data->data.dst, sizeof(message->dst));
	memcpy(message->msg, data->data.msg, msg_len * sizeof(char));
	message->msg_len = msg_len;
	message->crc = crc;

	return MSG_STATUS_OK;
}

msg_status_e message_send(const char *const ifname, int sock,
			  const message_t *const message)
{
	struct sockaddr_ll dest_addr = { .sll_family = AF_PACKET };
	msg_status_e status = MSG_STATUS_INVALID;
	eth_header_t *eth_hdr = NULL;
	message_bytes_t *data = NULL;
	uint8_t *buffer = NULL;
	int if_index = -1;
	ssize_t ret = 0;

	if_index = get_if_index(sock, ifname);
	if (if_index < 0) {
		status = MSG_STATUS_INVALID_IF_INDEX;
		goto fail;
	}

	if (memcmp(&message->dst, &ETHER_INVALID_ADDR,
		   sizeof(ETHER_INVALID_ADDR)) == 0) {
		status = MSG_STATUS_INVALID_DEST;
		goto fail;
	}

	buffer = calloc(1, sizeof(eth_header_t) + sizeof(message_bytes_t));
	if (!buffer) {
		status = MSG_STATUS_MALLOC_FAIL;
		goto fail;
	}
	eth_hdr = (eth_header_t *)buffer;
	data = (message_bytes_t *)(buffer + sizeof(eth_header_t));

	status = message_serialize(message, data);
	if (status != MSG_STATUS_OK)
		goto fail;

#if defined(DEBUG)
	log_message_bytes(stderr, data, "SERIALIZED");
	log_message_bytes_hex(stderr, data, NULL);
#endif /* defined(DEBUG) */

	memcpy(&dest_addr.sll_addr, &message->dst, sizeof(message->dst));
	dest_addr.sll_halen = sizeof(message->dst);
	dest_addr.sll_ifindex = if_index;

#if defined(DEBUG)
	log_raw_buffer(stderr, buffer, sizeof(*eth_hdr) + data->data_len);
#endif /* defined(DEBUG) */

	eth_hdr->data_len = sizeof(*eth_hdr) + data->data_len;

	/*
	 * Minimum length of ethernet frame must be 64 bytes. Since message_bytes_t
	 * have lots of free space, we can simply clamp lower bound to 64 here
	 * if needed
	 */
	if (eth_hdr->data_len < 64)
		eth_hdr->data_len = 64;

	eth_hdr->proto = htons(MSG_PROTO_ID);
	memcpy(eth_hdr->dest, &data->data.dst, sizeof(eth_hdr->dest));
	if (get_if_mac(sock, ifname, (struct ether_addr *)&eth_hdr->source)) {
		status = MSG_STATUS_CANNOT_GET_IF_MAC;
		goto fail;
	}

	status = message_serialize(message, data);
	if (status != MSG_STATUS_OK)
		goto fail;

#if defined(DEBUG)
	log_raw_buffer(stderr, buffer, sizeof(*eth_hdr) + data->data_len);
#endif /* defined(DEBUG) */

	errno = 0;
	ret = sendto(sock, buffer, eth_hdr->data_len, 0,
		     (const struct sockaddr *)&dest_addr, sizeof(dest_addr));
	if (ret < 0) {
		status = errno_to_msg_status(errno);
		goto fail;
	}

	if (((size_t)ret) != eth_hdr->data_len) {
		status = MSG_STATUS_PARTIAL_DATA_SENT;
		goto fail;
	}

	status = MSG_STATUS_OK;

fail:
	FREE(buffer);
	return status;
}

msg_status_e message_recv(const char *const ifname, int sock,
			  message_t *const message)
{
	const size_t buffer_size =
		sizeof(eth_header_t) + sizeof(message_bytes_t);
	struct sockaddr_ll src_addr = { .sll_halen = 0 };
	socklen_t src_addr_len = sizeof(src_addr);
	msg_status_e status = MSG_STATUS_INVALID;
	const eth_header_t *eth_hdr = NULL;
	const message_bytes_t *data = NULL;
	uint8_t *buffer = NULL;
	ssize_t ret = 0;

	(void)ifname;

#if defined(DEBUG)
	fprintf(stderr, "try recvfrom\n");
#endif /* defined(DEBUG) */

	buffer = calloc(1, sizeof(*eth_hdr) + sizeof(*data));
	if (!buffer) {
		status = MSG_STATUS_MALLOC_FAIL;
		goto fail;
	}
	eth_hdr = (const eth_header_t *)buffer;
	data = (const message_bytes_t *)(buffer + sizeof(*eth_hdr));

	errno = 0;
	ret = recvfrom(sock, buffer, buffer_size, 0,
		       (struct sockaddr *)&src_addr, &src_addr_len);
	if (ret < 0) {
		status = errno_to_msg_status(errno);
		goto fail;
	}
	if (((size_t)ret) >= buffer_size) {
		status = MSG_STATUS_TEXT_TOO_LONG;
		goto fail;
	}

#if defined(DEBUG)
	fprintf(stderr, "recevied message\n");
#endif /* defined(DEBUG) */

	/* message_deserialize() handles crc32 and length validation */
	status = message_deserialize(data, message);
	if (status != MSG_STATUS_OK)
		return status;

	memcpy(&message->src, &src_addr.sll_addr, sizeof(message->src));

fail:
	FREE(buffer);
	return status;
}

msg_status_e message_write(int fd, const message_t *const message)
{
	(void)fd;
	(void)message;
	return MSG_STATUS_OK;
}

msg_status_e message_read(int fd, message_t *const message)
{
	(void)fd;
	(void)message;
	return MSG_STATUS_OK;
}

int message_print(FILE *stream, const message_t *const message)
{
	char src_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };
	char dst_mac[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };

	return fprintf(stream, "%s->%s: %s\n",
		       ether_ntoa_r(&message->src, src_mac),
		       ether_ntoa_r(&message->dst, dst_mac), message->msg);
}
