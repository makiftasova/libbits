/*
 * @file kvercode.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Feb 22, 2022
 * @brief Makefile for brainfuc interpreter
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2022, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define PRINT(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)

#define PRINT_ERROR(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)

#define PRINT_ERRNO(fmt, ...) \
	PRINT_ERROR(fmt ": %s", ##__VA_ARGS__, strerror(errno))

#define __fallthrough__ __attribute__((fallthrough))

#define KERNEL_VERSION(a, b, c) (((a) << 16) + ((b) << 8) + (c))

#define KERNEL_MAJOR(code) (((code)&0x00FF0000U) >> 16)
#define KERNEL_MINOR(code) (((code)&0x0000FF00U) >> 8)
#define KERNEL_PATCH(code) (((code)&0x000000FFU))

#define KERNEL_VERSION_FRMT "%lu.%lu.%lu"
#define KERNEL_VERSION_ARGS(code) \
	KERNEL_MAJOR(code), KERNEL_MINOR(code), KERNEL_PATCH(code)

void usage(const char *const exec)
{
	PRINT("%s - encode/decode Linux Kernel version codes", exec);
	PRINT("Options:");
	PRINT("\t-d:\tDecode given version code");
	PRINT("\t-e:\tConvert given major.minor.patch string to version code");
	PRINT("\t-h:\tPrint this help message");
	PRINT("Examples:");
	PRINT("\t%s -d 132638", exec);
	PRINT("\t%s -e 5.16.8", exec);
}

int main(int argc, char *argv[])
{
	const char *version_string = NULL;
	unsigned long version_code = 0;
	unsigned long major = 0;
	unsigned long minor = 0;
	unsigned long patch = 0;
	bool decode = false;
	bool encode = false;
	int opt = 0;

	while ((opt = getopt(argc, argv, "d:e:h")) != -1) {
		switch (opt) {
		case 'd':
			if (encode) {
				PRINT_ERROR("encode parameters already given!");
				return EXIT_FAILURE;
			}
			errno = 0;
			version_code = strtoul(optarg, NULL, 10);
			if (errno != 0) {
				PRINT_ERRNO("%s is not a valid version code",
					    optarg);
				return EXIT_FAILURE;
			}
			decode = true;
			break;
		case 'e':
			if (decode) {
				PRINT_ERROR("decode paramaters already given");
				return EXIT_FAILURE;
			}
			version_string = optarg;
			encode = true;
			break;
		default:
			PRINT_ERROR("unrecognized option %c", opt);
			__fallthrough__;
		case 'h':
			usage(argv[0]);
			break;
		}
	}

	if (!encode && !decode) {
		PRINT_ERROR("No mode selected");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if (decode) {
		PRINT(KERNEL_VERSION_FRMT,
		      KERNEL_VERSION_ARGS(version_code));
		return EXIT_SUCCESS;
	}

	if (sscanf(version_string, KERNEL_VERSION_FRMT, &major, &minor, &patch) != 3) {
		PRINT_ERRNO("Could not parse version string \"%s\"", version_string);
		return EXIT_FAILURE;
	}

	PRINT("%lu", KERNEL_VERSION(major, minor, patch));

	return EXIT_SUCCESS;
}
