/**
 * @file llist.h
 * @author Mehmet Akif TASOVA <makiftasova@gmail.com>
 * @date Dec 25, 2019
 * @brief Header file for an implementation of Single Linked List
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif TASOVA.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __LLIST_H__
#define __LLIST_H__

/**
 * @brief struct definiiton of a node of llist.
 */
struct ll_node {
	struct ll_node *next;
	size_t data_len;
	void *data;
};

/**
 * @brieg free gven struct ll_node object
 *
 * @param[in] node node to free
 */
void ll_node_free(struct ll_node *node);

/**
 * @brief check if given nodes are equal to each other.
 *
 * nodes are equal if any one of the following is true:
 * * given struct ll_node pointers are equal
 * * data held in nodes are equal when compared via memcmp.
 *
 * @param[in] n1 first node to compare
 * @param[in] n2 second node to compare
 *
 * @return if given nodes are equal, then a non-zero value will be returned.
 * If nodes are not equal, this function will return 0.
 */
int ll_node_equal(const struct ll_node *n1, const struct ll_node *n2);

/**
 * @brief creates a copy of given struct ll_node object.
 *
 * struct definition of given object orig and its underlying data will be
 * copied to new struct ll_node object.
 *
 * @param[in] orig original struct ll_node object to copy.
 *
 * @return on success, returns copy of given object orig. on error return NULL
 * and sets errno.
 */
struct ll_node *ll_node_copy(const struct ll_node *orig);

/**
 * @brief dump given message msg followed by given node data to given file
 * descriptor
 *
 * msg string will be followed by a semi-colon and a space which followed by
 * node data.
 * output format will be "$(msg): $(node->data)"
 *
 * If msg parameter is NULL, output will only contain node data.
 *
 * @param[in] n node to dump;
 * @param[in] msg message to precede node data in output. might be NULL.
 * @param[in] fd file descriptor to dump into.
 */
void ll_node_dump_msg(const struct ll_node *n, const char *msg, int fd);

/**
 * @brief dump given node to given file descriptor as an array of characters.
 *
 * @param[in] n node to dump;
 * @param[in] fd file descriptor to dump into.
 */
void ll_node_dump(const struct ll_node *n, int fd);

/**
 * @brief definition of llist type.
 */
struct llist {
	struct ll_node *head;
	size_t num_items;
};

/**
 * @brief definitions of enum values which could be returned by callback
 * function of llist_for_each
 */
typedef enum llist_iter_retval {
	LLIST_ITER_CONTINUE = 0,
	LLIST_ITER_BREAK,
	LLIST_ITER_DELETE,
} llist_iter_retval_t;

/**
 * @brief definition of callback function type for llist_for_each
 */
typedef llist_iter_retval_t (*llist_iter_cb_t)(struct ll_node *node);

/**
 * @brief create a new struct llist object with given initial list of nodes.
 *
 * @param[in] init initial list of struct ll_node items. might be NULL
 *
 * @return on success, a poitner to struct llist will be returned. on error
 * returns NULL and sets errno. to indicate error
 */
struct llist *llist_create(struct ll_node *init);

/**
 * @brief create a new empty struct llist object with given initial list of
 * nodes.
 *
 * @return on success, a poitner to struct llist will be returned. on error
 * returns NULL and sets errno. to indicate error.
 */
struct llist *llist_create_empty(void);

/**
 * @brief destroy given struct llist object.
 *
 * This function will free all nodes in given llist and then frees struct llist
 * objectitself.
 *
 * @param[in] llist struct llist object to destroy
 */
void llist_destroy(struct llist *llist);

/**
 * @brief append given node to end of given llist.
 *
 * @param[in] llist llist to add items.
 * @param[in] node node to append into given llist.
 *
 * @return. on success, function will return zero. on error function will return
 * a negative value and set errno.
 */
int llist_append(struct llist *llist, struct ll_node *node);

/**
 * @brief create a new node with given data and data_len then append the node
 * into end of given llist.
 *
 * @param[in] llist llist to add items.
 * @param[in] data pointer to data
 * @param[in] data_len length of given data, in number of bytes.
 *
 * @return. on success, function will return zero. on error function will return
 * a negative value and set errno.
 */
int llist_append_data(struct llist *llist, const void *data, size_t data_len);

/**
 * @brief create a new node with given string then append the node into end of
 * given llist.
 *
 * @param[in] llist llist to add items.
 * @param[in] str string to append into llist
 *
 * @return. on success, function will return zero. on error function will return
 * a negative value and set errno.
 */
int llist_append_str(struct llist *llist, const char *str);

/**
 * @brief delete first occurence of given node from given llist.
 *
 * @param[in] llist llist to delete from.
 * @param[in] entry entry to find and delete.
 *
 * @return if entry is NULL or llist is invalid or empty, return -1 and sets
 * errno. otherwice returns 0.
 */
int llist_delete(struct llist *llist, const struct ll_node *entry);

/**
 * @brief delete first occurence of given data with data_len from given llist.
 *
 * @param[in] llist llist to delete from.
 * @param[in] data pointer to data to delete
 * @param[in] data_len length of given data, in number of bytes.
 *
 * @return if entry is NULL or llist is invalid or empty, return -1 and sets
 * errno. otherwice returns 0.
 */
int llist_delete_data(struct llist *llist, void *data, size_t data_len);

/**
 * @brief delete first occurence of given string from given llist.
 *
 * @param[in] llist llist to delete from.
 * @param[in] str string to delete
 *
 * @return if entry is NULL or llist is invalid or empty, return -1 and sets
 * errno. otherwice returns 0.
 */
int llist_delete_str(struct llist *llist, char *data);

/**
 * @brief iterate over each node in given llist, and call given callback
 * function cb for each item.
 *
 * Items marked for deletion by callback function will be deleted just before
 * llist_for_each returns.
 *
 * @param[in] llist llist to iterate over.
 * @param[in] cb llist_iter_cb_t function to call for each item in llist.
 *
 * @return if no error occurs, this function will return 0. on error, function
 * will return -1 and set errno.
 */
int llist_for_each(struct llist *llist, llist_iter_cb_t cb);

#endif /* __LLIST_H__ */
