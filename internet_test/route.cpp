/**
 * @file route.cpp
 * @brief Source code file of a parser for /proc/net/route
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <exception>
#include <fstream>
#include <sstream>

#include "common.hpp"
#include "route.hpp"

static const std::string flag_to_name(const netutils::route::flags_t &flag)
{
	switch (flag) {
	case netutils::route::flags_t::UP:
		return "UP";
	case netutils::route::flags_t::GATEWAY:
		return "GATEWAY";
	case netutils::route::flags_t::HOST:
		return "HOST";
	case netutils::route::flags_t::REINSTATE:
		return "REINSTATE";
	case netutils::route::flags_t::DYNAMIC:
		return "DYNAMIC";
	case netutils::route::flags_t::MODIFIED:
		return "MODIFIED";
	case netutils::route::flags_t::MTU:
		return "MTU";
	case netutils::route::flags_t::WINDOW:
		return "WINDOW";
	case netutils::route::flags_t::IRTT:
		return "IRTT";
	case netutils::route::flags_t::REJECT:
		return "REJECT";
	case netutils::route::flags_t::STATIC:
		return "STATIC";
	case netutils::route::flags_t::XRESOLVE:
		return "XRESOLVE";
	case netutils::route::flags_t::NOFORWARD:
		return "NOFORWARD";
	case netutils::route::flags_t::THROW:
		return "THROW";
	case netutils::route::flags_t::NOPMTUDISC:
		return "NOPMTUDISC";
	default:
		return "";
	}
}

std::string netutils::route::parse_status_to_name(const parse_status_t &status)
{
	switch (status) {
	case OK:
		return "OK";
	case INVALID_DESTINATION:
		return "INVALID DESTINATION";
	case INVALID_GATEWAY:
		return "INVALID GATEWAY";
	case INVALID_NETMASK:
		return "INVALID NETMASK";
	default:
		return "UNKNWON ERROR";
	}
}

netutils::route::route(uint32_t metric)
	: _iface()
	, _destination()
	, _gateway()
	, _flags(flags::UNKNOWN)
	, _ref_count(0)
	, _use(0)
	, _metric(metric)
	, _netmask()
	, _mtu(0)
	, _window(0)
	, _irtt(0)
{
	/* left blank */
}

netutils::route::route(const route &r)
	: _iface(r._iface)
	, _destination(r._destination)
	, _gateway(r._gateway)
	, _flags(r._flags)
	, _ref_count(r._ref_count)
	, _use(r._use)
	, _metric(r._metric)
	, _netmask(r._netmask)
	, _mtu(r._mtu)
	, _window(r._window)
	, _irtt(r._irtt)
{
	/* left blank */
}

netutils::route::route(const std::string &s)
	: route()
{
	parse_status status;

	status = this->from_line(s);
	if (status != parse_status::OK)
		throw std::invalid_argument(
			"Could not parse route: " +
			netutils::route::parse_status_to_name(status));
}

netutils::route::parse_status
netutils::route::from_line(const std::string &line)
{
	std::istringstream iss(line);
	std::string token;

	/* read iface */
	iss >> this->_iface;

	/* read destination IP Address */
	iss >> token;
	if (this->_destination.from_string(token))
		return parse_status::INVALID_DESTINATION;

	/* read gateway IP Address */
	iss >> token;
	if (this->_gateway.from_string(token))
		return parse_status::INVALID_GATEWAY;

	/* read & parse flags */
	iss >> token;
	this->_flags = static_cast<flags_t>(std::stoul(token, nullptr, 16));

	/* parse ref count */
	iss >> token;
	this->_ref_count = std::stoul(token, nullptr, 10);

	/* parse use */
	iss >> token;
	this->_use = std::stoul(token, nullptr, 10);

	/* parse metric */
	iss >> token;
	this->_metric = std::stoul(token, nullptr, 10);

	/* parse netmask */
	iss >> token;
	if (this->_netmask.from_string(token))
		return parse_status::INVALID_NETMASK;

	/* parse MTU */
	iss >> token;
	this->_mtu = std::stoul(token, nullptr, 10);

	/* parse window */
	iss >> token;
	this->_window = std::stoul(token, nullptr, 10);

	/* parse IRTT */
	iss >> token;
	this->_irtt = std::stoul(token, nullptr, 10);

	return parse_status::OK;
}

std::string netutils::route::to_string(void) const
{
	std::stringstream ss;
	ss << "iface: " << this->_iface << " dest: " << this->_destination
	   << " gw: " << this->_gateway << " flags: " << this->flags_to_string()
	   << " netmask: " << this->_netmask << " metric: " << this->_metric;
	return ss.str();
}

#define ROUTE_HAS_FLAG(flag, stream)                             \
	do {                                                     \
		if (this->_flags & (flag)) {                     \
			(stream) << flag_to_name((flag)) << ","; \
		}                                                \
	} while (0)

std::string netutils::route::flags_to_string(void) const
{
	std::stringstream ss;
	std::string s;

	ROUTE_HAS_FLAG(flags_t::UP, ss);
	ROUTE_HAS_FLAG(flags_t::GATEWAY, ss);
	ROUTE_HAS_FLAG(flags_t::HOST, ss);
	ROUTE_HAS_FLAG(flags_t::REINSTATE, ss);
	ROUTE_HAS_FLAG(flags_t::DYNAMIC, ss);
	ROUTE_HAS_FLAG(flags_t::MODIFIED, ss);
	ROUTE_HAS_FLAG(flags_t::MTU, ss);
	ROUTE_HAS_FLAG(flags_t::WINDOW, ss);
	ROUTE_HAS_FLAG(flags_t::IRTT, ss);
	ROUTE_HAS_FLAG(flags_t::REJECT, ss);
	ROUTE_HAS_FLAG(flags_t::STATIC, ss);
	ROUTE_HAS_FLAG(flags_t::XRESOLVE, ss);
	ROUTE_HAS_FLAG(flags_t::NOFORWARD, ss);
	ROUTE_HAS_FLAG(flags_t::THROW, ss);
	ROUTE_HAS_FLAG(flags_t::NOPMTUDISC, ss);

	s = ss.str();
	if (!s.empty() && s.back() == ',')
		s.pop_back();

	return s;
}

const netutils::route &netutils::route::operator=(const netutils::route &r)
{
	if (this == &r)
		goto skip;

	_iface = r._iface;
	_destination = r._destination;
	_gateway = r._gateway;
	_flags = r._flags;
	_ref_count = r._ref_count;
	_use = r._use;
	_metric = r._metric;
	_netmask = r._netmask;
	_mtu = r._mtu;
	_window = r._window;
	_irtt = r._irtt;

skip:
	return *this;
}

bool netutils::route::operator==(const netutils::route &r) const
{
	if (this == &r)
		return true;

	return _iface == r._iface && _destination == r._destination &&
	       _gateway == r._gateway && _flags == r._flags &&
	       _ref_count == r._ref_count && _use == r._use &&
	       _metric == r._metric && _netmask == r._netmask &&
	       _mtu == r._mtu && _window == r._window && _irtt == r._irtt;
}

std::ostream &netutils::operator<<(std::ostream &os,
				   const netutils::route &route)
{
	os << route.to_string();
	return os;
}

const netutils::route netutils::routes::dummy_route;

netutils::routes::routes()
	: _route_list()
{
	if (this->update_routes())
		throw std::invalid_argument("Could not update routes");
}

bool netutils::routes::update_routes(void)
{
	std::ifstream ifs("/proc/net/route");
	std::string line;

	this->_route_list.clear();

	/* skip header line */
	std::getline(ifs, line);

	/* process rest of the file */
	while (std::getline(ifs, line))
		this->_route_list.push_back(route(line));

	ifs.close();

	this->_route_list.shrink_to_fit();

	return false;
}

const std::vector<netutils::route> &netutils::routes::get_routes(void) const
{
	return this->_route_list;
}

std::vector<netutils::route> netutils::routes::get_default_routes(void) const
{
	std::vector<netutils::route> rts;

	for (auto route : this->get_routes())
		if (route.is_default_route())
			rts.push_back(route);

	rts.shrink_to_fit();
	return rts;
}

/* preferred route is the default route with the smallest metric */
const netutils::route &netutils::routes::get_preferred_route(void) const
{
	const netutils::route *route = &netutils::routes::dummy_route;

	for (const netutils::route &r : this->get_routes()) {
		if (r.is_default_route() &&
		    r.get_metric() < route->get_metric())
			route = &r;
	}

	return *route;
}

std::string netutils::routes::to_string(void) const
{
	std::stringstream ss;
	for (auto route : this->_route_list)
		ss << route << std::endl;
	return ss.str();
}

std::ostream &netutils::operator<<(std::ostream &os,
				   const netutils::routes &routes)
{
	os << routes.to_string();
	return os;
}
