/*
 * @file audio_keys.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date May 12, 2023
 * @brief audio_keys is a simple keyboard event listener for audi control keys.
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2023, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <err.h>
#include <fcntl.h>
#include <fts.h>
#include <poll.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <linux/input.h>

#define ARRAY_LEN(arr) (sizeof((arr)) / sizeof((arr)[0]))

#define WPCTL_MUTE "wpctl set-mute '@DEFAULT_SINK@' toggle"
#define WPCTL_VOL_DOWN "wpctl set-volume '@DEFAULT_SINK@' 5%-"
#define WPCTL_VOL_UP "wpctl set-volume '@DEFAULT_SINK@' 5%+"

typedef enum {
	KEYCODE_MUTE = 113,
	KEYCODE_VOL_DOWN = 114,
	KEYCODE_VOL_UP = 115,
} keycode_t;

size_t open_files(char *dir, int *fd_list, const size_t fd_list_capacity)
{
	int fts_options = FTS_COMFOLLOW | FTS_LOGICAL | FTS_NOCHDIR;
	FTS *handle = NULL;
	char *const dirs[2] = { dir, NULL };
	FTSENT *file = NULL;
	size_t i = 0;

	memset(fd_list, 0, fd_list_capacity * sizeof(*fd_list));

	handle = fts_open(dirs, fts_options, NULL);
	if (!handle)
		err(EXIT_FAILURE, "Cannot open directory %s", dir);

	file = fts_children(handle, 0);
	if (file == NULL)
		err(EXIT_FAILURE, "No files to process");

	while ((file = fts_read(handle)) != NULL) {
		switch (file->fts_info) {
		case FTS_DEFAULT:
			if (strstr(file->fts_path, "input/event") == NULL)
				break;
			if (i >= fd_list_capacity)
				err(EXIT_FAILURE, "fd_list too short");

			fd_list[i] = open(file->fts_path, O_RDONLY);
			if (fd_list[i] < 0)
				err(EXIT_FAILURE, "Cannot open %s",
				    file->fts_path);

			++i;
			break;
		default:
			break;
		}
	}

	fts_close(handle);

	printf("num_files: %lu\n", i);

	return i;
}

int close_files(int *fd_list, const size_t fd_list_capacity)
{
	for (size_t i = 0; i < fd_list_capacity; ++i) {
		close(fd_list[i]);
		fd_list[i] = -1;
	}
	return 0;
}

nfds_t fds_to_pollfds(int *fds, const size_t num_fds, struct pollfd *pollfds,
		      nfds_t num_pollfds)
{
	if (num_pollfds < num_fds)
		err(EXIT_FAILURE, "Not enough pollfds");

	for (nfds_t i = 0; i < num_fds; ++i) {
		pollfds[i].fd = fds[i];
		pollfds[i].events = POLLIN;
	}

	return num_fds;
}

int main(int argc, char *argv[])
{
	struct pollfd pollfds[128] = { 0 };
	char *devfs_input = "/dev/input";
	struct input_event event;
	nfds_t num_pollfds = 0;
	ssize_t event_len = 0;
	int fds[128] = { -1 };
	size_t num_fds = 0;
	int poll_ret = 0;

	(void)argc;
	(void)argv;

	num_fds = open_files(devfs_input, fds, ARRAY_LEN(fds));
	if (num_fds == 0)
		err(EXIT_FAILURE, "Cannot open files");

	num_pollfds = fds_to_pollfds(fds, num_fds, pollfds, ARRAY_LEN(pollfds));
	if (num_pollfds == 0)
		err(EXIT_FAILURE, "No fd to poll");

	/* wait until next event */
	while ((poll_ret = poll(pollfds, num_pollfds, -1)) > -1) {
		if (poll_ret == 0) /* nothing to read */
			continue;

		for (nfds_t i = 0; i < num_pollfds; ++i) {
			/* this fd has nothing */
			if (!(pollfds[i].revents & POLLIN))
				continue;

			event_len = read(pollfds[i].fd, &event, sizeof(event));
			if (event_len == -1) {
				if (errno == EINTR)
					continue;
				err(EXIT_FAILURE,
				    "Unexpected return value from read");
			}
			if (event_len != sizeof(event)) {
				errno = EIO;
				err(EXIT_FAILURE,
				    "Corrupted event data. expected %lu found %ld",
				    sizeof(event), event_len);
			}

			switch (event.code) {
			case KEYCODE_MUTE: /* mute/unmute */
				system(WPCTL_MUTE);
				break;
			case KEYCODE_VOL_DOWN: /* volume down */
				system(WPCTL_VOL_DOWN);
				break;
			case KEYCODE_VOL_UP: /* volume up */
				system(WPCTL_VOL_UP);
				break;
			default:
				/* ignore all the other keycodes */
				break;
			}
		}
	}

	close_files(fds, num_fds);

	return EXIT_SUCCESS;
}
