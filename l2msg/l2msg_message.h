/*
 * @file l2msg_message.h
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date August 05, 2024
 * @brief Message packer/unpacker for l2msg
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2024, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(__L2MSG_MESSAGE_H__)
#define __L2MSG_MESSAGE_H__ 1

#include <stdint.h>
#include <stdio.h>

#include <net/ethernet.h>

/** Local Experimental EtherType 2 as defined in IEEE Std 802 */
#define MSG_PROTO_ID 0x88B6

typedef enum : unsigned {
	MSG_STATUS_OK = 0,
	MSG_STATUS_INVALID,
	MSG_STATUS_INVALID_MAGIC,
	MSG_STATUS_TEXT_TOO_LONG,
	MSG_STATUS_LEN_MISMATCH,
	MSG_STATUS_CRC_MISMATCH,
	MSG_STATUS_INVALID_DEST,
	MSG_STATUS_SEND_FAILED,
	MSG_STATUS_RECV_FAILED,
	MSG_STATUS_WRITE_FAILED,
	MSG_STATUS_READ_FAILED,
	MSG_STATUS_AFNOTSUPP,
	MSG_STATUS_FD_NOT_VALID,
	MSG_STATUS_INTERRUPTED,
	MSG_STATUS_OTHEREND_CLOSED,
	MSG_STATUS_WOULDBLOCK,
	MSG_STATUS_PARTIAL_DATA_SENT,
	MSG_STATUS_INVALID_IF_INDEX,
	MSG_STATUS_CANNOT_GET_IF_MAC,
	MSG_STATUS_MALLOC_FAIL,
	MSG_STATUS_MAX,
} msg_status_e;

const char *msg_status_to_string(const msg_status_e status);

#define MSG_MAX_LEN 1023U

#define MESSAGE_MAGIC 4042322160U

typedef struct {
	uint32_t magic; /* magic value, always should be MESSAGE_MAGIC */
	struct ether_addr dst; /* destination mac address */
	uint8_t pad1[2]; /* 2 bytes of padding fpr ether_addr */
	uint32_t crc; /* crc-32 checksum */
	uint32_t msg_len; /* length of message text */
	char msg[MSG_MAX_LEN + 1]; /* message text */
	struct ether_addr src; /* source mac address */
	uint8_t pad2[2]; /* 2 bytes of padding for ether_addr */
} message_t;

#define MSG_MAX_BYTES \
	(sizeof(message_t) - sizeof(struct ether_addr) - (2 * sizeof(uint8_t)))

typedef struct {
	size_t data_len;
	union {
		uint8_t bytes[MSG_MAX_BYTES];
		struct {
			uint32_t magic;
			struct ether_addr dst;
			uint8_t pad1[2];
			uint32_t crc;
			uint32_t msg_len;
			char msg[MSG_MAX_LEN + 1];
		};
	} data;
} message_bytes_t;

extern const struct ether_addr ETHER_BCAST_ADDR; /* Broadcast mac address */
extern const struct ether_addr ETHER_INVALID_ADDR; /* zero mac address */

msg_status_e message_from_text(message_t *const message, const char *text);

msg_status_e message_set_destination(message_t *const message,
				     const struct ether_addr *dest);

msg_status_e message_serialize(const message_t *const message,
			       message_bytes_t *data);

msg_status_e message_deserialize(const message_bytes_t *const data,
				 message_t *const message);

msg_status_e message_send(const char *const ifname, int sock,
			  const message_t *const message);

msg_status_e message_recv(const char *const ifname, int sock,
			  message_t *const message);

msg_status_e message_write(int fd, const message_t *const message);

msg_status_e message_read(int fd, message_t *const message);

int message_print(FILE *stream, const message_t *const message);

#endif /* defined(__L2MSG_MESSAGE_H__) */
