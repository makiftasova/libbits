/**
 * @file mbf.c
 * @brief Yet Another brainfuck impelementataion.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Apr 28, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (C) 2021, Mehmet Akif Tasova
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if defined(DEBUG)
#include <sys/mman.h>
#endif /* DEBUG */

#define PRINT_ERROR(fmt, ...) \
	fprintf(stderr, "%s: " fmt "\n", argv[0], ##__VA_ARGS__)

#if defined(DEBUG)
#define BUF_MMAP_PROT (PROT_READ | PROT_WRITE)
#define BUF_MMAP_MAP (MAP_PRIVATE | MAP_ANONYMOUS)
#define PRINT_LOG(fmt, ...) \
	fprintf(stdout, "%s: " fmt "\n", argv[0], ##__VA_ARGS__)
#endif /* DEBUG */

int main(int argc, char **argv)
{
	const char *path = NULL;
#if defined(DEBUG)
	size_t buf_len = 8192 * sizeof(char);
	char *buf = NULL;
#endif /* DEBUG */

	switch (argc) {
	case 1:
		path = getenv("HOME");
		break;
	case 2:
		path = (!strcmp("-", argv[1])) ? getenv("OLDPWD") : argv[1];
		break;
	default:
		PRINT_ERROR("too many arguments");
		return EXIT_FAILURE;
	}

#if defined(DEBUG)
	buf = mmap(NULL, buf_len, BUF_MMAP_PROT, BUF_MMAP_MAP, -1, 0);
	if (buf == MAP_FAILED) {
		PRINT_ERROR("failed to allocate buffer for debug output: %s",
			    strerror(errno));
		buf = NULL;
	}

	PRINT_LOG("trying to cd into \"%s\"", path);
	if (buf) {
		if (buf != getcwd(buf, buf_len)) {
			PRINT_ERROR("getcwd failed: %s", strerror(errno));
		} else {
			PRINT_LOG("previous cwd: %s", buf);
		}
	}
#endif /* DEBUG */

	if (chdir(path)) {
		PRINT_ERROR("%s: %s", strerror(errno), path);
		return EXIT_FAILURE;
	}

#if defined(DEBUG)
	if (buf) {
		if (buf != getcwd(buf, buf_len)) {
			PRINT_ERROR("getcwd failed: %s", strerror(errno));
		} else {
			PRINT_LOG("new cwd: %s", buf);
		}
		munmap(buf, buf_len);
	}
#endif /* DEBUG */

	return EXIT_SUCCESS;
}
