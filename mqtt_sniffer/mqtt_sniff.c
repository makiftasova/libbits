/**
 * @file mqtt_sniffer.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @brief MQTT message record and replay utility.
 *
 * Copyright 2019 Mehmet Akif Tasova
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <endian.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <mosquitto.h>

#define FILE_MODE S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH /* rw-r--r-- */

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 1883
#define DEFAULT_TIMEOUT 60

#define UNUSED(var) ((void)(var))

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#define SCLOSE(fd)                   \
	do {                         \
		if ((fd) > -1) {     \
			close((fd)); \
			(fd) = -1;   \
		}                    \
	} while (0)

typedef unsigned char byte_t;

static volatile int __run_state = 0;
static int __fd = -1;

void sigaction_sigterm(int sig, siginfo_t *info, void *ucontext)
{
	UNUSED(sig);
	UNUSED(info);
	UNUSED(ucontext);
	puts("SIGINT received");
	__run_state = 0;
}

int set_signal_handlers(void)
{
	int ret = 0;
	static struct sigaction _sa_term;

	memset(&_sa_term, 0, sizeof(_sa_term));

	_sa_term.sa_sigaction = sigaction_sigterm;
	_sa_term.sa_flags = SA_SIGINFO;

	ret = sigaction(SIGTERM, &_sa_term, NULL);
	ret |= sigaction(SIGINT, &_sa_term, NULL);

	return ret;
}

static void print_bytes(const byte_t *bytes, const size_t nbytes)
{
#define NCHARS 20
	size_t i = 0;
	char line_buffer[((5 * NCHARS) + 1) * sizeof(char)] = { 0 };
	char *line_ptr = line_buffer;
	size_t buff_len = sizeof(line_buffer);
	int ret = 0;

	for (i = 0; i < nbytes; ++i) {
		ret = snprintf(line_ptr, buff_len, "0x%02X ", bytes[i]);
		line_ptr += ret;
		buff_len -= ret;
		if (((i + 1) % NCHARS) == 0) {
			puts(line_buffer);
			memset(line_buffer, 0, sizeof(line_buffer));
			line_ptr = line_buffer;
			buff_len = sizeof(line_buffer);
		}
	}
	if (line_buffer[0] != '\0') {
		puts(line_buffer);
	}
#undef NCHARS
}

struct mqtt_event {
	uint64_t topic_len;
	uint64_t payload_len;
	char *topic;
	void *payload;
};

void mqtt_event_print(const struct mqtt_event *e, FILE *stream)
{
	(void)fprintf(stream, "TOPIC \"%s\"' PAYLOAD:\n", e->topic);
	print_bytes(e->payload, e->payload_len);
}

void mqtt_event_deinit(struct mqtt_event *e)
{
	if (e) {
		SFREE(e->topic);
		SFREE(e->payload);
		e->topic_len = 0;
		e->payload_len = 0;
	}
}

int mqtt_event_init(struct mqtt_event *e, const char *topic, void *payload,
		    uint64_t payload_len)
{
	if (!e || !topic || !payload || !payload_len) {
		errno = EINVAL;
		return -1;
	}

	if (e->topic || e->payload) {
		errno = EPERM;
		return -1;
	}

	e->topic_len = strlen(topic);
	e->topic = strdup(topic);
	if (!e->topic) {
		goto fail;
	}

	e->payload_len = payload_len;
	e->payload = calloc(payload_len, sizeof(byte_t));
	if (!e->payload) {
		goto fail;
	}

	memcpy(e->payload, payload, payload_len);
	return 0;

fail:
	mqtt_event_deinit(e);
	return -1;
}

int mqtt_event_from_mosquitto_message(struct mqtt_event *e,
				      const struct mosquitto_message *msg)
{
	return mqtt_event_init(e, msg->topic, msg->payload, msg->payloadlen);
}

ssize_t mqtt_event_write(const struct mqtt_event *e, int fd)
{
	uint64_t lengths[2] = { 0 };
	uint64_t data_size = 0;
	uint64_t data_off = 0;
	uint8_t *data = NULL;
	ssize_t nbytes = 0;

	if (!e || fd < 0) {
		errno = EINVAL;
		return -1;
	}

	data_size = e->topic_len + 1 + e->payload_len + (2 * sizeof(uint64_t));

	data = calloc(data_size, sizeof(byte_t));
	if (!data) {
		return -1;
	}

	lengths[0] = htobe64(e->topic_len);
	lengths[1] = htobe64(e->payload_len);

	memcpy(data, lengths, sizeof(lengths));
	data_off += sizeof(lengths);

	memcpy(data + data_off, e->topic, e->topic_len * sizeof(char));
	data_off += (e->topic_len + 1) * sizeof(char);

	memcpy(data + data_off, e->payload, e->payload_len * sizeof(byte_t));

	nbytes = write(fd, data, data_size);
	if (nbytes < 0) {
		goto fail;
	}

	SFREE(data);
	return nbytes;
fail:
	SFREE(data);
	return -1;
}

ssize_t mqtt_event_read(struct mqtt_event *e, int fd)
{
	uint64_t lengths[2] = { 0 };
	ssize_t nbytes = 0;
	char *topic = NULL;
	void *payload = NULL;
	ssize_t rb = 0; /* read bytes */

	memset(e, 0, sizeof(*e));

	rb = read(fd, lengths, sizeof(lengths));
	if (rb != sizeof(lengths)) {
		return -1;
	}
	nbytes += rb;

	lengths[0] = be64toh(lengths[0]);
	lengths[1] = be64toh(lengths[1]);

	topic = calloc(lengths[0] + 1, sizeof(char));
	if (!topic) {
		goto fail;
	}

	payload = calloc(lengths[1], sizeof(byte_t));
	if (!payload) {
		goto fail;
	}

	rb = read(fd, topic, (lengths[0] + 1) * sizeof(char));
	if ((uint64_t)rb != ((lengths[0] + 1) * sizeof(char))) {
		goto fail;
	}
	nbytes += rb;

	rb = read(fd, payload, lengths[1] * sizeof(byte_t));
	if ((uint64_t)rb != (lengths[1] * sizeof(byte_t))) {
		goto fail;
	}
	nbytes += rb;

	e->topic_len = lengths[0];
	e->topic = topic;
	e->payload_len = lengths[1];
	e->payload = payload;

	return nbytes;
fail:
	SFREE(topic);
	SFREE(payload);
	return -1;
}

int mqtt_event_send(const struct mqtt_event *e, struct mosquitto *mosq)
{
	return mosquitto_publish(mosq, NULL, e->topic, e->payload_len,
				 e->payload, 0, 0);
}

void mqtt_log_callback(struct mosquitto *mosq, void *userdata, int level,
		       const char *str)
{
	UNUSED(mosq);
	UNUSED(userdata);
	printf("<%d> %s\n", level, str);
}

void mqtt_recv_callback(struct mosquitto *mosq, void *obj,
			const struct mosquitto_message *msg)
{
	struct mqtt_event e;

	UNUSED(mosq);
	UNUSED(obj);

	memset(&e, 0, sizeof(e));

	if (mqtt_event_from_mosquitto_message(&e, msg)) {
		perror("mqtt_event_from_mosquitto_message failed");
		goto fail;
	}

	if (mqtt_event_write(&e, __fd)) {
		perror("mqtt_event_write failed");
		goto fail;
	}

fail:
	mqtt_event_deinit(&e);
}

int mqtt_send_all(struct mosquitto *mosq, int fd)
{
	struct mqtt_event e;
	uint64_t count = 0;
	int ret = 0;

	memset(&e, 0, sizeof(e));

	ret = mosquitto_loop_start(mosq);
	if (ret) {
		printf("mosquitto_loop_start failed with code %d\n", ret);
		ret = -1;
		goto fail;
	}

	puts("mosq loop started");

	do {
		puts("reading event");

		errno = 0;
		ret = mqtt_event_read(&e, fd);
		if (ret < 0) {
			if (!errno) {
				puts("all events are read.");
				__run_state = 0;
				break;
			}
			perror("mqtt_event_read failed");
			ret = -1;
			goto fail;
		}

		printf("sending event: ");
		mqtt_event_print(&e, stdout);

		if (mqtt_event_send(&e, mosq)) {
			perror("mqtt_event_send failed");
			ret = -1;
			goto fail;
		}
		++count;

		puts("event sent");

		mqtt_event_deinit(&e);

		puts("event cleanup");
	} while (__run_state);

	printf("Published %lu events.\n", count);

	ret = mosquitto_loop_stop(mosq, true);
	if (ret) {
		printf("mosquitto_loop_stop failed with code %d\n", ret);
		ret = -1;
		goto fail;
	}
	ret = 0;

fail:
	mqtt_event_deinit(&e);
	return ret;
}

void usage(const char *exec)
{
	printf("%s - sniff/replay all mqtt messages from broker.\n", exec);
	puts("usage:");
	printf("\t%s [-i IP] [-p PORT] [-r] FILE\n", exec);
	puts("\t sniff/replay mqtt events to/from FILE");
	puts("options:");
	puts("\t-i [IP]\t: IP address of broker");
	puts("\t-p [PORT]\t: Port of broker");
	puts("\t-r \t: Replay mqtt events instead of sniffing them.");
}

int main(int argc, char *argv[])
{
	struct mosquitto *mosq = NULL;
	const char *filepath = NULL;
	const char *ip = DEFAULT_IP;
	int port = DEFAULT_PORT;
	int replay_mode = 0;
	int file_flags = 0;
	char opt = '\0';
	int ret = 0;

	while ((opt = getopt(argc, argv, "hi:p:r")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
		case 'i':
			ip = optarg;
			break;
		case 'p':
			errno = 0;
			port = strtol(optarg, NULL, 10);
			if (errno != 0) {
				printf("invalid port %s\n", optarg);
				usage(argv[0]);
				return EXIT_FAILURE;
			}
			errno = 0;
			break;
		case 'r':
			replay_mode = 1;
			break;
		default:
			printf("invalid parameter %c\n", opt);
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (optind >= argc) {
		printf("file path is missing\n");
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	filepath = argv[optind];

	file_flags = O_WRONLY | O_CREAT | O_EXCL;
	if (replay_mode) {
		file_flags = O_RDONLY;
	}

	__fd = open(filepath, file_flags, FILE_MODE);
	if (__fd < 0) {
		perror("cannot open file");
		return EXIT_FAILURE;
	}

	set_signal_handlers();

	mosquitto_lib_init();

	mosq = mosquitto_new(NULL, true, NULL);
	if (!mosq) {
		printf("mosquitto_new failed");
		goto out;
	}

	mosquitto_log_callback_set(mosq, mqtt_log_callback);

	if (!replay_mode) {
		mosquitto_message_callback_set(mosq, mqtt_recv_callback);
	}

	if (mosquitto_connect(mosq, ip, port, DEFAULT_TIMEOUT)) {
		perror("mosquitto_connect failed");
		ret = EXIT_FAILURE;
		goto out;
	}

	if (!replay_mode) {
		mosquitto_subscribe(mosq, NULL, "#", 0);
	}

	__run_state = 1;

	if (replay_mode) {
		ret = mqtt_send_all(mosq, __fd);
		if (ret < 0) {
			perror("mqtt_send_all failed");
		}
		goto out;
	}

	do {
		ret = mosquitto_loop(mosq, DEFAULT_TIMEOUT, 1);
		if (__run_state && ret) {
			printf("mosquitto connection error. code: %d\n", ret);
			sleep(1);
			mosquitto_reconnect(mosq);
		}
	} while (__run_state);

out:
	SCLOSE(__fd);
	if (mosq) {
		mosquitto_disconnect(mosq);
		mosquitto_destroy(mosq);
	}
	mosquitto_lib_cleanup();
	return ret;
}
