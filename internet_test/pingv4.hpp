/**
 * @file pingv4.hpp
 * @brief Header file of ICMP echo maker, for IPv4
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __PINGV4_HPP__
#define __PINGV4_HPP__

#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <string>
#include <thread>
#include <vector>

#include <netinet/in.h>

#include "ipv4.hpp"
#include "ping.hpp"

namespace netutils
{
class pingv4 {
    public:
	static const std::string DEFAULT_INTERFACE;
	static const uint16_t DEFAULT_MAX_TRIES; /* UINT16_MAX */
	static const std::chrono::milliseconds DEFAULT_TIMEOUT; /* SIZE_MAX */

	pingv4(const std::string &ifname = DEFAULT_INTERFACE,
	       const std::string &uri = "example.com",
	       const uint16_t max_tries = DEFAULT_MAX_TRIES,
	       const std::chrono::milliseconds &timeout = DEFAULT_TIMEOUT,
	       const bool print_responses = false);

	/* reset ping statistics. allows to re-try ping with same settings */
	void reset(void);

	bool set_ifname(const std::string &ifname);
	bool set_uri(const std::string &uri);
	bool set_max_tries(const uint16_t &max_tries);
	bool set_print_responses(const bool enable);

	bool set_timeout(const std::chrono::milliseconds &msecs);
	const std::chrono::milliseconds& get_timeout(void) const;

	const std::string &get_host_ip(void) const;

	const std::vector<netutils::ping_result> &get_results(void) const;

	void run(void);

	netutils::ping_result get_last_result(void) const;

    private:
	std::string _ifname;
	std::string _uri;
	uint16_t _max_tries;
	std::chrono::milliseconds _timeout;
	std::string _host_ip;
	bool _print_resps;
	std::vector<netutils::ping_result> _results;

	int _bind_to_interface(int sock) const;

	int _set_icmp_filter(int sock) const;

	void _get_host_ip(sockaddr_in *addr) const;

	size_t _calculate_latency(
		const std::chrono::steady_clock::time_point &begin,
		const std::chrono::steady_clock::time_point &end) const;

	uint16_t _get_echo_id(void);

	enum class sock_timeout_type {
		TIMEOUT_RECV = 0,
		TIMEOUT_SEND,
	};
	/* set socket timeout in milliseconds */
	int _set_socket_timeout(int sock, enum sock_timeout_type type) const;

	uint32_t _icmp_checksum(const uint16_t *buf, uint32_t len);
};

} /* namespace netutils */

#endif /* __PINGV4_HPP__ */
