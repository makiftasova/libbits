# A Collection of little libraries

A collection of various libraries and/or programs which written in free times
for practices.

# Contents:
* audio_keys: A small helper to allow using volume control keys without X.org or Wayland
* autofree: A set of macros utilizing gcc's "cleanup" attribute.
* cd: stand alone version of POSIX cd (it have very niche uses).
* bitmap: yet another library for processing bitmaps in C.
* bitmap-view: a simple bitmap image file parser/viewer using SDL2.
* brainfuck: yet another brainfuck interpreter.
* brainfuck-v2: another version of brainfuck which uses POSIX APIs for file
operations.
* diceroll: a dice roller, accepts XdY+M format
* elf_parser: a utility for reading ELF file header.
* internet_test: a utility to check if internet connection is UP.
* kvercode: a simple Linux kernel version decoder/encoder.
* l2msg: An ASCII text message broadcaster and receiver for OSI layer 2
* llist: an implementation of single linked list.
* mcscalc: A simple calculator for 802.11be MCS rates (except index 14 and 15)
* mkdir: partial reimplementation of mkdir command in UNIX.
* mmmalloc: yet another simple malloc implementation backed by mmap
* mqtt_bitmap: Read bitmap from given JSON file andpublish it to given MQTT
topic.
* mqtt_sniffer: records all messages for all topics sent to broker into a file.
also it can replay recorded messages in same order.
* nproc: show bnumber of available CPUs on system.
* pyspeedserver: A simple python code to use as upload/download speed test server node.
* sha256sum-nacl: partial reimplementation of sha256sum with using libsodium.
* sha256sum-ssl: partial reimplementation of sha256sum with using OpenSSL.
* sieve_of_eratosthenes: calculate prime number using Sieve of Eratosthenes
* waifu2x-video-upscale: warrper script to use waifu2x for video upscaling.
