/**
 * @file sha256sum.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Nov 11, 2019
 * @brief Yet another sha256sum calculator
 *
 * This implementation depends on OpenSSL.
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#include <openssl/sha.h>
#include <openssl/evp.h>

#define BUF_SIZ 1024

#define FILEPATH_STDIN "-"

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#define SCLOSE(fd)                   \
	do {                         \
		if ((fd) > -1) {     \
			close((fd)); \
			(fd) = -1;   \
		}                    \
	} while (0)

const char *basename(const char *path)
{
	char *outp = strrchr(path, '/');
	if (!outp) {
		return path;
	}
	return outp;
}

char *map_file(const char *const filepath, size_t *file_size)
{
	int fd = -1;
	struct stat statbuf;
	char *addr = NULL;

	fd = open(filepath, O_RDONLY);
	if (fd < 0) {
		perror("open failed");
		goto fail;
	}

	if (fstat(fd, &statbuf)) {
		perror("fstat failed");
		goto fail;
	}

	if (statbuf.st_size == 0) {
		errno = EIO;
		perror("file is empty or virtual file");
		goto fail;
	}

	addr = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (addr == MAP_FAILED) {
		perror("mmap failed");
		goto fail;
	}

	if (file_size) {
		*file_size = statbuf.st_size;
	}

	SCLOSE(fd);
	return addr;

fail:
	SCLOSE(fd);
	return NULL;
}

char *read_stdin(size_t *len)
{
	int ret = 0;
	char *data = NULL;
	char *new_data = NULL;
	char buf[BUF_SIZ + 1] = { '\0' };

	data = malloc(1 * sizeof(char));
	if (!data) {
		perror("malloc failed");
		goto fail;
	}
	*data = '\0';
	*len = 0;

	while ((ret = read(STDIN_FILENO, buf, BUF_SIZ)) != 0) {
		if (ret < 0) {
			perror("read failed");
			goto fail;
		}
		*len += strlen(buf);
		new_data = realloc(data, *len);
		if (!new_data) {
			perror("realloc failed");
			goto fail;
		}

		data = new_data;
		strcat(data, buf);
		memset(buf, '\0', sizeof(buf));
	}

	return data;

fail:
	SFREE(data);
	*len = 0;
	return NULL;
}

int sha256sum(const char *buf, size_t buflen, char *sum, size_t sumlen)
{
	uint8_t hash[EVP_MAX_MD_SIZE];
	unsigned int hash_len = 0;
	unsigned int i = 0;
	EVP_MD_CTX *evpctx = NULL;

	if (!buf || !sum) {
		errno = EINVAL;
		return -1;
	}

	evpctx = EVP_MD_CTX_new();
	if (evpctx == NULL) {
		perror("EVP_MD_CTX_new() failed");
		return -1;
	}

	if (sumlen < (SHA256_DIGEST_LENGTH * 2) + 1) {
		errno = ENOBUFS;
		goto fail;
	}

	if (EVP_DigestInit_ex(evpctx, EVP_sha256(), NULL) == 0) {
		perror("EVP_DigestInit_ex failed");
		goto fail;
	}

	if (EVP_DigestUpdate(evpctx, buf, buflen) == 0) {
		perror("EVP_Digestupdate failed");
		goto fail;
	}

	if (EVP_DigestFinal_ex(evpctx, hash, &hash_len) == 0) {
		perror("EVP_DigestFinal_ex failed");
		goto fail;
	}

	for (i = 0; i < hash_len; ++i) {
		snprintf(sum + (i * 2), 3, "%02x", hash[i]);
	}

	EVP_MD_CTX_free(evpctx);
	return 0;

fail:
	EVP_MD_CTX_free(evpctx);
	*sum = '\0';
	return -1;
}

void usage(const char *const exec)
{
	printf("%s - calculate sha256 hash of input\n", exec);
	printf("usage:\n\t%s [FILE]\n", exec);
	printf("\tif no file is given, reads stdin\n");
}

int main(int argc, char *argv[])
{
	bool mmaped = false;
	size_t input_size = 0;
	char *data = NULL;
	char *path = NULL;
	const char *file = NULL;
	char sum[(SHA256_DIGEST_LENGTH * 2) + 1] = { '\0' };

	if (argc > 2) {
		printf("invalid arguments");
		usage(argv[0]);
		return -1;
	}

	if (argc == 2) {
		path = strdup(argv[1]);
		if (!path) {
			perror("strdup failed");
			return -1;
		}
		mmaped = true;
		file = basename(path);
		data = map_file(argv[1], &input_size);
	} else {
		file = FILEPATH_STDIN;
		data = read_stdin(&input_size);
	}

	if (!data) {
		mmaped = false;
		goto cleanup;
	}

	if (sha256sum(data, input_size, sum, sizeof(sum))) {
		perror("sha256sum failed");
		goto cleanup;
	}

	printf("%s  %s\n", sum, file);

cleanup:
	if (mmaped) {
		munmap(data, input_size);
	} else {
		SFREE(data);
	}
	SFREE(path);

	return 0;
}
