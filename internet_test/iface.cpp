/**
 * @file iface.cpp
 * @brief A class to check a network interface's status
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>
#include <cstring>
#include <sstream>

#include <arpa/inet.h>
#include <linux/sockios.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include "iface.hpp"

const std::chrono::milliseconds
	netutils::iface::DEFAULT_VALIDITY_PERIOD(5000); /* 5 seconds */

static const std::string flag_to_name(const netutils::iface::flag flag)
{
	switch (flag) {
	case netutils::iface::flag::up:
		return "UP";
	case netutils::iface::flag::broadcast:
		return "BROADCAST";
	case netutils::iface::flag::debug:
		return "DEBUG";
	case netutils::iface::flag::loopback:
		return "LOOPBACK";
	case netutils::iface::flag::point_to_point:
		return "POINTTOPOINT";
	case netutils::iface::flag::no_trailers:
		return "NOTRAILERS";
	case netutils::iface::flag::running:
		return "RUNNING";
	case netutils::iface::flag::no_arp:
		return "NOARP";
	case netutils::iface::flag::promiscus:
		return "PROMISC";
	case netutils::iface::flag::all_multicast:
		return "ALLMULTI";
	case netutils::iface::flag::master:
		return "MASTER";
	case netutils::iface::flag::slave:
		return "SLAVE";
	case netutils::iface::flag::multicast:
		return "MULTICAST";
	case netutils::iface::flag::portsel:
		return "PORTSEL";
	case netutils::iface::flag::auto_media:
		return "AUTOMEDIA";
	case netutils::iface::flag::dynamic:
		return "DYNAMIC";
	default:
		break;
	}
	return "EMPTY";
}

netutils::iface::iface(const std::string &ifname,
		       const std::chrono::milliseconds &validity_period)
	: _ifname(ifname)
	, _validity_period(validity_period)
	, _flags()
{
	this->_update_flags();
}

void netutils::iface::update_flags(void)
{
	this->_update_flags();
}

const std::vector<netutils::iface::flag> &netutils::iface::get_flags(void)
{
	if (this->_flags_needs_update())
		this->update_flags();
	return this->_flags;
}

bool netutils::iface::is_flag_set(const netutils::iface::flag to_check) const
{
	if (this->_flags_needs_update())
		this->_update_flags();

	if (this->_flags.empty())
		return false;

	if (std::find(this->_flags.begin(), this->_flags.end(), to_check) ==
	    this->_flags.end())
		return false;

	return true;
}

std::string netutils::iface::to_string(void) const
{
	std::stringstream ss;
	std::string s;

	if (this->_flags_needs_update())
		this->_update_flags();

	ss << this->_ifname << " Flags: ";

	for (netutils::iface::flag f : this->_flags) {
		ss << flag_to_name(f) << ",";
	}

	s = ss.str();
	if (!s.empty() && s.back() == ',')
		s.pop_back();

	return s;
}

bool netutils::iface::_flags_needs_update(void) const
{
	std::chrono::milliseconds time_diff =
		std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now() - this->_last_update);

	return this->_flags.empty() || time_diff >= this->_validity_period;
}

void netutils::iface::_update_flags(void) const
{
	int sock = -1;
	ifreq ifr;

	std::memset(&ifr, 0, sizeof(ifr));

	/* clear current state */
	this->_flags.clear();
	this->_flags.shrink_to_fit();

	memccpy(ifr.ifr_name, this->_ifname.c_str(), '\0',
		sizeof(ifr.ifr_name));

	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		return;

	if (ioctl(sock, SIOCGIFFLAGS, &ifr) < 0) {
		close(sock);
		return;
	}
	close(sock);

	/* update this->_flags with ioctl results */
	if (ifr.ifr_flags & netutils::iface::flag::up)
		this->_flags.push_back(netutils::iface::flag::up);
	if (ifr.ifr_flags & netutils::iface::flag::broadcast)
		this->_flags.push_back(netutils::iface::flag::broadcast);
	if (ifr.ifr_flags & netutils::iface::flag::debug)
		this->_flags.push_back(netutils::iface::flag::debug);
	if (ifr.ifr_flags & netutils::iface::flag::loopback)
		this->_flags.push_back(netutils::iface::flag::loopback);
	if (ifr.ifr_flags & netutils::iface::flag::point_to_point)
		this->_flags.push_back(netutils::iface::flag::point_to_point);
	if (ifr.ifr_flags & netutils::iface::flag::no_trailers)
		this->_flags.push_back(netutils::iface::flag::no_trailers);
	if (ifr.ifr_flags & netutils::iface::flag::running)
		this->_flags.push_back(netutils::iface::flag::running);
	if (ifr.ifr_flags & netutils::iface::flag::no_arp)
		this->_flags.push_back(netutils::iface::flag::no_arp);
	if (ifr.ifr_flags & netutils::iface::flag::promiscus)
		this->_flags.push_back(netutils::iface::flag::promiscus);
	if (ifr.ifr_flags & netutils::iface::flag::all_multicast)
		this->_flags.push_back(netutils::iface::flag::all_multicast);
	if (ifr.ifr_flags & netutils::iface::flag::master)
		this->_flags.push_back(netutils::iface::flag::master);
	if (ifr.ifr_flags & netutils::iface::flag::slave)
		this->_flags.push_back(netutils::iface::flag::slave);
	if (ifr.ifr_flags & netutils::iface::flag::multicast)
		this->_flags.push_back(netutils::iface::flag::multicast);
	if (ifr.ifr_flags & netutils::iface::flag::portsel)
		this->_flags.push_back(netutils::iface::flag::portsel);
	if (ifr.ifr_flags & netutils::iface::flag::auto_media)
		this->_flags.push_back(netutils::iface::flag::auto_media);
	if (ifr.ifr_flags & netutils::iface::flag::dynamic)
		this->_flags.push_back(netutils::iface::flag::dynamic);

	this->_last_update = std::chrono::system_clock::now();
}

std::ostream &netutils::operator<<(std::ostream &os,
				   const netutils::iface &iface)
{
	os << iface.to_string();
	return os;
}
