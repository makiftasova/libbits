/**
 * @file bitmap.h
 * @brief Header file for a simple bitmap library written in C89
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Nov 21, 2019
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BITMAP_H__
#define __BITMAP_H__ 1

/**
 * @brief Opaque data type definition for bitmap
 */
typedef struct bitmap *bitmap_t;

/**
 * @brief Value returned by bitmap_create and bitmap_duplicate when
 * bitmap_create fails.
 */
#define BITMAP_FAILED ((struct bitmap *)NULL)

/**
 * @brief Creates a bitmap object with given data.
 *
 * A copy of given data will be created for internal manipulation.
 *
 * Possible errors:
 * 	* EINVAL: either data is NULL or nbytes is 0.
 * 	* ENOMEM: out of memory.
 *
 * @param[in] data pointer to data. will be treated as byte array.
 * @param[in] nbytes size of given data, in bytes.
 *
 * @return returns valid bitmap_t object on success. on error returns
 * BITMAP_FAILED abd sets errno.
 */
bitmap_t bitmap_create(const void *data, const size_t nbytes);

/**
 * @brief Duplicated given bitmap.
 *
 * allocates a new bitmap_t object and copies bitmap from given bitmap_t object
 * to new bitmap_t object.
 *
 * Possible errors:
 * 	* ENOMEM: out of memory.
 *
 * @param[in] bitmap bitmap_t object to duplicate
 *
 * @return returns valid bitmap_t object on success. on error returns
 * BITMAP_FAILED abd sets errno.
 */
bitmap_t bitmap_duplicate(const bitmap_t bitmap);

/**
 * @brief Destroys given bitmap which created by bitmap_create.
 *
 * Possible erros:
 * 	* No error.
 *
 * @param[in] bitmap bitmap_t object to destroy.
 *
 * @return does not return any value.
 */
void bitmap_destroy(bitmap_t bitmap);

/**
 * @brief Get bit at given index.
 *
 * Data will be treated as an endless array of bits.
 *
 * Possible errors:
 * 	* ERANGE: given bit index is out of range.
 *
 * @param[in] bitmap bitmap_t object.
 * @param[in] bit index of bit.
 *
 * @return on success returns either 1 or 0 depending on value of requested bit.
 * On error return -1 and sets errno.
 */
int bitmap_get_bit(const bitmap_t bitmap, const size_t bit);

/**
 * @brief Get bit at given index as an unsigned char (a.k.a. uint8_t).
 *
 * Data will be treated as an endless array of bits.
 *
 * This function does not indicate error via its return value, insttead it only
 * utilizes errno on errors. Set errno to 0 before calling this function if you
 * need to track erros or simply use bitmap_get_bit if you need check erros.
 *
 * Possible errors:
 * 	* ERANGE: given bit index is out of range.
 *
 * @param[in] bitmap bitmap_t object.
 * @param[in] bit index of bit.
 *
 * @return on success returns either 1 or 0 depending on value of requested bit.
 * On error returns 0 and sets errno.
 */
unsigned char bitmap_get_bit_uc(const bitmap_t bitmap, const size_t bit);

/**
 * @brief Returns number of bytes in underlying bitmap.
 *
 * Possible errors:
 * 	* EINVAL: given bitmap_t object is invalid.
 *
 * @param[in] bitmap bitmap_t object.
 *
 * @return on success, returns size of bitmap in bytes. on error return 0 and
 * sets errno.
 */
size_t bitmap_get_num_bytes(const bitmap_t bitmap);

/**
 * @brief duplicates bytes in bitmap_t object and returns pointer to newly
 * created byte array.
 *
 * it is safe to pass NULL as nbyes paramter. If nbytes == NULL, then function
 * will not return any info about number of bytes in bitmap and caller must use
 * bitmap_get_num_bytes function in order to learn number of bytes in bytes
 * array.
 *
 * If function succeeds, caller must free the returned bytes array by using
 * free() function from stdlib.h
 *
 * Possible errors:
 * 	* EINVAL: bitmap_t object is invalid.
 * 	* ENOMEM: failed to allocate buffer when duplicatiing bytes.
 *
 * @param[in] bitmap bitmap_t object.
 * @param[out] nbytes number bytes in duplicated bytes array. it is safe to pass
 * NULL into this parameter.
 *
 * @return on success, returns a valid pointer to array of bytes which must be
 * freed by caller. on error returns NULL and sets errno.
 */
unsigned char *bitmap_get_bytes(const bitmap_t bitmap, size_t *nbytes);

/**
 * @brief Set bit at given index.
 *
 * Data will be treated as an endless array of bits.
 * Bit at given bit index will be set to 1.
 *
 * Possible errors:
 * 	* ERANGE: given bit index is out of range.
 *
 * @param[in] bitmap bitmap_t object.
 * @param[in] bit index of bit.
 *
 * @return on success returns 0. On error return -1 and sets errno.
 */
int bitmap_set_bit(bitmap_t bitmap, const size_t bit);

/**
 * @brief Set bit at given index.
 *
 * Data will be treated as an endless array of bits.
 * Bit at given bit index will be set to 0.
 *
 * Possible errors:
 * 	* ERANGE: given bit index is out of range.
 *
 * @param[in] bitmap bitmap_t object.
 * @param[in] bit index of bit.
 *
 * @return on success returns 0. On error return -1 and sets errno.
 */
int bitmap_clear_bit(bitmap_t bitmap, const size_t bit);

/**
 * @brief Dump bits in bitmap into given string buffer.
 *
 * This function does not make any internal memory allocation for string buffer.
 * Caller must provide string buffer with sufficent size.
 *
 * Following conditions must be met in order to work:
 * 	* sizeof(buf) <= bufsize
 * 	* bufsize >= num_bits + (num_bits / bbl) + (num_bits / bpl) + 1
 *
 * Possible errors:
 * 	* EINVAL: bitmap or buf is invalid.
 * 	* ENOBUFS: bufsize is too small.
 *
 * @param[in] bitmap bitmap_t object to dump.
 * @param[in] bbl bit block lenngth. length of bits in each block. valid values
 * are between 1 and 8 (inclusive).
 * @param[in] bpl bits per line. number of bits per line. valid values are
 * between 1 and 32 (inclusive).
 * @param[in,out] buf pointer to string buffer.
 * @param[in] bufsize length of string buffer in bytes, including NULL
 * terminator.
 *
 * @return returns number of bytes printed into buffer on success. on error
 * returns -1 and sets errno.
 */
int bitmap_dump_to_string(const bitmap_t bitmap, unsigned char bbl,
			  unsigned char bpl, char *buf, const size_t bufsize);

/**
 * @brief Dumps bits in given bitmap to stdout.
 *
 * Uses bitmap_dump_to_string internally.
 * Allocates string buffer of enough size before calling bitmap_dump_to_string
 * then prints string generated by bitmap_dump_to_string to stdout then frees
 * the buffer.
 *
 * Possible erros:
 * 	* EINVAL: bitmap or buf is invalid.
 * 	* ENOBUFS: bufsize is too small.
 * 	* ENOMEM: failed to allocate buffer to string form of bitmap.
 *
 * @param[in] bitmap bitmap_t object to dump.
 * @param[in] bbl bit block lenngth. length of bits in each block. valid values
 * are between 1 and 8 (inclusive).
 * @param[in] bpl bits per line. number of bits per line. valid values are
 * between 1 and 32 (inclusive).
 *
 * @return returns number of bytes printed into stdout on success. on error
 * returns -1 and sets errno.
 */
int bitmap_dump_bits_format(const bitmap_t bitmap, unsigned char bbl,
			    unsigned char bpl);

/**
 * @brief Dumps bits in given bitmap to stdout.
 *
 * Calls bitmap_dump_bits_format with bbl = 8 and bpl = 32
 *
 * Possible erros:
 * 	* EINVAL: bitmap or buf is invalid.
 * 	* ENOBUFS: bufsize is too small.
 * 	* ENOMEM: failed to allocate buffer to string form of bitmap.
 *
 * @param[in] bitmap bitmap_t object to dump.
 *
 * @return returns number of bytes printed into stdout on success. on error
 * returns -1 and sets errno.
 */
int bitmap_dump_bits(const bitmap_t bitmap);

#endif /* __BITMAP_H__ */
