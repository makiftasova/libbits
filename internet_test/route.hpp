/**
 * @file route.hpp
 * @brief Header file of a parser for /proc/net/route
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ROUTE_HPP__
#define __ROUTE_HPP__

#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#include <net/route.h>

#include "ipv4.hpp"

namespace netutils
{
class route {
    public:
	// clang-format off
	/* from net/route.h */
	enum flags {
		UNKNOWN		= 0x0000,		/* No flags set */
		UP		= RTF_UP,		/* Route usable.  */
		GATEWAY		= RTF_GATEWAY,		/* Destination is a gateway.  */
		HOST		= RTF_HOST,		/* Host entry (net otherwise).  */
		REINSTATE	= RTF_REINSTATE,	/* Reinstate route after timeout.  */
		DYNAMIC		= RTF_DYNAMIC,		/* Created dyn. (by redirect).  */
		MODIFIED	= RTF_MODIFIED,		/* Modified dyn. (by redirect).  */
		MTU		= RTF_MTU,		/* Specific MTU for this route.  */
		WINDOW		= RTF_WINDOW,		/* Per route window clamping.  */
		IRTT		= RTF_IRTT,		/* Initial round trip time.  */
		REJECT		= RTF_REJECT,		/* Reject route.  */
		STATIC		= RTF_STATIC,		/* Manually injected route.  */
		XRESOLVE	= RTF_XRESOLVE,		/* External resolver.  */
		NOFORWARD	= RTF_NOFORWARD,	/* Forwarding inhibited.  */
		THROW		= RTF_THROW,		/* Go to next class.  */
		NOPMTUDISC	= RTF_NOPMTUDISC,	/* Do not send packets with DF.  */
	};
	typedef enum flags flags_t;
	// clang-format on

	enum parse_status {
		OK = 0,
		UNKNOWN_STATUS,
		INVALID_DESTINATION, /* destination IP is invalid */
		INVALID_GATEWAY, /* gateway IP is invalid */
		INVALID_NETMASK, /* netmask is invalid */
	};
	typedef enum parse_status parse_status_t;

	route(uint32_t metric = UINT32_MAX);
	route(const route &r);
	route(const std::string &s);

	parse_status_t from_line(const std::string &line);

	inline bool is_flag_set(const flags_t &flag) const
	{
		return (this->_flags & flag) != 0;
	}

	inline bool is_up(void) const
	{
		return this->is_flag_set(flags_t::UP);
	}

	inline bool is_gw(void) const
	{
		return this->is_flag_set(flags_t::GATEWAY);
	}

	bool is_default_route(void) const
	{
		return this->is_up() && this->is_gw();
	}

	inline const std::string &get_iface(void) const
	{
		return this->_iface;
	}

	inline const ipv4 &get_destination(void) const
	{
		return this->_destination;
	}

	inline const ipv4 &get_gateway(void) const
	{
		return this->_gateway;
	}

	inline const ipv4 &get_netmask(void) const
	{
		return this->_netmask;
	}

	inline uint32_t get_metric(void) const
	{
		return this->_metric;
	}

	std::string to_string(void) const;

	const route &operator=(const route &r);

	bool operator==(const route &r) const;

    private:
	std::string _iface;
	ipv4 _destination;
	ipv4 _gateway;
	flags_t _flags;
	uint32_t _ref_count;
	uint32_t _use;
	uint32_t _metric;
	ipv4 _netmask;
	uint32_t _mtu;
	uint32_t _window;
	uint32_t _irtt;

	std::string flags_to_string(void) const;
	static std::string parse_status_to_name(const parse_status_t &status);
};

std::ostream &operator<<(std::ostream &os, const route &route);

class routes {
    public:
	static const route dummy_route;

	routes();

	bool update_routes(void);

	const std::vector<route>& get_routes(void) const;

	std::vector<route> get_default_routes(void) const;

	/**
	 * @brief preferred route is the default route with the smallest metric
	 *
	 * @returns returns #dummy_route if no preferred gateway is found.
	 */
	const route &get_preferred_route(void) const;

	std::string to_string(void) const;

    private:
	std::vector<route> _route_list;
};

std::ostream &operator<<(std::ostream &os, const routes &routes);

} /* namespace netutils */

#endif /* __ROUTE_HPP__ */
