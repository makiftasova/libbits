/**
 * @file main.c
 * @brief test code for ELF file format parser.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#undef _GNU_SOURCE

#include "elf.h"

enum codes {
	EXIT_OK = EXIT_SUCCESS,
	EXIT_FAIL,
	EXIT_MISSING_PARAM,
	EXIT_READ_ERR,
	EXIT_LAST,
};

static inline void usage(const char *name)
{
	printf("usage:\n\t%s <file>\n", name);
}

int main(int argc, char *argv[])
{
	struct elf_file *file_hdr;

	if (argc != 2) {
		puts("missing argument\n");
		usage(argv[0]);
		return EXIT_MISSING_PARAM;
	}

	file_hdr = elf_read_file(argv[1]);
	if (file_hdr == NULL) {
		char *buf = NULL;
		if (errno == EINVAL) {
			asprintf(&buf, "file %s is not an ELF file", argv[1]);
		} else {
			asprintf(&buf, "failed to read file %s", argv[1]);
		}
		perror(buf);
		free(buf);
		return EXIT_READ_ERR;
	}

	elf_file_dump(file_hdr, NULL);

	if (elf_release_file(file_hdr)) {
		char *buf = NULL;
		asprintf(&buf, "error when closing elf file %s", argv[1]);
		perror(buf);
		free(buf);
	}

	return EXIT_OK;
}
