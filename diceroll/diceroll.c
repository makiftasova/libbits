/**
 * @file diceroll.c
 * @brief Yet another dice roller which understands XdY+M syntax
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Apr 14, 2022
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2022, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define __fallthrough__ __attribute__((fallthrough))

#define XSTR(x) #x
#define STR(x) XSTR(x)

#define RANDOM_PATH "/dev/random"

#define SCANF_PATTERN_MAX 32
#define USER_STRING_LEN 255
#define USER_STRING_MAX (USER_STRING_LEN + 1)

#define USER_STRING_FMT ("%" STR(USER_STRING_LEN) "s")

#define DICE_MIN 1 /* min number on a given die */

#define DICE_MOD_NONE '\0'
#define DICE_MOD_MINUS '-'
#define DICE_MOD_PLUS '+'

#define CLOSE(fd)                    \
	do {                         \
		if ((fd) > -1) {     \
			close((fd)); \
			(fd) = -1;   \
		}                    \
	} while (0)

#define STRTOUL(value, string, endptr, base)                         \
	do {                                                         \
		errno = 0;                                           \
		(value) = strtoul((string), &(endptr), (base));      \
		if ((value) == ULONG_MAX && errno != 0) {            \
			PRINT_ERROR("Could not parse string \"%s\"", \
				    (string));                       \
			goto out;                                    \
		}                                                    \
		errno = 0;                                           \
	} while (0)

#define PRINT(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)

#define PRINT_ERROR(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)

#define PRINT_ERRNO(fmt, ...) \
	PRINT_ERROR(fmt ": %s", ##__VA_ARGS__, strerror(errno))

#if defined(DEBUG)
#define DEBUG_PRINT(fmt, ...) PRINT(fmt, ##__VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#endif

static unsigned long get_random(int fd, const unsigned long min,
				const unsigned long max)
{
	unsigned long data = 0;

	if (read(fd, &data, sizeof(data)) != sizeof(data)) {
		PRINT_ERRNO("Could not read random data");
		data = 0;
	}

	return (data % (max - min + 1)) + min;
}

void help(const char *const exec)
{
	PRINT("%s - roll a dice", exec);
	PRINT("%s [-h] [-d DICE] [DICE]", exec);
	PRINT("%s -d 3d8+5", exec);
	PRINT("%s 3d8+5", exec);
	PRINT("Run without any parameter for interactive mode");
	PRINT("Options:");
	PRINT("\th\t: Show this help text");
	PRINT("\td DICE\t: pass dice string in XdY+M format");
}

unsigned long roll_dice(const char *dice_string)
{
	unsigned long dice_side = 0; /* number of dice sides */
	unsigned long dice_num = 0; /* number of dice to roll */
	unsigned long roll = 0;
	unsigned long mod = 0;
	char mod_sign = DICE_MOD_NONE;
	unsigned result = 0;
	char *endptr = NULL;
	int random_fd = -1;

	STRTOUL(dice_num, dice_string, endptr, 10);

	if (*endptr != 'd' && *endptr != 'D') {
		PRINT_ERROR("Invalid dice string \"%s\"", dice_string);
		goto out;
	}
	++endptr;

	STRTOUL(dice_side, endptr, endptr, 10);

	if (*endptr == '\0') { /* dice string has no modifier part */
		mod_sign = DICE_MOD_NONE;
		goto calculate_result;
	}

	if (*endptr != DICE_MOD_MINUS && *endptr != DICE_MOD_PLUS) {
		PRINT_ERROR("Invalid dice string \"%s\"", dice_string);
		goto out;
	}
	mod_sign = *endptr;
	++endptr;

	STRTOUL(mod, endptr, endptr, 10);

calculate_result:

	DEBUG_PRINT("num: %lu\ndice: %lu\nmod: %c%lu", dice_num, dice_side,
		    mod_sign, mod);

	/* why would you want to roll(!) a 1 sided die? */
	if (dice_side == 1) {
		PRINT_ERROR("I also like to live dangerously.");
	}

	random_fd = open(RANDOM_PATH, O_RDONLY);
	if (random_fd < 0) {
		PRINT_ERRNO("Could not open " RANDOM_PATH);
		goto out;
	}

	for (unsigned long i = 0; i < dice_num; ++i) {
		roll = get_random(random_fd, DICE_MIN, dice_side);
		DEBUG_PRINT("roll #%lu: %lu", i + 1, roll);
		result += roll;
	}

	switch (mod_sign) {
	case DICE_MOD_NONE:
		break;
	case DICE_MOD_MINUS:
		result -= mod;
		break;
	case DICE_MOD_PLUS:
		result += mod;
		break;
	default:
		PRINT_ERROR("Invalid dice string \"%s\"", dice_string);
		result = 0;
		goto out;
		break;
	}

out:
	CLOSE(random_fd);
	return result;
}

int main(int argc, char *argv[])
{
	char user_string[USER_STRING_MAX] = { 0 };
	const char *dice_string = NULL;
	unsigned long result = 0;
	int ret = EXIT_FAILURE;
	int opt = 0;

	while ((opt = getopt(argc, argv, "hd:")) != -1) {
		switch (opt) {
		case 'd':
			dice_string = optarg;
			goto skip_input;
			break;
		case 'h':
			help(argv[0]);
			ret = EXIT_SUCCESS;
			goto out;
			break;
		default:
			PRINT("Unknown option: %c", opt);
			help(argv[0]);
			goto out;
			break;
		}
	}

	if (optind >= argc) { /* use interactive mode to take dice string */
		printf(">>> ");
		if (scanf(USER_STRING_FMT, user_string) == EOF) {
			PRINT_ERRNO("Could not read user input");
			goto out;
		}
		dice_string = user_string;
	} else { /* last argument is dice string */
		dice_string = argv[optind];
	}

skip_input:
	result = roll_dice(dice_string);
	if (!result) {
		goto out;
	}

	PRINT("roll for %s: %lu", dice_string, result);

	ret = EXIT_SUCCESS;
out:
	return ret;
}
