/**
 * @file elf.h
 * @brief Header file for ELF file format parser.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ELF_H__
#define __ELF_H__

#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include <assert.h>

#if CHAR_BIT != 8
#error type char must be 8 bits.
#endif

#ifndef static_assert
#error static_assert not supported. Use a compiler with C11 support
#endif

static_assert(sizeof(char) == sizeof(uint8_t),
	      "sizeof(char) != sizeof(uint8_t)");

/* ELF IDENT indices */
enum EI_INDEX {
	EI_MAG0 = 0,
	EI_MAG1,
	EI_MAG2,
	EI_MAG3,
	EI_CLASS,
	EI_DATA,
	EI_VERSION,
	EI_OSABI,
	EI_ABIVERSION,
	EI_PAD0,
	EI_PAD1,
	EI_PAD2,
	EI_PAD3,
	EI_PAD4,
	EI_PAD5,
	EI_PAD6,
	EI_NIDENT = 16,
	EI_LAST = 16,
};

#define ELF_CLASS_INVAL 0 /* Invalid ELF class type */
#define ELF_CLASS_32_BIT 1
#define ELF_CLASS_64_BIT 2

#define ELF_DATA_ENDIAN_INV 0 /* Invalid ELF data type */
#define ELF_DATA_ENDIAN_LIT 1
#define ELF_DATA_ENDIAN_BIG 2

enum ELF_BITS {
	ELF_BITS_INVAL = 0,
	ELF_BITS_32,
	ELF_BITS_64,
};

/* Type definition and valid values for OS ABI */
typedef uint8_t ELF_OSABI;
#define EOA_SYSV 0x00
#define EOA_HP_UX 0x01
#define EOA_NETBSD 0x02
#define EOA_LINUX 0x03
#define EOA_GNUHURD 0x04
#define EOA_SOLARIS 0x06
#define EOA_AIX 0x07
#define EOA_IRIX 0x08
#define EOA_FREEBSD 0x09
#define EOA_TRU64 0x0A
#define EOA_MODESTO 0x0B
#define EOA_OPENBSD 0x0C
#define EOA_OPENVMS 0x0D
#define EOA_NONSTOP 0x0E
#define EOA_AROS 0x0F
#define EOA_FENIX_OS 0x10
#define EOA_CLOUDABI 0x11
#define EOA_LAST 0xFF /* reference purposes only, not a valid value */

const char *elf_osabi_to_str(ELF_OSABI osabi);

/* Type definition and valid values for file types */
typedef uint16_t ELF_TYPE;
#define ET_NONE 0x0000
#define ET_REL 0x0001
#define ET_EXEC 0x0002
#define ET_DYN 0x0003
#define ET_CORE 0x0004
#define ET_LOOS 0xFE00
#define ET_HIOS 0xFEFF
#define ET_LOPROC 0xFF00
#define ET_HIPROC 0xFFFF

const char *elf_type_to_str(ELF_TYPE type);

/* Type definition and valid values for machine types */
typedef uint16_t ELF_MACHINE;
#define EMAC_GENERIC 0x00
#define EMAC_SPARC 0x02
#define EMAC_X86 0x03
#define EMAC_MIPS 0x08
#define EMAC_POWERPC 0x14
#define EMAC_S390 0x16
#define EMAC_ARM 0x28
#define EMAC_SUPERH 0x2A
#define EMAC_IA_64 0x32
#define EMAC_X86_64 0x3E
#define EMAC_AARCH64 0xB7
#define EMAC_RISC_V 0xF3

const char *elf_machine_to_str(ELF_MACHINE machine);

struct elf_ident {
	uint8_t magic[4];
	uint8_t class; /* 1 is 32-bits, 2 is 63-bits*/
	uint8_t data; /* 1 is little endian aand 2 is big endian */
	uint8_t version; /* set to 1 for the original version of ELF */
	ELF_OSABI osabi;
	uint8_t abi_version;
	uint8_t pad[7]; /* currently not used */
} __attribute__((packed));

#define ELF_IDENT_SIZE (sizeof(struct elf_ident))

static_assert(ELF_IDENT_SIZE == EI_NIDENT,
	      "sizeof(struct elf_ident) !=	EI_NIDENT");

#define ELF_IDENT_CLASS_INVAL(_ident_) ((ELF_CLASS_INVAL) == ((_ident_)->class))
#define ELF_IDENT_CLASS_32_BIT(_ident_) \
	((ELF_CLASS_32_BIT) == ((_ident_)->class))
#define ELF_IDENT_CLASS_64_BIT(_ident_) \
	((ELF_CLASS_64_BIT) == ((_ident_)->class))

#define ELF_IDENT_ENDIAN_INV(_ident_) \
	((ELF_DATA_ENDIAN_INV) == ((_ident_)->data))
#define ELF_IDENT_ENDIAN_LIT(_ident_) \
	((ELF_DATA_ENDIAN_LIT) == ((_ident_)->data))
#define ELF_IDENT_ENDIAN_BIG(_ident_) \
	((ELF_DATA_ENDIAN_BIG) == ((_ident_)->data))

void elf_ident_dump(const struct elf_ident *ident, FILE *stream)
	__attribute__((nonnull));

struct elf32_header {
	union {
		struct elf_ident ident;
		uint8_t e_ident[ELF_IDENT_SIZE];
	};
	ELF_TYPE type;
	ELF_MACHINE machine;
	uint32_t version; /* set to 1 for original version of ELF */
	uint32_t entry; /* memory address of entry point */
	uint32_t phoff; /* start of the program header table */
	uint32_t shoff; /* start of the section header table */
	uint32_t flags;
	uint16_t ehsize; /* size of elf header, should be 52 bytes for 32 bit */
	uint16_t phentsize; /* size of program header table */
	uint16_t phnum; /* contains the number of entries in program header table */
	uint16_t shentsize; /* size of section header table */
	uint16_t shnum; /* contains the number of entries in section header table */
	uint16_t shstrndx; /* contains index of the section header table entry that contains
				   the section names.*/
};

#define ELF32_HEADER_SIZE (sizeof(struct elf32_header))

static_assert(ELF32_HEADER_SIZE == 52, "ELF32 header is not 53 bytes");

void elf32_header_dump(const struct elf32_header *header, FILE *stream)
	__attribute__((nonnull));

struct elf64_header {
	union {
		struct elf_ident ident;
		uint8_t e_ident[ELF_IDENT_SIZE];
	};
	ELF_TYPE type;
	ELF_MACHINE machine;
	uint32_t version; /* set to 1 for original version of ELF */
	uint64_t entry; /* memory address of entry point */
	uint64_t phoff; /* start of the program header table */
	uint64_t shoff; /* start of the section header table */
	uint32_t flags;
	uint16_t ehsize; /* size of elf header, should be 64 bytes for 64-bit */
	uint16_t phentsize; /* size of program header table */
	uint16_t phnum; /* contains the number of entries in program header table */
	uint16_t shentsize; /* size of section header table */
	uint16_t shnum; /* contains the number of entries in section header table */
	uint16_t shstrndx; /* contains index of the section header table entry that contains
				   the ection names.*/
};

#define ELF64_HEADER_SIZE (sizeof(struct elf64_header))

static_assert(ELF64_HEADER_SIZE == 64, "ELF64 header is not 64 bytes");

void elf64_header_dump(const struct elf64_header *header, FILE *stream)
	__attribute__((nonnull));

typedef uint32_t ELF_PH_TYPE; /* ELF Program header type */
#define EPT_NULL 0x00000000
#define EPT_LOAD 0x00000001
#define EPT_DYNAMIC 0x00000002
#define EPT_INERTP 0x00000003
#define EPT_NOTE 0x00000004
#define EPT_SHLIB 0x00000005
#define EPT_PHDR 0x00000006
#define EPT_LOOS 0x60000000
#define GNU_EH_FRAME 0x6474E550
#define GNU_STACK 0x6474E551
#define GNU_RELRO 0x6474E552
#define EPT_HIOS 0x6FFFFFFF
#define EPT_LOPROC 0x70000000
#define EPT_HIPROC 0x7FFFFFFF

const char *elf_prog_type_to_str(ELF_PH_TYPE type);

struct elf32_prog_header {
	ELF_PH_TYPE type; /* type of segment */
	uint32_t offset;
	uint32_t vaddr;
	uint32_t paddr;
	uint32_t filesz;
	uint32_t memsz;
	uint32_t flags;
	uint32_t align; /* indicates no alignment if 0 or 1 */
};

struct elf64_prog_header {
	ELF_PH_TYPE type; /* type of segment */
	uint32_t flags;
	uint64_t offset;
	uint64_t vaddr;
	uint64_t paddr;
	uint64_t filesz;
	uint64_t memsz;
	uint64_t align; /* indicates no alignment if 0 or 1 */
};

void elf64_prog_header_dump(const struct elf64_prog_header *phdr, FILE *stream)
	__attribute__((nonnull));

struct elf_prog_header_list {
	enum ELF_BITS bits;
	uint16_t count;
	union {
		struct elf32_prog_header *elf32_prog_headers;
		struct elf64_prog_header *elf64_prog_headers;
		void *elf_prog_headers;
	};
};

void elf_prog_header_list_dump(const struct elf_prog_header_list *hdrs,
			       FILE *stream);

struct elf_file {
	int fd;
	struct elf_ident *elf_ident; /* points to elf_header.ident */
	union {
		struct elf32_header
			*elf32_header; /* points to 32 bit elf header struct */
		struct elf64_header
			*elf64_header; /* points to 64 bit elf header struct */
		void *elf_header; /* used for freeing elf_header */
	};
	struct elf_prog_header_list elf_prog_headers;
};

struct elf_file *elf_read_file(const char *const file_path);
int elf_release_file(struct elf_file *file);

/**
 * dumps elf file metadata to given FILE* stream
 *
 * @param[in] elf_file pointer to struct elf_file
 * @param[in] stream FILE stream pointer, if NULL, defaults to stdout.
 *
 */
void elf_file_dump(const struct elf_file *file, FILE *stream);

#endif /* __ELF_H__ */
