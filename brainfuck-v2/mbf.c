/**
 * @file mbf.c
 * @brief Yet Another brainfuck impelementataion.
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Dec 20, 2019
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (C) 2019, Mehmet Akif Tasova
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define ERR_INVALID_ARGS 1
#define ERR_READ_FILE 10
#define ERR_READ_STDIN 11
#define ERR_BUFF_OVERFLOW 20
#define ERR_BUFF_UNDERFLOW 21

#define BF_BUFFER_LENGTH (1 << 15) /* 2^15 */

#define BUF_SIZ_STDIN 1023

#define print_error(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define print_msg(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)

#ifdef DEBUG
#define print_debug(fmt, ...)                                        \
	fprintf(stderr, "%s:%d: [DBG]" fmt "\n", __FILE__, __LINE__, \
		##__VA_ARGS__)
#else /* ! DEBUG */
#define print_debug(fmt, ...)
#endif /* DEBUG */

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#define SCLOSE(fd)                   \
	do {                         \
		if ((fd) > -1) {     \
			close((fd)); \
			(fd) = -1;   \
		}                    \
	} while (0)

char *file_mmap_readonly(const char *path, off_t *nbytes)
{
	struct stat statbuf;
	char *fmap = NULL;
	int fd = -1;

	if (!path || !nbytes) {
		errno = EINVAL;
		goto out;
	}

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		goto out;
	}

	if (fstat(fd, &statbuf)) {
		goto out;
	}

	if (statbuf.st_size == 0) {
		errno = EIO;
		goto out;
	}
	*nbytes = statbuf.st_size;

	fmap = mmap(NULL, statbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (fmap == MAP_FAILED) {
		fmap = NULL;
		goto out;
	}

out:
	SCLOSE(fd);
	return fmap;
}

char *read_stdin(size_t *len)
{
	char buf[BUF_SIZ_STDIN + 1] = { '\0' };
	char *new_data = NULL;
	char *data = NULL;
	int ret = 0;

	errno = 0;

	if (!len) {
		errno = EINVAL;
		goto out;
	}
	*len = 0;

	data = calloc(1, sizeof(char));
	if (!data) {
		goto out;
	}

	while ((ret = read(STDIN_FILENO, buf, BUF_SIZ_STDIN)) != 0) {
		if (ret < 0) {
			goto out;
		}

		*len += strlen(buf);

		new_data = realloc(data, *len);
		if (!new_data) {
			goto out;
		}

		data = new_data;
		strcat(data, buf);

		memset(buf, '\0', sizeof(buf));
	}

out:
	if (errno) {
		SFREE(data);
		*len = 0;
	}
	return data;
}

void usage(const char *exec)
{
	print_msg("%s - yet another brainfuck interpreter", exec);
	print_msg("usage:\n\t%s [SOURCE]", exec);
	puts("parameters:\n\tSOURCE : A valid Brainfuck source code file");
	puts("\tIf no source file given, program will read stdin.");
	puts("exit codes:");
	print_msg("\t%d\t: Invalid command line argument(s)", ERR_INVALID_ARGS);
	print_msg("\t%d\t: Cannot open/read file", ERR_READ_FILE);
	print_msg("\t%d\t: Cannot read stdin", ERR_READ_STDIN);
	print_msg("\t%d\t: Brainfuck buffer owerflow", ERR_BUFF_OVERFLOW);
	print_msg("\t%d\t: Brainfuck buffer underflow", ERR_BUFF_UNDERFLOW);
}

int main(int argc, char **argv)
{
	int8_t bf_buffer[BF_BUFFER_LENGTH] = { 0 };
	int8_t *bf_buffer_end = NULL;
	char *prg_mem_end = NULL;
	int8_t *bf_ptr = NULL;
	char *prg_mem = NULL;
	char *prg_ptr = NULL;
	size_t prg_len = 0;
	off_t prg_off = 0;
	size_t loop = 0;
	int mmapped = 0;
	int8_t ret = 0;

	if (argc > 2) {
		print_error("invalid arguments");
		usage(argv[0]);
		return ERR_INVALID_ARGS;
	}

	if (argc == 2) {
		prg_mem = file_mmap_readonly(argv[1], &prg_off);
		if (!prg_mem) {
			perror("failed to read source file");
			return ERR_READ_FILE;
		}
		mmapped = 1;
		prg_len = (size_t)prg_off;
	} else {
		prg_mem = read_stdin(&prg_len);
		if (!prg_mem) {
			perror("failed to read stdin");
			return ERR_READ_STDIN;
		}
		mmapped = 0;
	}

	print_debug("prg_mem=%p prg_len=%ld", prg_mem, prg_len);

	prg_mem_end = prg_mem + prg_len;
	prg_ptr = prg_mem;

	print_debug("prg: \"%s\"", prg_mem);

	bf_buffer_end = bf_buffer + BF_BUFFER_LENGTH;
	bf_ptr = bf_buffer;

	while (prg_ptr != prg_mem_end) {
		if ('\0' == *prg_ptr)
			break;

		print_debug("current input: %c at %p", *prg_ptr, prg_ptr);

		switch (*prg_ptr) {
		case '>':
			++bf_ptr;
			if (bf_ptr == bf_buffer_end) {
				print_error("Buffer overflow!");
				ret = ERR_BUFF_OVERFLOW;
				goto out;
			}
			break;
		case '<':
			if (bf_ptr == bf_buffer) {
				print_error("Buffer underflow!");
				ret = ERR_BUFF_UNDERFLOW;
				goto out;
			}
			--bf_ptr;
			break;
		case '+':
			++*bf_ptr;
			break;
		case '-':
			--*bf_ptr;
			break;
		case '.':
			if (*bf_ptr)
				putchar((char)(*bf_ptr));
			break;
		case ',':
			*bf_ptr = (int8_t)getchar();
			break;
		case '[':
			if (*bf_ptr == 0) {
				loop = 1;
				while (loop > 0) {
					++prg_ptr;
					switch (*prg_ptr) {
					case '[':
						++loop;
						break;
					case ']':
						--loop;
						break;
					}
				}
			}
			break;
		case ']':
			loop = 1;
			while (loop > 0) {
				--prg_ptr;
				switch (*prg_ptr) {
				case '[':
					--loop;
					break;
				case ']':
					++loop;
					break;
				}
			}
			--prg_ptr;
			break;
		default:
			break;
		}
		++prg_ptr;
	}

out:
	if (mmapped) {
		munmap(prg_mem, prg_off);
	} else {
		SFREE(prg_mem);
	}
	prg_mem = NULL;
	return ret;
}
