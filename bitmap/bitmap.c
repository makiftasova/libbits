/**
 * @file bitmap.c
 * @brief Impletemtation of a simple bitmap library written in C89
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Nov 21, 2019
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bitmap.h"

#if CHAR_BIT != 8
#error This library does not support platforms where CHAR_BIT != 8
#endif

/**
 * @brief Macro to make free more safer.
 *
 * on some libc implementations, passing NULL to free might cause segmentation
 * faults. Thus wrapping free with some safety checks to prevent such occasions.
 */
#define SFREE(ptr)                    \
	do {                          \
		if ((ptr) != NULL) {  \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

/**
 * @brief Base struct for bitmap library.
 */
struct bitmap {
	size_t nbytes; /**< number of bytes in bitmap */
	uint8_t *bytes; /**< poiner to array of bits */
};

/**
 * @brief Macro to calculate number of bits in bitmap
 */
#define _NBITS(bitmap) ((bitmap)->nbytes * CHAR_BIT)

/**
 * @brief Macro to generate bitmask for given bit index. Only works in 8 bits.
 */
#define _MASK(bit) (1 << ((bit) % CHAR_BIT))

/**
 * @brief Macro to find out number of bytes to skip when dealing with multi
 * byte data.
 */
#define _FIND_BYTE(bit) (bit / CHAR_BIT)

/**
 * @brief Macro to calculate address of the byte which contains requested bit.
 */
#define _GET_BYTE_ADDR(bitmap, bit)                    \
	((_FIND_BYTE((bit)) < (bitmap)->nbytes) ?      \
		 ((bitmap)->bytes + _FIND_BYTE(bit)) : \
		 (NULL))

bitmap_t bitmap_create(const void *data, const size_t nbytes)
{
	bitmap_t bmap = BITMAP_FAILED;

	if (data == NULL || nbytes == 0) {
		errno = EINVAL;
		return BITMAP_FAILED;
	}

	bmap = calloc(1, sizeof(*bmap));
	if (bmap == NULL) {
		return BITMAP_FAILED;
	}

	bmap->bytes = calloc(nbytes, sizeof(uint8_t));
	if (bmap->bytes == NULL) {
		bitmap_destroy(bmap);
		return BITMAP_FAILED;
	}

	bmap->nbytes = nbytes;
	memcpy(bmap->bytes, data, nbytes);

	return bmap;
}

bitmap_t bitmap_duplicate(const bitmap_t bitmap)
{
	return bitmap_create(bitmap->bytes, bitmap->nbytes);
}

void bitmap_destroy(bitmap_t bitmap)
{
	if (bitmap != BITMAP_FAILED) {
		if (bitmap->bytes != NULL) {
			free(bitmap->bytes);
		}
		free(bitmap);
	}
}

int bitmap_get_bit(const bitmap_t bitmap, const size_t bit)
{
	const uint8_t *const byte = _GET_BYTE_ADDR(bitmap, bit);
	if (byte == NULL) {
		errno = ERANGE;
		return -1;
	}

	return (*byte & _MASK(bit)) ? 1 : 0;
}

unsigned char bitmap_get_bit_uc(const bitmap_t bitmap, const size_t bit)
{
	const uint8_t *const byte = _GET_BYTE_ADDR(bitmap, bit);
	if (byte == NULL) {
		errno = ERANGE;
		return 0U;
	}
	return (*byte & _MASK(bit)) ? 1U : 0U;
}

size_t bitmap_get_num_bytes(const bitmap_t bitmap)
{
	if (bitmap) {
		return bitmap->nbytes;
	}
	errno = EINVAL;
	return 0;
}

unsigned char *bitmap_get_bytes(const bitmap_t bitmap, size_t *nbytes)
{
	uint8_t *dup = NULL;

	if (bitmap == BITMAP_FAILED) {
		errno = EINVAL;
		return NULL;
	}

	dup = calloc(bitmap->nbytes, sizeof(*dup));
	if (dup == NULL) {
		return NULL;
	}

	memcpy(dup, bitmap->bytes, bitmap->nbytes);

	if (nbytes != NULL) {
		*nbytes = bitmap->nbytes;
	}

	return dup;
}

int bitmap_set_bit(bitmap_t bitmap, const size_t bit)
{
	uint8_t *const byte = _GET_BYTE_ADDR(bitmap, bit);
	if (byte == NULL) {
		errno = ERANGE;
		return -1;
	}

	*byte |= _MASK(bit);
	return 0;
}

int bitmap_clear_bit(bitmap_t bitmap, const size_t bit)
{
	uint8_t *const byte = _GET_BYTE_ADDR(bitmap, bit);
	if (byte == NULL) {
		errno = ERANGE;
		return -1;
	}

	*byte &= ~(_MASK(bit));
	return 0;
}

int bitmap_dump_to_string(const bitmap_t bitmap, unsigned char bbl,
			  unsigned char bpl, char *buf, const size_t bufsize)
{
	size_t buflen = 0;
	size_t i = 0;
	int j = 0;

	if (bitmap == BITMAP_FAILED || buf == NULL) {
		errno = EINVAL;
		return -1;
	}

	/* bbl = bit block length */
	if (bbl > 8 || bbl == 0) {
		bbl = 8;
	}

	/* bpl = bits per line */
	if (bpl > 32 || bpl == 0) {
		bpl = 32;
	}

	buflen = _NBITS(bitmap) + (_NBITS(bitmap) / bbl) +
		 (_NBITS(bitmap) / bpl) + 1;

	if (buflen < bufsize) {
		errno = ENOBUFS;
		return -1;
	}

	for (i = 0; i < _NBITS(bitmap); ++i) {
		j += sprintf(buf + j, "%d", bitmap_get_bit(bitmap, i));
		if ((i + 1) % bbl == 0) {
			j += sprintf(buf + j, " ");
		}
		if ((i + 1) % bpl == 0) {
			j += sprintf(buf + j, "\n");
		}
	}

	if (i % bpl != 0) {
		j += sprintf(buf + j, "\n");
	}

	return j;
}

int bitmap_dump_bits_format(const bitmap_t bitmap, unsigned char bbl,
			    unsigned char bpl)
{
	int ret = 0;
	size_t bufsize = 0;
	char *buf = NULL;

	if (bitmap == BITMAP_FAILED) {
		errno = EINVAL;
		return -1;
	}

	bufsize = _NBITS(bitmap) + (_NBITS(bitmap) / bbl) +
		  (_NBITS(bitmap) / bpl) + 1;

	buf = calloc(bufsize, sizeof(char));
	if (buf == NULL) {
		return -1;
	}

	/*
	 * bbl = bit block length
	 * bpl = bits per line
	 */
	ret = bitmap_dump_to_string(bitmap, bbl, bpl, buf, bufsize);
	if (ret < 0) {
		SFREE(buf);
		return -1;
	}

	printf("%s", buf);

	SFREE(buf);
	return ret;
}

int bitmap_dump_bits(const bitmap_t bitmap)
{
	return bitmap_dump_bits_format(bitmap, 4, 32);
}
