# Notes

* image file code-1839406_1920.jpg acquired from [pixabay](https://pixabay.com/photos/code-code-editor-coding-computer-1839406 "Code Editor Coding - Free photo on Pixabay")

* code.bmp is created from code-1839406_1920.jpg file with following command:
```
convert code-1839406_1920.jpg  code.bmp
```

* `convert` is a part of [ImageMagick](https://imagemagick.org "ImageMagick - Convert, Edit, or Compose Bitmap Images")
