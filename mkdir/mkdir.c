/**
 * @file mkdir.c
 * @brief Partial reimplementation of UNIX's mkdir command
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Nov 21, 2019
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define PATH "/tmp/dir1/dir2/dir3"

/* default mode flags (0755) for mkdir. */
#define MODE_DEFAULT (S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)

#define UNUSED(var) ((void)(var))

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#ifdef DEBUG
#define PRINTF(fmt, ...)                                              \
	fprintf(stdout, "%s:%d in %s: " fmt "\n", __FILE__, __LINE__, \
		__func__, ##__VA_ARGS__)
#define PRINT_ERROR(fmt, ...)                                             \
	fprintf(stderr, "%s:%d in %s: " fmt ": %s\n", __FILE__, __LINE__, \
		__func__, ##__VA_ARGS__, strerror(errno))
#else
#define PRINTF(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)
#define PRINT_ERROR(fmt, ...) \
	fprintf(stderr, fmt ": %s\n", ##__VA_ARGS__, strerror(errno))
#endif

typedef int (*mkdir_func_t)(const char *path, mode_t mode);

/**
 * Create an empty directory at given path with given mode flags.
 * If parent directories are not exists, create them before creating the target
 * directory
 *
 * see `man 2 mkdir`, `man 2 stat`, and `man 3 strdup` for details.
 * see `man 2 open` for possible mode flags.
 *
 * @param[in] pathname directory path to create;
 * @param[in] mode mode of the directory.
 * @return returns 0 on success. on error, return -1 and sets errno.
 */
int mkdir_p(const char *pathname, mode_t mode)
{
	size_t len = 0;
	struct stat sb;
	char *path = NULL;
	char *p = NULL;

	memset(&sb, 0, sizeof(sb));

	errno = 0;
	if (stat(pathname, &sb) == 0) {
		errno = EEXIST;
		return -1;
	}

	path = strdup(pathname);
	if (path == NULL) {
		return -1;
	}

	len = strlen(path) - 1;
	while (path[len] == '/') {
		path[len--] = '\0';
	}
	if (*path == '\0') {
		errno = EINVAL;
		goto fail;
	}

	p = path;
	/* skip first / for absolute paths */
	if (*p == '/') {
		++p;
	}
	while (*p) {
		if (*p == '/') {
			*p = '\0';
			if (stat(path, &sb) != 0) {
				errno = 0;
				if (mkdir(path, mode) < 0) {
					goto fail;
				}
			} else if (!S_ISDIR(sb.st_mode)) {
				errno = ENOTDIR;
				goto fail;
			}
			*p = '/';
		}
		++p;
	}

	if (stat(path, &sb) != 0) {
		errno = 0;
		if (mkdir(path, mode) != 0) {
			goto fail;
		}
	} else if (!S_ISDIR(sb.st_mode)) {
		errno = ENOTDIR;
		goto fail;
	}

	SFREE(path);
	return 0;

fail:
	SFREE(path);
	return -1;
}

static mode_t g_old_umask = 0;

void restore_umask(void)
{
#ifdef DEBUG
	PRINTF("restore umask");
#endif
	umask(g_old_umask);
}

void usage(const char *const exec)
{
	printf("Usage: %s [OPTION]... DIRECTORY...\n", exec);
	puts("Create the DIRECTORY(ies), if they do not already exists.");
	puts("OPTIONS:");
	puts("\t-p\tno error if existing, make parent directories as needed.");
	puts("\t-m\tset file mode (as in chmod), not a=rwx - umask.");
	puts("\t-h\tdipslay this help and exit.");
}

#define OPERAND_CHECK(expr)                                       \
	do {                                                      \
		if (!(expr)) {                                    \
			PRINTF("%s: missing operand\n", argv[0]); \
			usage(argv[0]);                           \
			return EXIT_FAILURE;                      \
		}                                                 \
	} while (0)

#ifdef DEBUG
#define LOG_PARAMS(mode, path, func)                                         \
	do {                                                                 \
		PRINTF("mode=0%lo (%d)", (mode), (mode));                    \
		PRINTF("path=\"%s\"", (path) ? (path) : "(null)");           \
		PRINTF("type=%s", (func) == mkdir_p ? "parents" : "normal"); \
	} while (0)
#endif

int main(int argc, char *argv[])
{
	char opt;
	unsigned long int raw_mode = 0;
	mode_t mode = MODE_DEFAULT; /* 0755 */
	const char *path = NULL;
	mkdir_func_t mkdir_func = mkdir;

	g_old_umask = umask(0000); /* clear effective umask */
	atexit(restore_umask); /* restore original umask at exit */

#ifdef DEBUG
	LOG_PARAMS(mode, path, mkdir_func);
#endif

	OPERAND_CHECK(argc > 1);

	while ((opt = getopt(argc, argv, "pm:h")) != -1) {
		switch (opt) {
		case 'p':
			/* might need to create parent directories */
			mkdir_func = mkdir_p;
			break;
		case 'm':
			errno = 0;
			raw_mode = strtoul(optarg, NULL, 8);
			if (errno != 0 || raw_mode > 0777 || raw_mode == 0) {
				errno = EINVAL;
				PRINT_ERROR("mode string %s is not valid",
					    optarg);
				usage(argv[0]);
				return EXIT_FAILURE;
			}
			mode = (mode_t)raw_mode;
			break;
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
		default:
			PRINTF("unknown option %c\n", opt);
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}

	OPERAND_CHECK(optind < argc);
	path = argv[optind];

#ifdef DEBUG
	LOG_PARAMS(mode, path, mkdir_func);
#endif

	if (mkdir_func(path, mode) != 0) {
		PRINT_ERROR("failed to create directory: %s", path);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
#undef OPERAND_CHECK
#ifdef DEBUG
#undef LOG_PARAMS
#endif
