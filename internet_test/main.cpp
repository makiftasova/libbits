/**
 * @file main.cpp
 * @brief Exampel usage of a parser for /proc/net/route
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <iostream>

#include <unistd.h>

#include "iface.hpp"
#include "pingv4.hpp"
#include "route.hpp"

void help(void)
{
	std::cout << "-I INTERFACE" << std::endl << "-c COUNT" << std::endl;
}

int main(int argc, char **argv)
{
	std::string host("example.com");
	uint16_t count = UINT16_MAX;
	std::string ifname("");
	int opt = 0;

	while ((opt = getopt(argc, argv, "I:c:h")) != -1) {
		switch (opt) {
		case 'c':
			count = std::stoul(optarg, nullptr, 10);
			if (count == 0)
				count = UINT16_MAX;
			break;
		case 'I':
			ifname = optarg;
			break;
		case 'h':
			help();
			return 0;
		}
	}

	if (optind >= argc) {
		std::cout << "Expected argument after options" << std::endl;
		return -1;
	}
	host = argv[optind];

	if (ifname.length() == 0) {
		netutils::routes routes;
		ifname = routes.get_preferred_route().get_iface();
	}

	std::cout << "Using interface " << ifname << std::endl;

	netutils::iface iface(ifname);
	std::cout << iface << std::endl;

	if (!iface.is_active()) {
		if (!iface.is_up())
			std::cout << ifname << " is down!" << std::endl;
		if (!iface.is_running())
			std::cout << ifname << " is not running!" << std::endl;
		return -2;
	}

	std::cout << "interface " << ifname
		  << " seems to be working. continuing with ping test"
		  << std::endl;

	netutils::pingv4 ping(ifname, host, count);
	ping.set_timeout(std::chrono::milliseconds(5000)); /* 5 seconds */

	std::cout << "waiting for ping results..." << std::endl;

	ping.run();

	auto ping_results = ping.get_results();
	size_t num_timeouts = 0;
	size_t num_unreached = 0;
	double to_percent = 0.0L;
	double unreach_percent = 0.0L;

	for (const netutils::ping_result &res : ping_results) {
		if (res.is_timedout())
			++num_timeouts;
		if (res.is_unreachable())
			++num_unreached;
		std::cout << res << std::endl;
	}
	to_percent = static_cast<double>(num_timeouts) / ping_results.size();
	unreach_percent =
		static_cast<double>(num_unreached) / ping_results.size();

	if (to_percent > 0.5L)
		std::cout << "More than 50% of ping requests timed out."
			  << std::endl;

	if (unreach_percent > 0.5L)
		std::cout
			<< "More than 50% of ping packets could not reach their target."
			<< std::endl;

	std::cout << num_timeouts << "/" << ping_results.size()
		  << " requests timed out." << std::endl;

	std::cout << num_unreached << "/" << ping_results.size()
		  << " requests didn't reached the target." << std::endl;
	return 0;
}
