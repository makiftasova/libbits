/**
 * @file ipv4.cpp
 * @brief Implementation file of simple IPv4 class
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Oct 02, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <exception>
#include <sstream>

#include "common.hpp"
#include "ipv4.hpp"

netutils::ipv4::ipv4()
	: _octets{ 0, 0, 0, 0 }
{
	/* left blank */
}
netutils::ipv4::ipv4(const ipv4 &ip)
	: _octets{ ip._octets[0], ip._octets[1], ip._octets[2], ip._octets[3] }
{
	/* left blank */
}

netutils::ipv4::ipv4(const std::string &s)
{
	if (this->from_string(s))
		throw std::invalid_argument("Given string " + s +
					    " is not a valid IPv4 Address");
}

bool netutils::ipv4::is_zero(void) const
{
	return (this->_octets[0] | this->_octets[1] | this->_octets[2] |
		this->_octets[3]) == 0;
}

bool netutils::ipv4::from_string(const std::string &s)
{
	uint8_t addr[4];

	/* try to parse hex string. e.g.: 0002A8C0 */
	if (s.length() == 8 && s.find('.') == std::string::npos) {
		const char *cs = s.c_str();
		char octet[3] = { 0, 0, 0 };

		for (size_t i = 0; i < ARRAY_LEN(this->_octets); ++i) {
			octet[0] = cs[2 * i];
			octet[1] = cs[2 * i + 1];

			this->_octets[3 - i] = std::stoul(octet, nullptr, 16);
		}
		goto parse_ok;
	}

	/* try to parse good old dot notation e.g.: 192.168.2.0 */
	if (!inet_pton(AF_INET, s.c_str(), addr))
		goto parse_fail;

	for (size_t i = 0; i < ARRAY_LEN(this->_octets); ++i)
		this->_octets[i] = static_cast<unsigned short>(addr[i]);

parse_ok:
	return false;

parse_fail:
	return true;
}

std::string netutils::ipv4::to_string(void) const
{
	if (this->is_zero())
		return std::string("0.0.0.0");

	std::stringstream ss;
	ss << this->_octets[0] << '.' << this->_octets[1] << '.'
	   << this->_octets[2] << '.' << this->_octets[3];
	return ss.str();
}

const netutils::ipv4 &netutils::ipv4::operator=(const netutils::ipv4 &ip)
{
	if (this == &ip)
		goto skip;

	this->_octets[0] = ip._octets[0];
	this->_octets[1] = ip._octets[1];
	this->_octets[2] = ip._octets[2];
	this->_octets[3] = ip._octets[3];

skip:
	return *this;
}

bool netutils::ipv4::operator==(const netutils::ipv4 &ip) const
{
	if (this == &ip)
		return true;

	return (this->_octets[0] == ip._octets[0]) &&
	       (this->_octets[1] == ip._octets[1]) &&
	       (this->_octets[2] == ip._octets[2]) &&
	       (this->_octets[3] == ip._octets[3]);
}

std::ostream &netutils::operator<<(std::ostream &os, const netutils::ipv4 &ip)
{
	os << ip.to_string();
	return os;
}
