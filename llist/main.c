/**
 * @file main.c
 * @author Mehmet Akif TASOVA <makiftasova@gmail.com>
 * @date Dec 25, 2019
 * @brief Test/Example file for an implementation of Single Linked List
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif TASOVA.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "llist.h"

#define print_err_fmt(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)
#define print_out_fmt(fmt, ...) fprintf(stdout, fmt "\n", ##__VA_ARGS__)

#define print_err(str) fprintf(stderr, str "\n")
#define print_out(str) fprintf(stdout, str "\n")

llist_iter_retval_t llist_cb_print_all(struct ll_node *node)
{
	ll_node_dump(node, STDOUT_FILENO);
	return LLIST_ITER_CONTINUE;
}

llist_iter_retval_t llist_cb_delete_all(struct ll_node *node)
{
	ll_node_dump_msg(node, "deleting",  STDOUT_FILENO);
	return LLIST_ITER_DELETE;
}

llist_iter_retval_t llist_cb_delete_odd(struct ll_node *node)
{
	char *data = (char *)node->data;

	ll_node_dump_msg(node, "processing", STDOUT_FILENO);
	if (*(data + 4) % 2 == 1) {
		return LLIST_ITER_DELETE;
	}
	return LLIST_ITER_CONTINUE;
}

int main(int argc, char *argv[])
{
	struct llist *list = NULL;
	char *str[] = { "test1", "test2", "test3", NULL };
	char **s = str;

	(void)argc;
	(void)argv;

	print_err("setting up");

	list = llist_create_empty();
	if (!list) {
		perror("cannot create llist");
		return EXIT_FAILURE;
	}

	print_err("list created");

	while (*s) {
		if (llist_append_str(list, *s)) {
			perror("failed to add data to list");
			llist_destroy(list);
			return EXIT_FAILURE;
		}
		++s;
	}

	print_err("list filled");

	llist_for_each(list, llist_cb_print_all);

	print_err("list dump OK");

	print_err("test data delete");

	if (llist_delete_str(list, "test2")) {
		perror("failed to delete data");
	}

	if (llist_delete_str(list, "test3")) {
		perror("failed to delete data");
	}

	if (llist_delete_str(list, "test1")) {
		perror("failed to delete data");
	}

	print_err("test data delete DONE");

	print_err_fmt("list->head=%p", (void *)list->head);

	llist_for_each(list, llist_cb_print_all);

	print_err("list dump OK");

	print_err("repopulate list");

	s = str;
	while (*s) {
		if (llist_append_str(list, *s)) {
			perror("failed to add data to list");
			llist_destroy(list);
			return EXIT_FAILURE;
		}
		++s;
	}
	print_err_fmt("list->head=%p", (void *)list->head);

	print_err("repopulate OK");

	llist_for_each(list, llist_cb_delete_all);

	print_err("list cleanup OK");
	print_err_fmt("list->head=%p", (void *)list->head);

	print_err("repopulate list");

	s = str;
	while (*s) {
		if (llist_append_str(list, *s)) {
			perror("failed to add data to list");
			llist_destroy(list);
			return EXIT_FAILURE;
		}
		++s;
	}
	print_err_fmt("list->head=%p", (void *)list->head);

	print_err("repopulate OK. deletind odd test items");

	llist_for_each(list, llist_cb_delete_odd);

	print_err("result:");
	llist_for_each(list, llist_cb_print_all);

	print_err("list dump OK");

	llist_destroy(list);

	print_err("cleanup OK");

	return EXIT_SUCCESS;
}
