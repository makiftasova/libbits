/**
 * @file autofree.h
 * @author Mehmet Akif TASOVA <makiftasova@gmail.com>
 * @date Jul 05, 2021
 * @brief Header file for a collection of macros ustilizing gcc's "cleanup" attribute
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif TASOVA.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __AUTOFREE_H__
#define __AUTOFREE_H__

#define __AUTOFREE_USING_TYPE(type_name, type)              \
	static void __autofree_free_##type_name(type **buf) \
	{                                                   \
		if (*buf) {                                 \
			free(*buf);                         \
			*buf = NULL;                        \
		}                                           \
	}

#define __AUTOFREE_USING_BASIC_TYPE(type) __AUTOFREE_USING_TYPE(type, type)

#define AUTOFREE_USING_VOID __AUTOFREE_USING_BASIC_TYPE(void)
#define AUTOFREE_USING_CHAR __AUTOFREE_USING_BASIC_TYPE(char)
#define AUTOFREE_USING_UCHAR __AUTOFREE_USING_TYPE(unsigned_char, unsigned char)
#define AUTOFREE_USING_SHORT __AUTOFREE_USING_BASIC_TYPE(short)
#define AUTOFREE_USING_USHORT \
	__AUTOFREE_USING_TYPE(unsigned_short, unsigned short)
#define AUTOFREE_USING_INT __AUTOFREE_USING_BASIC_TYPE(int)
#define AUTOFREE_USING_UINT __AUTOFREE_USING_TYPE(unsigned_int, unsigned int)
#define AUTOFREE_USING_LONG __AUTOFREE_USING_BASIC_TYPE(long)
#define AUTOFREE_USING_ULONG __AUTOFREE_USING_TYPE(unsigned_long, unsigned long)
#define AUTOFREE_USING_LONGLONG __AUTOFREE_USING_TYPE(long_long, long long)
#define AUTOFREE_USING_ULONGLONG \
	__AUTOFREE_USING_TYPE(unsigned_long_long, unsigned long long)

#define AUTOFREE_USING_CUSTOM(type) __AUTOFREE_USING_BASIC_TYPE(type)
#define AUTOFREE_USING_CUSTOM_LONG(type_name, type) \
	__AUTOFREE_USING_TYPE(type_name, type)

#define __AUTOFREE_TYPE(type_name, type, name) \
	__attribute__((cleanup(__autofree_free_##type_name))) type *name

#define __AUTOFREE_BASIC_TYPE(type, name) __AUTOFREE_TYPE(type, type, name)

#define AUTOFREE_VOID(name) __AUTOFREE_BASIC_TYPE(void, name)
#define AUTOFREE_CHAR(name) __AUTOFREE_BASIC_TYPE(char, name)
#define AUTOFREE_UCHAR(name) __AUTOFREE_TYPE(unsigned_char, unsigned char, name)
#define AUTOFREE_SHORT(name) __AUTOFREE_BASIC_TYPE(short, name)
#define AUTOFREE_USHORT(name) \
	__AUTOFREE_TYPE(unsigned_short, unsigned short, name)
#define AUTOFREE_INT(name) __AUTOFREE_BASIC_TYPE(int, name)
#define AUTOFREE_UINT(name) __AUTOFREE_TYPE(unsigned_int, unsigned int, name)
#define AUTOFREE_LONG(name) __AUTOFREE_BASIC_TYPE(long, name)
#define AUTOFREE_ULONG(name) __AUTOFREE_TYPE(unsigned_long, unsigned long, name)
#define AUTOFREE_LONGLONG(name) __AUTOFREE_TYPE(long_long, long long, name)
#define AUTOFREE_ULONGLONG(name) \
	__AUTOFREE_TYPE(unsigned_long_long, unsigned long long, name)

#define AUTOFREE_CUSTOM(type, name) __AUTOFREE_BASIC_TYPE(type, name)
#define AUTOFREE_CUSTOM_LONG(type_name, type, name) \
	__AUTOFREE_TYPE(type_name, type, name)

#endif /* __AUTOFREE_H__ */
