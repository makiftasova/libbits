#List of examples
* add.bf: simple brainfuck code to calculate 2 + 5.
* bf.bf: brainfuck program to print Brainfuc on terminal.
* bf_overflow.bf: brainfuck program to trigger internal buffer overflow for
interpreter.
* bf_underflow.bf: brainfuck program to trigger internal buffer underflow for
interpreter.
* gol.bf: Game of Life implemented in brainfuck.
* hello_world.bf: brainfuck program to print hello world on terminal.
* hello_world_short.bf: brainfuck program to print hello world on terminal,
without any inline comments.
* rot13.bf: encode/decode user input with ROT13 cipher
* test.bf: A simple test program to check if brainfuck interpreter works.
Outputs character `A` followed by new line to terminal.
