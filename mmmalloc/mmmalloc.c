/**
 * @file mmmalloc.c
 * @brief Source code file of a simple mmap backed malloc implementation
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date Mar 17, 2021
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2021, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include <mmmalloc.h>

#define UNUSED(var) ((void)(var))

#define MMM_PROT (PROT_READ | PROT_WRITE)
#define MMM_MAP (MAP_PRIVATE | MAP_ANONYMOUS)

#define MMM_ERRORF(fmt, ...) fprintf(stderr, fmt "\n", ##__VA_ARGS__)

#define MMM_PERRORF(fmt, ...) \
	MMM_ERRORF(fmt ": %s", ##__VA_ARGS__, strerror(errno))

struct mmm_header {
	size_t length;
};

#define MMM_HEADER_SIZE (sizeof(struct mmm_header))

struct mmm_chunk {
	struct mmm_header header;
	char buf[];
};

#define MMM_LEN(chunk) ((chunk)->header.length)

#define MMM_TO_PTR(chunk) ((void *)(&((chunk)->buf)))

#define MMM_FROM_PTR(ptr) ((struct mmm_chunk *)(((char *)(ptr))-MMM_HEADER_SIZE))

struct mmm_chunk *__mmm_alloc_chunk(size_t size)
{
	struct mmm_chunk *chunk = NULL;
	const size_t length = size + MMM_HEADER_SIZE;

	if (length < size) {
		/* TODO: indicate overflow rather than saying we are out of memory */
		errno = ENOMEM;
		return NULL;
	}

	chunk = mmap(NULL, length, MMM_PROT, MMM_MAP, -1, 0);
	if (chunk == MAP_FAILED) {
		MMM_PERRORF("mmap failed");
		errno = ENOMEM;
		return NULL;
	}

	MMM_LEN(chunk) = length;
	return chunk;
}

void *mmm_malloc(size_t size)
{
	struct mmm_chunk *chunk = __mmm_alloc_chunk(size);

	if (!chunk) {
		errno = ENOMEM;
		return NULL;
	}

	return MMM_TO_PTR(chunk);
}

void mmm_free(void *ptr)
{
	struct mmm_chunk *chunk = MMM_FROM_PTR(ptr);

	/* TODO: check if we really allocated this chunk */

	if (munmap((void *)chunk, MMM_LEN(chunk))) {
		MMM_PERRORF("munmap failed");
		abort();
	}
}

void *mmm_calloc(size_t nmemb, size_t size)
{
	return mmm_malloc(nmemb * size);
}

void *mmm_realloc(void *ptr, size_t size)
{
	/* TODO: check whether we allocated ptr in the first place */
	struct mmm_chunk *old = MMM_FROM_PTR(ptr);
	struct mmm_chunk *new = NULL;
	size_t copy_len = 0;

	/* ptr != NULL && size == 0 means we act like free */
	if (ptr && !size) {
		mmm_free(ptr);
		return NULL;
	}

	/* ptr == NULL && size != 0 means we act like malloc */
	if (!ptr && size) {
		return mmm_malloc(size);
	}

	/* No need to play with memory */
	if (size == MMM_LEN(old)) {
		return ptr;
	}

	/* allocate new chunk */
	new = __mmm_alloc_chunk(size);
	if (!new) {
		return NULL;
	}

	/* move data to new memory region */
	copy_len = size < MMM_LEN(old) ? size : MMM_LEN(old);
	memcpy(MMM_TO_PTR(new), MMM_TO_PTR(old), copy_len);

	/* free old region */
	mmm_free(ptr);

	return MMM_TO_PTR(new);
}
