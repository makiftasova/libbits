/*
 * @file l2msg.c
 * @author Mehmet Akif Tasova <makiftasova@gmail.com>
 * @date August 05, 2024
 * @brief An ASCII text message broadcaster and receiver for OSI layer 2
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2024, Mehmet Akif Tasova.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <arpa/inet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <netpacket/packet.h>

#include "l2msg_common.h"
#include "l2msg_message.h"

static atomic_bool __run_state = false;

static void sigaction_sigterm(int sig __UNUSED, siginfo_t *info __UNUSED,
			      void *ucontext __UNUSED)
{
	puts("SIGINT received");
	__run_state = false;
}

static int set_signal_handlers(void)
{
	int ret = 0;
	static struct sigaction _sa_term;

	memset(&_sa_term, 0, sizeof(_sa_term));

	_sa_term.sa_sigaction = sigaction_sigterm;
	_sa_term.sa_flags = SA_SIGINFO;

	ret = sigaction(SIGTERM, &_sa_term, NULL);
	ret |= sigaction(SIGINT, &_sa_term, NULL);

	return ret;
}

static int get_if_index(int sock, const char *const ifname)
{
	struct ifreq if_idx;
	int ret = -1;

	memset(&if_idx, 0, sizeof(if_idx));
	memccpy(if_idx.ifr_name, ifname, '\0', sizeof(if_idx.ifr_name) - 1);

	errno = 0;
	ret = ioctl(sock, SIOCGIFINDEX, &if_idx);
	if (ret < 0) {
		fprintf(stderr, "Cannot get if index for interface %s: %s\n",
			ifname, strerror(errno));
		return -1;
	}

	return if_idx.ifr_ifindex;
}

int recieve_loop(const char *const ifname)
{
	struct sockaddr_ll sock_addr = { .sll_family = AF_PACKET };
	msg_status_e status = MSG_STATUS_OK;
	int ret = EXIT_FAILURE;
	message_t message;
	int sock = -1;

	sock = socket(PF_PACKET, SOCK_RAW, htons(MSG_PROTO_ID));
	if (sock < 0) {
		perror("Failed to open socket for listening");
		goto fail;
	}

	sock_addr.sll_ifindex = get_if_index(sock, ifname);
	if (sock_addr.sll_ifindex < 0) {
		perror("Cannot get ifindex");
		goto fail;
	}
	sock_addr.sll_protocol = htons(MSG_PROTO_ID);

	ret = bind(sock, (const struct sockaddr *)&sock_addr,
		   sizeof(sock_addr));
	if (ret != 0) {
		perror("Failed to bind");
		goto fail;
	}

#if defined(DEBUG)
	fprintf(stderr, "setting up signal handlers\n");
#endif /*defined(DEBUG) */

	set_signal_handlers();
	__run_state = true;

	while (__run_state) {
#if defined(DEBUG)
		fprintf(stderr, "Waiting for message\n");
#endif /*defined(DEBUG) */
		status = message_recv(ifname, sock, &message);
		if (status != MSG_STATUS_OK) {
			fprintf(stderr, "Cannot receive message: %s\n",
				msg_status_to_string(status));
			continue;
		}

		message_print(stdout, &message);
	}

	ret = EXIT_SUCCESS;

fail:
	CLOSE(sock);
	return ret;
}

int send_message(const char *const ifname, const struct ether_addr *dst,
		 const char *msg)
{
	msg_status_e status = MSG_STATUS_OK;
	int ret = EXIT_FAILURE;
	message_t message;
	int sock = -1;

	sock = socket(PF_PACKET, SOCK_RAW, htons(MSG_PROTO_ID));
	if (sock < 0) {
		perror("Failed to open socket for sending");
		goto fail;
	}

	status = message_from_text(&message, msg);
	if (status != MSG_STATUS_OK) {
		fprintf(stderr,
			"Cannot create message_t object from text \"%s\": %s\n",
			msg, msg_status_to_string(status));
		goto fail;
	}

	status = message_set_destination(&message, dst);
	if (status != MSG_STATUS_OK) {
		fprintf(stderr, "Cannot set message destination: %s\n",
			msg_status_to_string(status));
		goto fail;
	}

	status = message_send(ifname, sock, &message);
	if (status != MSG_STATUS_OK) {
		fprintf(stderr, "Could not send message: %s\n",
			msg_status_to_string(status));
		goto fail;
	}

	ret = EXIT_SUCCESS;

fail:
	CLOSE(sock);
	return ret;
}

void usage(const char *const exec)
{
	printf("%s - send/recive ASCII text messages over OSI layer 2\n", exec);
	puts("Options:");
	puts("\t-h        : Print this help message");
	puts("\t-i IFNAME : Network interface to send the packet (mandatory)");
	puts("\t-m MAC    : Set destination mac (FF:FF:FF:FF:FF:FF by default)");
	puts("\t-r        : Run in recieve-only mode");
	puts("\t-s MSG    : Broadcast given MSG to network");
}

int main(__UNUSED int argc, __UNUSED char *argv[])
{
	char mac_str[sizeof("FF:FF:FF:FF:FF:FF")] = { 0 };
	struct ether_addr dst = ETHER_BCAST_ADDR;
	const char *msg_to_send = NULL;
	const char *ifname = NULL;
	bool recv_mode = false;
	int opt = 0;

	while ((opt = getopt(argc, argv, "hi:m:rs:")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
		case 'i':
			ifname = optarg;
			break;
		case 'm':
			ether_aton_r(optarg, &dst);
			break;
		case 'r':
			recv_mode = true;
			break;
		case 's':
			msg_to_send = optarg;
			break;
		default:
			fprintf(stderr, "invalid parameter %c\n", opt);
			usage(argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (ifname == NULL) {
		fprintf(stderr, "No interface name given!\n");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if (recv_mode > 0)
		return recieve_loop(ifname);

	if (msg_to_send == NULL) {
		fprintf(stderr, "No message provided!\n");
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	if (memcmp(&dst, &ETHER_INVALID_ADDR, sizeof(dst)) == 0) {
		fprintf(stderr, "Invalid destination MAC Address: %s\n",
			ether_ntoa_r(&dst, mac_str));
		return EXIT_FAILURE;
	}

	return send_message(ifname, &dst, msg_to_send);
}
