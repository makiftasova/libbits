/**
 * @file llist.c
 * @author Mehmet Akif TASOVA <makiftasova@gmail.com>
 * @date Dec 25, 2019
 * @brief Source file for an implementation of Single Linked List
 *
 * Licensed under 3-clause BSD license
 *
 * Copyright (c) 2019, Mehmet Akif TASOVA.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Copyright Holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "llist.h"

#define SFREE(ptr)                    \
	do {                          \
		if ((ptr)) {          \
			free((ptr));  \
			(ptr) = NULL; \
		}                     \
	} while (0)

#define llist_true (0 == 0)
#define llist_false (0 == 1)

void ll_node_free(struct ll_node *node)
{
	if (node) {
		SFREE(node->data);
		free(node);
	}
}

int ll_node_equal(const struct ll_node *n1, const struct ll_node *n2)
{
	return (!n1 || !n2) ? llist_false :
			      (!n1->data || !n2->data) ?
			      llist_false :
			      (n1 == n2) ?
			      llist_true :
			      (n1->data_len != n2->data_len) ?
			      llist_false :
			      memcmp(n1->data, n2->data, n1->data_len) == 0 ?
			      llist_true :
			      llist_false;
}

struct ll_node *ll_node_copy(const struct ll_node *orig)
{
	struct ll_node *new = NULL;

	if (!orig) {
		errno = EINVAL;
		goto fail;
	}

	new = calloc(1, sizeof(*new));
	if (!new) {
		goto fail;
	}

	if (orig->data && orig->data_len) {
		new->data = calloc(orig->data_len, sizeof(unsigned char));
		if (!new->data) {
			SFREE(new);
			goto fail;
		}

		memcpy(new->data, orig->data, orig->data_len);
		new->data_len = orig->data_len;
		new->next = NULL;
	}

	return new;
fail:
	return NULL;
}

void ll_node_dump_msg(const struct ll_node *n, const char *msg, int fd)
{
	char *buf = NULL;
	int offset = 0;
	size_t buf_size = 0;

	if (!n || !n->data || !n->data_len) {
		return;
	}

	buf_size = n->data_len + 2;
	if (msg) {
		buf_size += strlen(msg) + 2;
	}

	buf = calloc(buf_size, sizeof(*buf));
	if (!buf) {
		errno = 0;
		return;
	}

	if (msg) {
		offset += snprintf(buf, buf_size, "%s: ", msg);
	}

	memcpy(buf + offset, n->data, n->data_len);
	offset += n->data_len;
	*(buf + offset) = '\n';

	write(fd, buf, sizeof(buf) - 1);
	fsync(fd);
	free(buf);
}

void ll_node_dump(const struct ll_node *n, int fd)
{
	ll_node_dump_msg(n, NULL, fd);
}

struct llist *llist_create(struct ll_node *init)
{
	size_t count = 0;
	struct llist *llist = NULL;

	llist = calloc(1, sizeof(*llist));
	if (!llist) {
		goto out;
	}

	llist->head = init;

	if (init) {
		struct ll_node **iter = NULL;

		iter = &init;
		while (*iter) {
			iter = &(*iter)->next;
			++count;
		}
	}

	llist->num_items = count;

out:
	return llist;
}

struct llist *llist_create_empty(void)
{
	return llist_create(NULL);
}

void llist_destroy(struct llist *llist)
{
	if (llist) {
		if (llist->head) {
			struct ll_node **itm = &llist->head;
			while (itm && *itm) {
				/* TODO fix removig all elements */
				struct ll_node *n = *itm;
				*itm = (*itm)->next; /* remove node from list*/
				itm = &(*itm)->next; /* iterator.next() */
				ll_node_free(n); /* free node memory */
				n = NULL;
			}
			/* free last item */
			ll_node_free(llist->head);
		}
		free(llist);
	}
}

int llist_append(struct llist *llist, struct ll_node *node)
{
	int ret = -1;
	struct ll_node **iter = NULL;

	if (!llist) {
		errno = EINVAL;
		goto out;
	}

	iter = &llist->head;

	while (*iter) {
		iter = &(*iter)->next;
	}
	*iter = node;
	++(llist->num_items);

	ret = 0;
out:
	return ret;
}

int llist_append_data(struct llist *llist, const void *data, size_t data_len)
{
	int ret = -1;
	struct ll_node *node = NULL;

	if (!llist) {
		errno = EINVAL;
		goto out;
	}

	node = calloc(1, sizeof(*node));
	if (!node) {
		goto out;
	}

	if (data && data_len) {
		node->data = calloc(data_len, sizeof(unsigned char));
		if (!node->data) {
			ll_node_free(node);
			goto out;
		}
		memcpy(node->data, data, data_len);
		node->data_len = data_len;
	}

	ret = llist_append(llist, node);

out:
	return ret;
}

int llist_append_str(struct llist *llist, const char *str)
{
	return llist_append_data(llist, (const void *)str, strlen(str));
}

int llist_delete(struct llist *llist, const struct ll_node *entry)
{
	int ret = -1;
	struct ll_node **iter = NULL;
	struct ll_node *del = NULL;

	if (!llist || !llist->head || !entry) {
		errno = EINVAL;
		goto out;
	}

	iter = &llist->head;

	while (iter && !ll_node_equal(entry, *iter)) {
		iter = &(*iter)->next;
	}
	if (iter && *iter) {
		del = *iter;
		*iter = (*iter)->next;
		ll_node_free(del);
		--(llist->num_items);
	}

	ret = 0;
out:
	return ret;
}

int llist_delete_data(struct llist *llist, void *data, size_t data_len)
{
	struct ll_node entry = { .data = data, .data_len = data_len };
	return llist_delete(llist, &entry);
}

int llist_delete_str(struct llist *llist, char *data)
{
	return llist_delete_data(llist, (void *)data, strlen(data));
}

int llist_for_each(struct llist *llist, llist_iter_cb_t cb)
{
	int ret = -1;
	size_t i = 0;
	struct ll_node **iter = NULL;
	enum llist_iter_retval cbret;
	struct ll_node **del_list = NULL;

	if (!llist || !cb) {
		errno = EINVAL;
		goto out;
	}

	del_list = calloc(llist->num_items, sizeof(*del_list));
	if (!del_list) {
		goto out;
	}

	iter = &llist->head;

	while (iter && *iter) {
		cbret = cb(*iter);
		if (cbret == LLIST_ITER_BREAK) {
			break;
		}
		if (cbret == LLIST_ITER_DELETE) {
			del_list[i++] = *iter;
		}
		iter = &(*iter)->next;
	}

	for (size_t j = 0; j < i; ++j) {
		llist_delete(llist, del_list[j]);
	}

	ret = 0;
out:
	SFREE(del_list);
	return ret;
}
